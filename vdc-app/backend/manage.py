from flask.cli import FlaskGroup

from __init__ import app, db


cli = FlaskGroup(app)


@cli.command("create_db")
def create_db():
    with app.app_context():
        db.drop_all()
    with app.app_context():
        db.create_all()
    db.session.commit()


if __name__ == "__main__":
    cli()
