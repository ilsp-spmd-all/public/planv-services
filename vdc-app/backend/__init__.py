from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from itsdangerous import URLSafeTimedSerializer
from config import Config

# init SQLAlchemy so we can use it later in our models
login_manager = LoginManager()
config = Config()
login_serializer = URLSafeTimedSerializer(config.SECRET_KEY)
cookie_duration = config.REMEMBER_COOKIE_DURATION.total_seconds()

app = Flask(__name__)

app.config.from_object('config.Config')
app.config.from_object('config.DevConfig')
app.config["SQLALCHEMY_BINDS"] = {'results': 'sqlite:///results.sqlite'}
login_manager.init_app(app)
login_manager.login_view = 'auth.login'
db = SQLAlchemy(app)
# blueprint for auth routes in our app
from auth import auth as auth_blueprint
app.register_blueprint(auth_blueprint)

## Using a production configuration
# app.config.from_object('config.ProdConfig')

# Using a development configuration





# with app.app_context():
#     db.create_all()

