from flask import Blueprint, request, Response
from flask_api import status
from flask_login import login_user, logout_user, login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from __init__ import db
from models import User
import json

auth = Blueprint('auth', __name__)

@auth.route("/getpatients", methods=["GET"])
def get_patients():
    username = request.form.get('username')
    password = request.form.get('password')

    user = User.query.filter(User.username==username).first()

    if not user:
        resp = {
            "error": "User does not exist"
        }
        resp = json.dumps(resp)
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    if check_password_hash(user.password, password):
        resp = user.patients[0].to_json()
        token = user.get_auth_token()
        print(token, resp)

        return Response(
            resp,
            status=status.HTTP_200_OK,
            mimetype="application/json")

@auth.route('/signup', methods=['POST'])
def signup_post():
    username = request.form.get('username')
    password = request.form.get('password')
    is_admin = True

    user = User.query.filter_by(username=username).first() # if this returns a user, then the username already exists in database

    if user: # if a user is found, we want to redirect back to signup page so user can try again
        resp = {
            "error": "User already exists"
        }
        resp = json.dumps(resp)
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(username=username, password=generate_password_hash(password, method='sha256'), is_admin=is_admin)

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    resp = 'User {}, has been saved'.format(username)

    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")

@auth.route('/create_patient', methods=['POST'])
def create_patient():
    username = request.form.get('username')
    password = request.form.get('password')
    admin = User.query.filter_by(id=request.form.get('admin_id')).first() # if this returns a user, then the username already exists in database
    is_admin = False

    user = User.query.filter_by(username=username).first() # if this returns a user, then the username already exists in database

    if user: # if a user is found, we want to redirect back to signup page so user can try again
        resp = {
            "error": "User already exists"
        }
        resp = json.dumps(resp)
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    if not admin:
        resp = {
            "error": "Admin doesn't exist"
        }
        resp = json.dumps(resp)
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(username=username, password=generate_password_hash(password, method='sha256'), admin=admin, is_admin=is_admin)

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    resp = 'User {}, has been saved'.format(username)

    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")

@auth.route("/login", methods=["GET", "POST"])
def login():
    username = request.form.get('username')
    password = request.form.get('password')

    print("username", username)
    print("password", password)
    user = User.query.filter(User.username==username).first()

    if not user:
        resp = {
            "error": "User does not exist"
        }
        resp = json.dumps(resp)
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    if check_password_hash(user.password, password):
        user.authenticated = True
        db.session.add(user)
        db.session.commit()
        login_user(user, remember=True)
        print("current_user", current_user.to_json())
        resp = {'user': current_user.to_json(), 'token': current_user.get_auth_token()}
        resp = json.dumps(resp)

        return Response(
            resp,
            status=status.HTTP_200_OK,
            mimetype="application/json")

@auth.route('/logout')
@login_required
def logout():
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    resp = 'Logout'

    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")

@auth.route("/getid", methods=["GET", "POST"])
def get_id():
    username = request.form.get('username')
    password = request.form.get('password')

    user = User.query.filter(User.username==username).first()

    if not user:
        resp = {
            "error": "User does not exist"
        }
        resp = json.dumps(resp)
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    if check_password_hash(user.password, password):
        resp = user.get_id()
        token = user.get_auth_token()
        print(token)

        return Response(
            resp,
            status=status.HTTP_200_OK,
            mimetype="application/json")
