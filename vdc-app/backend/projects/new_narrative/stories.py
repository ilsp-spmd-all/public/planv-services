stories = [
    { "id": 0, 
      "quote": "Δοκιμαστική Ηχογράφηση", 
      "title": "test", 
      "path": "./projects/new_narrative/static/test",
      "type": "none"
      },
    { "id": 1,
      "quote": "Πείτε μου τι συνέβη όταν πάθατε το εγκεφαλικό." ,
      "title": "stroke",
      "path": "./projects/new_narrative/static/stroke",
      "type": "none"
      },
    { "id": 2,
      "quote": "Κοιτάξτε πρώτα όλες τις εικόνες με τη σειρά. Μετά πείτε μου τι συμβαίνει σε αυτές σαν μια ιστορία με αρχή, μέση και τέλος." ,
      "title": "umbrella",
      "path": "./projects/new_narrative/static/umbrella",
      "type" : "single_image"
      },
    { "id": 3,
      "quote": "Δείτε όλα όσα συμβαίνουν σε αυτή την εικόνα. Μετά πείτε μου μια ιστορία γι' αυτά που βλέπετε. Πείτε μου την ιστορία με αρχή, μέση και τέλος." ,
      "title": "cat",
      "path": "./projects/new_narrative/static/cat",
      "type": "single_image"
      },
    { "id": 4,
      "quote": "Θυμάστε το παραμύθι της Σταχτοπούτας; Κοιτάξτε με τη σειρά τις εικόνες, που θα σας βοηθήσουν να το θυμηθείτε. Μετά πείτε μου την ιστορία της Σταχτοπούτας με δικά σας λόγια, χωρίς να κοιτάζετε τις εικόνες. ​" ,
      "title": "cinderella",
      "path": "./projects/new_narrative/static/cinderella",
      "type" : "carousel_images"
      },
    { "id": 5,
      "quote": "Κοιτάξτε πρώτα όλες τις εικόνες με τη σειρά. Μετά πείτε μου τι συμβαίνει σε αυτές σαν μια ιστορία με αρχή, μέση και τέλος." ,
      "title": "party",
      "path": "./projects/new_narrative/static/party",
      "type": "multiple_images"
      },
    { "id": 6,
      "quote": "Θα ακούσετε μια ιστορία για το τι συμβαίνει σε αυτές τις εικόνες. Μόλις τελειώσει θα πρέπει να την επαναλάβετε όσο το δυνατόν πιο ολοκληρωμένα. Κοιτάξτε πρώτα τις εικόνες." ,
      "title": "ring",
      "path": "./projects/new_narrative/static/ring",
      "type": "multiple_images"
      },
    { "id": 7,
      "quote": "Θα ακούσετε μια ιστορία. Ακούστε την προσεκτικά και μόλις τελειώσει θα πρέπει να την επαναλάβετε όσο το δυνατόν πιο ολοκληρωμένα." ,
      "title": "rabbit",
      "path": "./projects/new_narrative/static/rabbit",
      "type": "none"
      },
]
