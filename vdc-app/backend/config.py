"""Flask config."""
import os
from os import environ, path
from dotenv import load_dotenv
from datetime import timedelta
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))
class Config:
    """Base config."""
    SECRET_KEY = environ.get('SECRET_KEY')
    STATIC_FOLDER = 'static'
    TEMPLATES_FOLDER = 'templates'
    REMEMBER_COOKIE_DURATION = timedelta(days=14)


# class ProdConfig(Config):
#     FLASK_ENV = 'production'
#     DEBUG = False
#     TESTING = False
#     SQLALCHEMY_DATABASE_URI = environ.get('PROD_DATABASE_URI')
#     SQLALCHEMY_BINDS = environ.get('SQLALCHEMY_BINDS')
    
class DevConfig(Config):
    FLASK_ENV = 'development'
    DEBUG = True
    TESTING = True
    # SQLALCHEMY_DATABASE_URI = environ.get('DEV_DATABASE_URI')
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", "sqlite://")
    # SQLALCHEMY_BINDS = environ.get('SQLALCHEMY_BINDS')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
