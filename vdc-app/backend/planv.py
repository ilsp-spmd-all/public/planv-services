from flask import Flask, request, Response, render_template
from flask_api import status

import json
import os
import gevent
import re
import base64
from gevent.pywsgi import WSGIServer
import time
from sqlalchemy.orm import query
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage
from flask_login import current_user
from flask_sock import Sock
from __init__ import db, app
from publisher import Publisher
import pathlib
from urllib.parse import urlparse


# change project for other versions
# from projects.bdae_part_one.stories import stories
# from projects.bdae_part_three.stories import stories
from projects.new_narrative.stories import stories

# from projects.dysarthria.stories import stories

from aio_pika import connect, Message
import asyncio
import threading

import pika, os
from models import Results, Story
from datetime import datetime, date
from typing import Dict
import logging

logging.basicConfig()

# RECORDINGS_PATH = "/home/dimastro/planv_recordings"
RECORDINGS_PATH = "./recordings"
# FRONTEND_HOST = "https://apps.ilsp.gr"
FRONTEND_HOST = "http://localhost:3000"

HOSTNAME_WHITELIST = [
    "http://localhost:3000",
    "http://localhost:8000",
    "http://chomsky.ilsp.gr:3004",
]


MICROSERVICE_HOST = "http://localhost:8000"

# app = create_app()
connection = None
channel = None
loop = None
ws_dict = {}

sockets = Sock(app)

audio = bytearray()
filename = ""

# Create recordings path if does not exist
pathlib.Path(RECORDINGS_PATH).mkdir(parents=True, exist_ok=True)

# Check if all stories folders exist
# if not create empty folder
for story in stories:
    pathlib.Path(story["path"]).mkdir(parents=True, exist_ok=True)


def load_json(myjson):
    try:
        json_object = json.loads(myjson)
    except ValueError as e:
        return False
    return json_object


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)


channel = None


global channel2
channel2 = Publisher(host="rabbitmq", port="5672", username="vdc", password="vdc" )
channel2.connect()
channel2.consume()




WS = None


@sockets.route("/")
def stream_socket(ws):
    header_flag = False
    global channel, msg_thread, channel2, ws_dict

    while True:
        message = ws.receive()
        message_json = load_json(message)

        if message_json and "control" in message_json:
            if message_json["control"] == "start":
                audio = bytearray()
                print("start sending", flush=True)
                ws.send(json.dumps({"status": "accepting"}))
                filename = message_json["filename"]
                username = filename.split("_")[0]
                print(f"filename = {filename}", flush=True)
                # Publish to exchange
                if "test" in filename:
                    continue
                obj = {"user": username, "control": "start"}
                channel2.publish(obj)
            elif message_json["control"] == "header":
                header_flag = True
                if "test" in filename:
                    continue
                obj = {"user": username, "control": "header"}
                channel2.publish(obj)
            elif message_json["control"] == "ping":
                print("got ping!", flush=True)
            elif message_json["control"] == "end":
                audio_path = f"{RECORDINGS_PATH}/{filename}"
                with open(audio_path, "wb") as file:
                    file.write(audio)

                if "test" in filename:
                    ws.send(json.dumps({"status": "received"}))
                    continue
                story_name = filename.split("_")[1]
                print(f"story name: {story_name}", flush=True)
                results_session = Results.query.filter(Results.username==username).order_by(Results.created_at.desc()).first()
                if not results_session or results_session.completed:
                    results_session = Results(
                        username=username,
                    )
                    db.session.add(results_session)
                    db.session.commit()

                story = Story.query.filter(Story.results_id == results_session.id, Story.name == story_name).first()
                if not story:
                    story = Story(
                        name = story_name,
                        results_id = results_session.id,
                    )
                    db.session.add(story)
                    db.session.commit()
                story_id = story.id
                print(f"story id: {story_id}", flush=True)
                story.audio_name = filename
                db.session.commit()
                print("end sending", flush=True)  #
                obj = {"user": username, "control": "end", "message": filename}
                channel2.publish(obj)
                ws.send(json.dumps({"status": "received"}))
            elif message_json["control"] == "close":
                ws.close()
                print("closed socket", flush=True)
                for thread in threading.enumerate():
                    print(thread.name)
            elif message_json["control"] == "results":
                ws_dict[username] = ws
                if "test" in filename:
                    continue
                print(f"results story id: {story_id}", flush=True)
                print(
                    f"Flask backend is requested from React frontend to fetch users results {message_json}", flush=True
                )
                filename = message_json["filename"]
                sessionId = message_json["sessionId"]
                print(f"Session ID = {sessionId}", flush=True)
                username = filename.split("_")[0]
                obj = {
                    "user": username,
                    "control": "results",
                    "message": filename,
                    "story_id": story_id,
                }
                channel2.publish(obj)
        elif not message == "undefined":
            if not header_flag:
                audio += message
                print("got chunk", flush=True)
                if "test" in filename:
                    continue
                obj = {
                    "user": username,
                    "control": "streaming-data",
                    "message": message,
                }
                channel2.publish(obj)
            else:
                audio = message + audio
                header_flag = False
                print("got header", flush=True)
                if "test" in filename:
                    continue
                obj = {
                    "user": username,
                    "control": "streaming-header",
                    "message": message,
                }
                channel2.publish(obj)


white = [FRONTEND_HOST, MICROSERVICE_HOST]


@app.after_request
def after_request(response):
    """!
    @brief Add necessary info to headers. Useful for preventing CORS permission errors.
    """
    parsed_uri = urlparse(request.referrer)
    url = "{uri.scheme}://{uri.netloc}".format(uri=parsed_uri)
    if url in HOSTNAME_WHITELIST:
        response.headers.add("Access-Control-Allow-Origin", url)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add(
            "Access-Control-Allow-Headers", "Content-Type,Authorization"
        )
        response.headers.add(
            "Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS"
        )
    return response

@app.route("/save_audio", methods=["POST"])
def save_audio():
    if (
        request is None
        or not request.files.getlist("audio")
        or not isinstance(request.files.getlist("audio")[0], FileStorage)
    ):
        resp = {"error": "No JSON POSTed containing the wav"}
        return Response(
            resp, status=status.HTTP_400_BAD_REQUEST, mimetype="application/json"
        )

    audio = request.files.getlist("audio")[0]
    # Sanitise the filename
    filename = secure_filename(audio.filename)
    # Save local copy of wav data
    if not os.path.exists(RECORDINGS_PATH):
        os.mkdir(RECORDINGS_PATH)

    rec_path = os.path.join(RECORDINGS_PATH, "{}.wav".format(filename))

    audio.save(rec_path)
    response = "Audiofile {} has been saved".format(filename)

    return Response(response, status=status.HTTP_200_OK, mimetype="application/json")


@app.route("/get_stories_results", methods=["GET"])
def get_stories_results():
    stories = {}
    username = request.args.get('username', None)
    print(f"get stories_results {username}", flush=True)
    results_session = Results.query.filter(Results.username==username).order_by(Results.created_at.desc()).first()
    if not results_session:
        response = f"Faild to End Session for user: {username}"
        return Response(
            response, status=status.HTTP_400_BAD_REQUEST, mimetype="application/json"
        )
    results_session.completed = True
    db.session.commit()
    stories_list = [story.to_dict() for story in results_session.stories]
    print(stories_list, flush=True)
    stories["id"] = results_session.id
    stories["stories_list"] = stories_list
    resp = json.dumps(stories, cls=CustomJSONEncoder)
    return Response(resp, status=status.HTTP_200_OK, mimetype="application/json")

@app.route("/get_stories", methods=["GET"])
def get_stories():
    print("get stories", flush=True)
    # test_obj = {"control": "test"}
    # channel2.publish(test_obj)
    resp = json.dumps(stories)
    return Response(resp, status=status.HTTP_200_OK, mimetype="application/json")


@app.route("/download_instructions", methods=["GET", "POST"])
def download_instructions():
    pdf_filepath = "./static/test/instructions.pdf"
    with open(pdf_filepath, "rb") as pdf_file:
        base64_bytes = base64.b64encode(pdf_file.read())
        base64_string = base64_bytes.decode("utf-8")
        resp = json.dumps({"pdf": base64_string})
    return Response(resp, status=status.HTTP_200_OK, mimetype="application/json")


@app.route("/get_photos/<id>", methods=["GET"])
def get_photos(id):
    story_dict = next((item for item in stories if item["id"] == int(id)), None)
    if not story_dict:
        resp = {"error": "Story doesn't exist"}
        resp = json.dumps(resp)
        return Response(
            resp, status=status.HTTP_400_BAD_REQUEST, mimetype="application/json"
        )

    path = story_dict["path"]
    photos = []
    for file in [f for f in os.listdir(path) if f.endswith(".jpg")]:
        with open(os.path.join(path, file), "rb") as image_file:
            base64_bytes = base64.b64encode(image_file.read())
            base64_string = base64_bytes.decode("utf-8")
            photos.append({"id": os.path.splitext(file)[0], "src": base64_string})

    resp = json.dumps(photos)
    return Response(resp, status=status.HTTP_200_OK, mimetype="application/json")


@app.route("/get_instructions/<id>", methods=["GET"])
def get_instructions(id):

    if int(id) == 42000:
        story_dict = next((item for item in stories if item["id"] == 25), None)
    elif int(id) == 42001:
        story_dict = next((item for item in stories if item["id"] == 48), None)
    else:
        story_dict = next((item for item in stories if item["id"] == int(id)), None)
    if not story_dict:
        resp = {"error": "Story doesn't exist"}
        resp = json.dumps(resp)
        return Response(
            resp, status=status.HTTP_400_BAD_REQUEST, mimetype="application/json"
        )

    path = story_dict["path"]
    audios = []
    for file in [
        f
        for f in os.listdir(path)
        if f.endswith(".mp3") and f.startswith("instructions")
    ]:
        with open(os.path.join(path, file), "rb") as audio_file:
            base64_bytes = base64.b64encode(audio_file.read())
            base64_string = base64_bytes.decode("utf-8")
            audios.append({"id": os.path.splitext(file)[0], "src": base64_string})

    resp = json.dumps(audios)
    return Response(resp, status=status.HTTP_200_OK, mimetype="application/json")


@app.route("/get_audio/<id>", methods=["GET"])
def get_audio(id):
    story_dict = next((item for item in stories if item["id"] == int(id)), None)
    if not story_dict:
        resp = {"error": "Story doesn't exist"}
        resp = json.dumps(resp)
        return Response(
            resp, status=status.HTTP_400_BAD_REQUEST, mimetype="application/json"
        )

    path = story_dict["path"]
    audios = []
    for file in [
        f for f in os.listdir(path) if f.endswith(".mp3") and f.startswith("story")
    ]:
        with open(os.path.join(path, file), "rb") as audio_file:
            base64_bytes = base64.b64encode(audio_file.read())
            base64_string = base64_bytes.decode("utf-8")
            audios.append({"id": os.path.splitext(file)[0], "src": base64_string})

    resp = json.dumps(audios)
    return Response(resp, status=status.HTTP_200_OK, mimetype="application/json")


@app.route("/get_other_audio/<id>", methods=["GET"])
def get_other_audio(id):
    story_dict = next((item for item in stories if item["id"] == int(id)), None)
    if not story_dict:
        resp = {"error": "Story doesn't exist"}
        resp = json.dumps(resp)
        return Response(
            resp, status=status.HTTP_400_BAD_REQUEST, mimetype="application/json"
        )

    path = story_dict["path"]
    audios = []
    # for file in [f for f in os.listdir(path) if f.endswith('.wav') ]:
    #     with open(os.path.join(path, file), "rb") as audio_file:
    #         base64_bytes = base64.b64encode(audio_file.read())
    #         base64_string = base64_bytes.decode('utf-8')
    #         audios.append({"id": os.path.splitext(file)[0], "src": base64_string })

    for file in [f for f in os.listdir(path) if f.endswith(".wav")]:
        with open(os.path.join(path, file), "rb") as audio_file:
            base64_bytes = base64.b64encode(audio_file.read())
            base64_string = base64_bytes.decode("utf-8")
            audios.append({"id": os.path.splitext(file)[0], "src": base64_string})

    resp = json.dumps(audios)
    return Response(resp, status=status.HTTP_200_OK, mimetype="application/json")


@app.route("/get_recording/<filename>", methods=["GET"])
def get_recording(filename):
    print(filename, flush=True)
    audiofile = filename + ".wav"
    audios = []
    with open(os.path.join(RECORDINGS_PATH, audiofile), "rb") as audio_file:
        base64_bytes = base64.b64encode(audio_file.read())
        base64_string = base64_bytes.decode("utf-8")
        audios.append({"id": os.path.splitext(audiofile)[0], "src": base64_string})

    resp = json.dumps(audios)
    return Response(resp, status=status.HTTP_200_OK, mimetype="application/json")

@app.route("/end_session/<username>", methods=["GET"])
def end_session(username):
    print(username, flush=True)

    obj = {"user": username, "control": "mean_scores"}
    channel2.publish(obj)
    response = f"End Session for user: {username}"
    return Response(response, status=status.HTTP_200_OK, mimetype="application/json")

@app.route("/session_status/<username>", methods=["GET"])
def session_status(username):

    results_session = Results.query.filter(Results.username==username).order_by(Results.created_at.desc()).first()
    print(f"session status: {username}, {results_session}", flush=True)
    if not results_session or results_session.completed:
        print(f"session status: not", flush=True)
        response = json.dumps([])
        return Response(response, status=status.HTTP_200_OK, mimetype="application/json")
    stories_list = [story.to_dict()["name"] for story in results_session.stories]
    print(f"session status: stories list, {stories_list}", flush=True)
    response = json.dumps(stories_list)
    return Response(response, status=status.HTTP_200_OK, mimetype="application/json")

@app.route("/sessions_results/<username>", methods=["GET"])
def session_results(username):
    results_dict = {}
    sessions_results = Results.query.filter(Results.username==username).order_by(Results.created_at).all()
    print(f"session results: {username}, {sessions_results}", flush=True)
    if not sessions_results:
        print(f"session results: not", flush=True)
        response = json.dumps([])
        return Response(response, status=status.HTTP_200_OK, mimetype="application/json")
    for i, session in enumerate(sessions_results):
        results_dict[f"session_{i}"] = session.to_dict()
        for story in session.stories:
            results_dict[f"session_{i}"][story.to_dict()["name"]] = story.to_dict()
    print(f"sessions results: {sessions_results}", flush=True)
    response = json.dumps(results_dict, cls=CustomJSONEncoder)
    return Response(response, status=status.HTTP_200_OK, mimetype="application/json")


    print(f"session status: stories list, {stories_list}", flush=True)
    response = json.dumps(stories_list)
    return Response(response, status=status.HTTP_200_OK, mimetype="application/json")

@app.route("/post_severity_results", methods=["POST"])
def post_severity_results():
    global ws_dict
    print("inside results", flush=True)
    results = request.json
    print(f"Results: {results}", flush=True)
    resp = json.dumps({"msg": "success"})

    username = results["username"].split("_")[0]
    severity = results["severity"]
    speech_non_speech_ratio = results["speech-non-speech-ratio"]
    words_per_minute = results["words-per-minute"]
    avg_silence = results["avg-silence"]
    avg_speech = results["avg-speech"]
    sessionId = results["sessionId"]

    websocket = ws_dict[username]

    # Query past results
    past_results_queries = Results.query.filter(Results.username == username).all()

    def create_past_results_dict(past_result):
        return {
            "username": past_result.username,
            "date": past_result.date,
            "sessionId": past_result.sessionId,
            "severity": past_result.severity,
            "speech_non_speech_ratio": past_result.speech_non_speech_ratio,
            "words_per_minute": past_result.words_per_minute,
            "avg_silence": past_result.avg_silence,
            "avg_speech": past_result.avg_speech,
        }

    past_results = [
        create_past_results_dict(past_results_query)
        for past_results_query in past_results_queries
    ]

    # Add to database
    res_db = Results(
        username=username,
        date=datetime.datetime.now(),
        severity=severity,
        speech_non_speech_ratio=speech_non_speech_ratio,
        sessionId=sessionId,
        words_per_minute=words_per_minute,
        avg_silence=avg_silence,
        avg_speech=avg_speech,
    )
    db.session.add(res_db)
    db.session.commit()

    feats = {
        "speech_non_speech_ratio": speech_non_speech_ratio,
        "words_per_minute": words_per_minute,
        "avg_silence": avg_silence,
        "avg_speech": avg_speech,
    }

    results = {
        "status": "results",
        "severity": severity,
        "features": feats,
        "past_results": past_results,
    }

    websocket.send(json.dumps(results))
    del ws_dict[username]
    return Response(resp, status=status.HTTP_200_OK, mimetype="application/json")


print(app.url_map)
