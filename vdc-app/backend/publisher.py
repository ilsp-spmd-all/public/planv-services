import logging
import json
import pika
import pickle
import time
import threading
import asyncio
from __init__ import db
from models import Results, Story
from planv import app

def tranform_dict_to_encoded_json(obj):
    pickled = pickle.dumps(obj)
    return pickled

class Publisher:
    EXCHANGE='default_exchange'
    ROUTING_KEY = 'ws'
    QUEUE = 'ws'
    CONS_ROUTING_KEY = 'results'
    CONS_QUEUE = 'results'


    def __init__(self, host, port, username, password):
        self._params = pika.connection.ConnectionParameters(
            host=host,
            port=port,
            credentials=pika.credentials.PlainCredentials(username, password))
        self._conn = None
        self._conn2 = None
        self._channel = None
        self._channel2 = None

    def connect(self):
        if not self._conn or self._conn.is_closed:
            self._conn = pika.BlockingConnection(self._params)
            self._channel = self._conn.channel()
            self._channel.exchange_declare(exchange=self.EXCHANGE)
            self._channel.queue_declare(queue=Publisher.QUEUE, durable=True)
            self._channel.queue_bind(queue=Publisher.QUEUE, exchange=Publisher.EXCHANGE, routing_key=Publisher.ROUTING_KEY)

    def _publish(self, msg):
        self._channel.basic_publish(exchange=self.EXCHANGE,
                                    routing_key=self.ROUTING_KEY,
                                    body=tranform_dict_to_encoded_json(msg))
        logging.debug('message sent: %s', msg)

    def publish(self, msg):
        """Publish msg, reconnecting if necessary."""

        try:
            self._publish(msg)
        except:
            logging.debug('reconnecting to queue', flush=True)
            self.connect()
            self._publish(msg)

    def close(self):
        if self._conn and self._conn.is_open:
            logging.debug('closing queue connection')
            self._conn.close()

    def consume(self):
        def on_message(channel, method_frame, header_frame, body):
            print(method_frame.delivery_tag, flush=True)
            print(f"test receive", flush=True)
            print()
            str_data = body.decode('utf-8')

            # Deserialize the JSON string into a dictionary
            dict_results = json.loads(str_data)
            print(dict_results, flush=True)
            print(dict_results.keys(), flush=True)
            channel.basic_ack(delivery_tag=method_frame.delivery_tag)
            if dict_results["control"] == "story_results":
                with app.app_context():
                    story = Story.query.filter_by(id=dict_results['story_id']).first()
                    if story:
                        Story.query.filter_by(id=dict_results['story_id']).update({
                            "average_utterance_length": dict_results['average_utterance_length'],
                            "verbs_words_ratio": dict_results['verbs_words_ratio'],
                            "nouns_words_ratio": dict_results['nouns_words_ratio'],
                            "adjectives_words_ratio": dict_results['adjectives_words_ratio'],
                            "adverbs_words_ratio": dict_results['adverbs_words_ratio'],
                            "conjuctions_words_ratio": dict_results['conjuctions_words_ratio'],
                            "prepositions_words_ratio": dict_results['prepositions_words_ratio'],
                            "number_of_words": dict_results['number_of_words'],
                            "words_per_minute": dict_results['words_per_minute'],
                            "verbs_per_utterance": dict_results['verbs_per_utterance'],
                            "unique_words_per_words": dict_results['unique_words_per_words'],
                            "nouns_verbs_ratio": dict_results['nouns_verbs_ratio'],
                            "open_close_class_ratio": dict_results['open_close_class_ratio'],
                            "mean_clauses_per_utterance": dict_results['mean_clauses_per_utterance'],
                            "mean_dependent_clauses": dict_results['mean_dependent_clauses'],
                            "mean_independent_clauses": dict_results['mean_independent_clauses'],
                            "dependent_all_clauses_ratio": dict_results['dependent_all_clauses_ratio'],
                            "mean_tree_height": dict_results['mean_tree_height'],
                            "max_tree_depth": dict_results['max_tree_depth'],
                            "n_independent": dict_results['n_independent'],
                            "n_dependent": dict_results['n_dependent'],
                            "propositional_density": dict_results['propositional_density'],
                            "words_in_vocabulary_per_words": dict_results['words_in_vocabulary_per_words'],
                            "unique_words_in_vocabulary_per_words": dict_results['unique_words_in_vocabulary_per_words'],
                            "determiners_words_ratio": dict_results['determiners_words_ratio'],
                            "pronouns_words_ratio": dict_results['pronouns_words_ratio'],
                            # "interjections_words_ratio": dict_results['interjections_words_ratio'],
                            "transcription": dict_results['transcription'],
                        })
                        db.session.commit()

            if dict_results["control"] == "mean_scores":
                with app.app_context():
                    results = Results.query.filter_by(id=dict_results['results_id']).first()
                    if results:
                        Results.query.filter_by(id=dict_results['results_id']).update({
                            "average_utterance_length": dict_results['average_utterance_length'],
                            "verbs_words_ratio": dict_results['verbs_words_ratio'],
                            "nouns_words_ratio": dict_results['nouns_words_ratio'],
                            "adjectives_words_ratio": dict_results['adjectives_words_ratio'],
                            "adverbs_words_ratio": dict_results['adverbs_words_ratio'],
                            "conjuctions_words_ratio": dict_results['conjuctions_words_ratio'],
                            "prepositions_words_ratio": dict_results['prepositions_words_ratio'],
                            "number_of_words": dict_results['number_of_words'],
                            "words_per_minute": dict_results['words_per_minute'],
                            "verbs_per_utterance": dict_results['verbs_per_utterance'],
                            "unique_words_per_words": dict_results['unique_words_per_words'],
                            "nouns_verbs_ratio": dict_results['nouns_verbs_ratio'],
                            "open_close_class_ratio": dict_results['open_close_class_ratio'],
                            "mean_clauses_per_utterance": dict_results['mean_clauses_per_utterance'],
                            "mean_dependent_clauses": dict_results['mean_dependent_clauses'],
                            "mean_independent_clauses": dict_results['mean_independent_clauses'],
                            "dependent_all_clauses_ratio": dict_results['dependent_all_clauses_ratio'],
                            "mean_tree_height": dict_results['mean_tree_height'],
                            "max_tree_depth": dict_results['max_tree_depth'],
                            "n_independent": dict_results['n_independent'],
                            "n_dependent": dict_results['n_dependent'],
                            "propositional_density": dict_results['propositional_density'],
                            "words_in_vocabulary_per_words": dict_results['words_in_vocabulary_per_words'],
                            "unique_words_in_vocabulary_per_words": dict_results['unique_words_in_vocabulary_per_words'],
                            "determiners_words_ratio": dict_results['determiners_words_ratio'],
                            "pronouns_words_ratio": dict_results['pronouns_words_ratio'],
                            # "interjections_words_ratio": dict_results['interjections_words_ratio'],
                            "WAB_AQ_category": dict_results['severity'],
                            "date": db.func.now(),
                        })
                        db.session.commit()
        if not self._conn2 or self._conn2.is_closed:
            self._conn2 = pika.BlockingConnection(self._params)
            self._channel2 = self._conn2.channel()
            self._channel2.exchange_declare(exchange=Publisher.EXCHANGE)
            self._channel2.queue_declare(queue=Publisher.CONS_QUEUE, durable=True)
            self._channel2.queue_bind(queue=Publisher.CONS_QUEUE, exchange=Publisher.EXCHANGE, routing_key=Publisher.CONS_ROUTING_KEY)
        self._channel2.basic_consume(Publisher.CONS_QUEUE, on_message)
        t1 = threading.Thread(target=self._channel2.start_consuming)
        t1.start()
        t1.join(0)


