const path = require('path');
const waveWorker = path.resolve(
            __dirname,
            'node_modules/opus-recorder/dist/waveWorker.min.js')
const publicPath = path.resolve(
            __dirname,
            'public')
const PATHS = {
    prod: '/planv/augmented-narrative/',
    test: '/planv/test/',
    bdae_part_one: '/planv/bdae-part-1/',
    bdae_part_three: '/planv/bdae-part-3/',
    dysarthria: '/planv/dysarthria/'
};
module.exports = {
    type: 'react-component',
    npm: {
        esModules: true,
        umd: {
            global: 'ReactPageScroller',
            externals: {
                react: 'React'
            }
        }
    },
    devServer: {
      disableHostCheck: true,
    },
    webpack: {
      publicPath: PATHS.test,
        copy: {
      options: {
        debug: true
      },
      patterns: [
        {from: waveWorker, to: publicPath}
      ]},
        extra : {
            module: {
                rules: [
                  {
                    test: /waveWorker\.min\.js$/,
                    use: [{ loader: 'file-loader' , 
                      options: {
                        postTransformPublicPath: (p) => "https://apps.ilsp.gr/" + PATHS.test + p,
                      }
                    }],
                  }
                ]
              }
        },
        html: {
            template: 'demo/src/index.html'
        },
    },
    
};
console.log(waveWorker);
