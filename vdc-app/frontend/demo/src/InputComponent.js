import ReactDOM from 'react-dom'
import React, {useState, useEffect} from "react";
import 'antd/dist/antd.css';
import {
         Spin, Form, Input, Tooltip, Select, Button,
        } from 'antd';
import {QuestionCircleOutlined, KeyOutlined} from '@ant-design/icons'
import { SentencesContext, sentences } from "./globals";
import { UserLogout } from "../../utils/utils";
import LogoEE from'../assets/ee.jpg';
import LogoEPANEK from'../assets/epanek.jpg';
import LogoESPA from'../assets/espa.jpg';
import { useKeycloak } from "@react-keycloak/web";
import { useNavigate } from '@reach/router';


const { Option } = Select;

export default function InputComponent(props){

  const {loading, setLoading} = useState(false)
  var userInfo = []
  let navigate = useNavigate()

  function nextPage(){
      props.inputNextPage(this.context.currentPage);
  }
  const {keycloak,initialized} = useKeycloak()
        
  const formItemLayout = {
      labelCol: {
          xs: { span: 24 },
          sm: { span: 8 },
      },
      wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
      },
  };
  const tailFormItemLayout = {
      wrapperCol: {
          xs: {
              span: 24,
              offset: 0,
          },
          sm: {
              span: 16,
              offset: 8,
          },
      },
  };

  useEffect(()=>{
    if(initialized && keycloak.authenticated){
      if(!keycloak.realmAccess.roles.includes('patient')){
        console.log('Not a patient gt logged in')
        UserLogout()
      }else{
        userInfo = keycloak.loadUserInfo();
        props.setUser(userInfo.preferred_username, keycloak.token).then(()=>{
        navigate('/planv/test/continue-session'),()=>{}
        })
        console.log(userInfo)
      }
    }
  },[])

  async function login(){
    if(initialized){
      if(!keycloak.authenticated){
        keycloak.login().then(async res => {
          console.log(res)
          if(!keycloak.realmAccess.roles.includes('patient')){
            console.log('Not a patient got logged in')
            let res = await UserLogout()
            console.log(res)
          }else{
            userInfo = await keycloak.loadUserInfo();
            await props.setUser(userInfo.preferred_username, keycloak.token).then(
             () => navigate('/planv/test/continue-session')
            )
            console.log(userInfo)
          }
        })
      
      
      }else{
        if(!keycloak.realmAccess.roles.includes('patient')){
          console.log('Not a patient got logged in')
          let res = await UserLogout()
          console.log(res)
        }else{
          userInfo = await keycloak.loadUserInfo();
          await props.setUser(userInfo.preferred_username, keycloak.token).then(
           () => navigate('/planv/test/*')
          )
          console.log(userInfo)
        }
      }
    }
  }

  return (
    <div>
        <Spin className="spin" spinning={false}>
      <div className="planv-header">
          <img  className="logos" src={LogoEE}  height="150"/>
          <img  className="logos" src={LogoEPANEK}  height="100"/>
          <img  className="logos" src={LogoESPA}  height="150"/>
      </div>
      <div className="planv-title">
        {props.title}
      </div>
      <div className="planv-title subtitle">
        {props.subtitle}
      </div>
      <div className="planv-form">
        <Button type="primary" htmlType="submit" onClick={()=>{login()}}>Log In</Button>
        <Button type="primary" htmlType="submit" onClick={()=>{keycloak.logout();navigate('/planv/test/')}}>Logout</Button>
        <Button type="primary" htmlType="submit" onClick={()=>{console.log(keycloak)}}>Keycloak</Button>
      </div>
        </Spin>
  </div>
  )
}

// InputComponent.contextType = SentencesContext;

