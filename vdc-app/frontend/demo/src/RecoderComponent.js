import React, { Component } from "react";
import 'antd/dist/antd.css';
import {
         Spin, Alert, ButtonToolbar, Button, notification, Layout, Typography, Card, Row, Col, message, Modal, Carousel,Image
        } from 'antd';
import { AudioOutlined, SoundOutlined , RightCircleOutlined, PauseCircleOutlined, LogoutOutlined, NodeIndexOutlined, InfoCircleOutlined, SlidersTwoTone} from '@ant-design/icons';
// import Recorder from '../../utils/recorder.js';
// import Recorder from '../../utils/opus_recorder.js';
// // import Recorder from 'opus-recorder';
// import encoderPath from '../../public/worklet/encoderWorker.min.js';
import Recorder from 'opus-recorder';
// import Recorder from './recorder.min.js';
import encoderPath from '../../public/waveWorker.min.js';

import {
  getAudioStream, exportBuffer, uploadFile, createHeader, downsampleBuffer,
} from '../../utils/audio';
import { SentencesContext, sentences, stories } from "./globals";
import {BACKEND_URL, WEBSOCKET_URL, VERSION} from '../../utils/config.js';
import './recorder.css';
import {downloadPDF, getRecording} from '../../utils/utils';

function concatenate(ResultConstructor, ...arrays) {
  let totalLength = 0;
  arrays.map((arr) => {
    totalLength += arr.length;
    return true;
  });
  const result = new ResultConstructor(totalLength);
  let offset = 0;
  arrays.map((arr) => {
    result.set(arr, offset);
    offset += arr.length;
    return true;
  });
  return result;
}



class RecorderComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stream: null,
            // recording: false,
            
            // listen: false,
            // socketReady: false,
            loading: false,
            // countdown_over: false,
            isModalVisible: false,
            can_dl_recs: false,
        };
        this.startRecord = this.startRecord.bind(this);
        this.stopRecord = this.stopRecord.bind(this);
        this.streamAudio = this.streamAudio.bind(this);
        this.sampleRate = 44100;
        this.audioBlob = null;
        this.chunk = null;
        this.webSocket = null;
        this.APIsampleRate = 44100;
        this.channels = 1;
        this.images = []
        this.thumbnail_height = 300;
        this.thumbnail_width = 500;
        this.get_recs_from_backend=true;
        this.recording_length = 0;
        this.use_chunks=true;
        this.recorder= null;
        // this.audioContext=null;


        if (!this.audioContext) {
          this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
        }
        this.sampleRate = this.audioContext.sampleRate;
        console.log('Recording with samplerate: ', this.sampleRate);
        this.audioContext.close();
       


    }

  // async componentDidMount() {
  //   this.setupBeforeUnloadListener();
  //   if (VERSION==1){
  //     this.forceUpdate(); // this needs to change as we force update when component is mounted 
  //   } 
  //   // await this.openSocket();
  // }


  //   setupBeforeUnloadListener() {
  //   window.addEventListener('beforeunload', (ev) => {
  //     ev.preventDefault();
  //     return this.unloadWebsocket();
  //   });
  // }

  //   async unloadWebsocket() {
  //   // await this.sendStop();
  //   await this.closeSocket();
  // }


    

    async prepareAudioStream() {
        let stream;

        try {
            stream = await getAudioStream();
        } catch (error) {
            // Users browser doesn't support audio.
            // Add your handler here.
            console.log(error);
        }

        this.setState({ stream });
        return stream;
    }

    setPID(newpid) {
    this.setState({
      pid: newpid,
    });
  }



    submitTCP() {
    const parThis = this;
    return new Promise((resolve, reject) => {
      console.log('Requesting PID for live demo...');
      parThis.setPID('');
      var rest_url = `${BACKEND_URL}/get_pid?channels=${parThis.channels}`;
      const options = {
        method: 'POST',
        headers: {
          'X-Auth-Token': parThis.state.token,
        },
        cache: 'no-cache',
      };
      fetch(rest_url, options)
        .then(data => data.json())
        .then((response) => {
          if (response) {
            const pid = response.pid;
            if (pid > 0) {
              parThis.setPID(pid);
              console.log(`Init successful with pid ${pid}`);
              resolve(true);
            } else {
              notification.error({
                message: 'Could not retrieve Process ID from API!',
              });
              reject('error');
            }
          } else {
            notification.error({
              message: 'Could not read API response!',
            });
            reject('error');
          }
        })
        .catch((error) => {
          notification.error({
            message: 'Error on POST request',
            description: `error: ${error}`,
          });
          reject('error');
        });
      return false;
    });
  }


    async startRecord() {
      // console.log('Started Record')
    try {
      // Get new PID and start sending data
      if (
        this.webSocket === null
        || this.webSocket.readyState === WebSocket.CLOSED
      ) {
        await this.openSocket();
      }
      // await this.submitTCP();

      this.audioBlob = null;
      this.recording_length = 0;
      this.chunk = new Float32Array();
      if (this.audioRef) {
        this.audioRef.removeAttribute('src');
        this.audioRef.load();
      }
      // const stream = await this.prepareAudioStream();
      // console.log("Prepared Audio Stream: ", stream);
      // this.header = createHeader(this.APIsampleRate, 4000000);
      await this.sendStart();


     
      // const recorder = new Recorder(this.audioContext);
      // const recorder = new Recorder(
      //   {encoderPath}
      //   // {encoderPath :'opus-recorder/dist/encoderWorker.min.js',   }
      //   );
      // const processorURL = chrome.runtime.getURL("encoderWorker.min.js");
      // console.log('URL to processor: ', processorURL);
      const bufferLength = 16384;
      const recorder = new Recorder({ encoderPath,bufferLength });
      
      // var recorder = import ('../../utils/opus_recorder.js');
      
      // recorder.ondataavailable = function( typedArray ){
      //   console.log("Recorded Buffer: ", typedArray);
      //   const buffer = typedArray;
      // }
      // recorder.onstart= function(){
      //   console.log('OpusRecorder started');
      // }

      


      await recorder.start();
      // console.log('OpusRecorder State: ', recorder.state);
      // console.log('OpusRecorder Init: ', recorder.initialize);
      // recorder.init(stream);


      // this.setState(
      //   {
      //     recorder,
      //     // recording: true,
      //     // countdown_over: false,
      //   },
      // );
      this.recorder=recorder;
    } catch (error) {
      console.log('StartRecord Error', error);
    }
    }

    openSocket() {
    const parThis = this;
    return new Promise((resolve, reject) => {
      // Ensures only one connection is open at a time
      console.log('Opening websocket connection...');
      if (
        parThis.webSocket !== null
        && parThis.webSocket.readyState !== WebSocket.CLOSED
      ) {
        console.log('WebSocket is already opened.');
        resolve();
      }
      // Create a new instance of the websocket
      // console.log("token:", parThis.context.token);
      parThis.webSocket = new WebSocket(
        // `ws://${parThis.state.apihostname}:8080/stream/${parThis.state.clientid}?X-Auth-Token=${parThis.state.token}`,
        `${WEBSOCKET_URL}/?token=${parThis.context.token}`,
      );

      /**
       * Binds functions to the listeners for the websocket.
       */
      parThis.webSocket.onopen = function openSock(event) {
        console.log(event);
        // send regular ping messages to keep websocket open
        parThis.pingInterval=setInterval(()=> {
          parThis.webSocket.send(JSON.stringify({ control: 'ping' }));
          // console.log("sent ping!")
        }, 20000);

        // parThis.setState({ socketReady: true });
        resolve(true);
        
      };

      parThis.webSocket.onmessage = function socketMessage(event) {
        console.log(event.data);
        // console.log(JSON.parse(event.data));
        const status = JSON.parse(event.data).status;
       
        if (status == 'accepting') {
          
          const interval = setInterval(() => {
            if (parThis.recorder!==null) {
              // parThis.counter = 0;
              // parThis.webSocket.send(parThis.header);
              // console.log("OpusRecorderr:", parThis.recorder);
              
              // console.log('encoderPath: ',parThis.recorder.config.encoderPath);
              parThis.recorder.start();
              
              
              
              // Stream first segment and then start interval
              if (parThis.use_chunks){
              // parThis.streamAudio();
              
              parThis.streamAudioHandle = setInterval(parThis.streamAudio, 2000);
              }
              clearInterval(interval);
            }
          }, 20);
        }
        if (status == 'received') {
          console.log("status update")
          
          parThis.setState({
            can_dl_recs: true,
          });
          console.log("can receive now!", parThis.state.can_dl_recs)
          
          
        }
      };

      parThis.webSocket.onclose = function (event) {
        console.log('Connection closed');
      };
      parThis.webSocket.onerror = function (event) {
        reject(event);
      };
    });
  }

  sendStart() {
    const parThis = this;
    {/*
    let target_team = ''
    if (this.context.sentence.id == 42000){
      target_team = 'test1';
    }
    else if (this.context.sentence.id == 42001){
      target_team = 'test2';
    }
    else {
      target_team = this.context.sentence.path.split('/')[2].split('_')[0];
    }
    */}

    // possibly not working!!!
    
    let target_team="";
    if (this.props.layout=="wordCards"){
    target_team = this.context.sentence.path.split('/')[2].split('_')[0];
    target_team="_"+target_team;
    }
    // if (this.props.layout=="wordCards"){
    //   target_team = this.context.sentence.path.split('/')[2].split('_')[0];
    //   let filename = `${this.context.user}_${this.context.sentence.title}_${this.context.session_ts}.wav`; 
    // }
    // else{
    //   let filename = `${this.context.user}_${this.context.sentence.title}_${this.context.session_ts}.wav`; 
    // }
    // let filename = `${this.context.user}_${this.context.sentence.title}_${this.context.session_ts}.wav`; 
    let filename = `${this.context.user}${target_team}_${this.context.sentence.title}_${this.context.session_ts}.wav`; 
    return new Promise((resolve, reject) => {
      try {
        console.log(
          `Sending start message to WS for pid ${parThis.state.pid}`,
        );
        parThis.webSocket.send(
          JSON.stringify({
            control: 'start',
            filename: filename,
            // channels: parThis.channels.toString(),
          }),
        );
        resolve(true);
      } catch (e) {
        reject(e);
      }
    });
  }

  async sendStop() {
    const parThis = this;
    let header = createHeader(this.sampleRate, this.recording_length);
    // console.log('Header', header);
    console.log('Recording Length', this.recording_length);

    return new Promise((resolve, reject) => {
      console.log('Sending stop message to WS');
      parThis.webSocket.send(JSON.stringify({ control: 'header' }));
      parThis.webSocket.send(header);
      parThis.webSocket.send(JSON.stringify({ control: 'end' }));
    });
  }

  async sendStopNoHeader() {
    const parThis = this;
    // let header = createHeader(this.APIsampleRate, this.recording_length);
    // console.log('Header', header);
    console.log('Recording Length', this.recording_length);

    return new Promise((resolve, reject) => {
      console.log('Sending stop message to WS');
      // parThis.webSocket.send(JSON.stringify({ control: 'header' }));
      // parThis.webSocket.send(header);
      parThis.webSocket.send(JSON.stringify({ control: 'end' }));
    });
  }

  closeSocket() {
    const parThis = this;
    return new Promise((resolve, reject) => {
      parThis.webSocket.send(JSON.stringify({ control: 'close' }));
      parThis.webSocket.close();
    });
  }

  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  streamAudio() {
    // const [ stream ] =  this.state.stream;
    // if (stream) {
    // console.log("StramAudio")
    if (this.props.recording === true) {
      this.getChunk();
    }
    // } else {
    //   // We need sendStop() in order to send all segments back
    //   this.sendStop();
    //   clearInterval(this.streamAudioHandle);
    // }
  }


  convertFloat32ToInt16(buffer) {
    let l = buffer.length;
    const buf = new Int16Array(l);
    while (l--) {
      buf[l] = Math.min(1, buffer[l]) * 0x7fff;
    }
    return buf.buffer;
  }

  int16ToFloat32(inputArray, startIndex, length) {
    var output = new Float32Array(inputArray.length-startIndex);
    for (var i = startIndex; i < length; i++) {
        var int = inputArray[i];
        // If the high bit is on, then it is a negative number, and actually counts backwards.
        var float = (int >= 0x8000) ? -(0x10000 - int) / 0x8000 : int / 0x7FFF;
        output[i] = float;
    }
    return output;
  }

  
  async getChunk() {    
  // console.log('gettingchunk')
  // const { recorder } = this.state;
  const recorder=this.recorder;
  let parThis = this;
  if (recorder == null) {
    console.log('recorder is null')
    return;
  }
  let buffer = null;
  
  recorder.ongetbuffer = function( typedArray ){
    // console.log('recorder buffer: ',typedArray);
      buffer = typedArray;
  }
  await recorder.getBuffer();
  // console.log('chunk buffer: ',buffer);
  const downsampledBuffer = downsampleBuffer(
    buffer,
    this.sampleRate,
    this.APIsampleRate,
  );
  // const convertedBuffer = this.convertFloat32ToInt16(downsampledBuffer);
  const convertedBuffer = buffer;
  if (this.webSocket === null || this.webSocket.readyState === WebSocket.CLOSED) {
    this.initRecording();
    notification.error({
      message: 'WebSocket is closed',
    });
    console.log('websocket null')
    return;
  }
  if (convertedBuffer.byteLength > 0) {
    console.log('about to send')
    this.webSocket.send(convertedBuffer);
    // if (this.use_chunks) {
    this.chunk = concatenate(Uint8Array, this.chunk, convertedBuffer);
    // }
    this.recording_length += convertedBuffer.byteLength;    
  }
  else{}
  }

  initRecording() {
    console.log("initializing recorder")
    clearInterval(this.streamAudioHandle);
    this.recorder = null;
    this.setState({
      // recorder: null,
      stream: null,
      // recording: false,
      can_dl_recs: false,
    });
  }

  // async waitForRecs(){
  //   if (this.can_dl_recs){
  //     console.log("we can now dl recs")
  //   }
  // }
  
  
  ensureCanDlRecs(timeout) {
    const parThis= this;
    var start = Date.now();
    return new Promise(waitForFoo); // set the promise object within the ensureCanDlRecs object

    // waitForFoo makes the decision whether the condition is met
    // or not met or the timeout has been exceeded which means
    // this promise will be rejected
    function waitForFoo(resolve, reject) {
      const {can_dl_recs}= parThis.state;
      // console.log("can dl: ",can_dl_recs)
        if (can_dl_recs){
            resolve(can_dl_recs);
            console.log("We can now download the recording!");
        }
        else if (timeout && (Date.now() - start) >= timeout)
            {
              console.log('timeout!');
              parThis.closeSocket();
              reject(new Error("timeout"));
              
            }
        else
            setTimeout(waitForFoo.bind(this, resolve, reject), 200);
    }
  }
  
  

  async stopRecord() {

    
    await this.timeout(250); // setting time out to receive last chunk
    const {  stream } = this.state;
    const recorder=this.recorder;
    // await this.getChunk();
    if (this.use_chunks) 
    {
      console.log("using chunks")
      // const { buffer } = await recorder.stop();
      // this.chunk = concatenate(Float32Array, this.chunk, buffer[0]);

      // this.sendStop();
      // this.initRecording();


   
      // console.log("using chunks")
      var fileName = new Date().toISOString() + ".wav";

 
      await this.getChunk();
      await recorder.stop();

      let header = createHeader(this.sampleRate, this.recording_length);
      this.chunk=concatenate(Uint8Array, header,this.chunk);
      // let audioBlob = exportBuffer(this.chunk, this.sampleRate);
      var dataBlob = new Blob( [this.chunk], { type: 'audio/wav' } );
      var fileName = new Date().toISOString() + ".wav";
      var url = URL.createObjectURL( dataBlob );

      var audio = document.createElement('audio');
      audio.controls = true;
      this.audioRef.src = url;
      this.audioRef.load();
      
     
      await recorder.close();
      this.sendStop();
      var timeout=600000;
      await this.ensureCanDlRecs(timeout);
      clearInterval(this.pingInterval);
      this.props.handleUpload(false);
      
      this.closeSocket();
      this.initRecording();
    
 



    }

    if (!this.use_chunks){
      this.props.handleUpload(true);
      const parThis=this;
      console.log("not using chunks")
      recorder.ondataavailable = function( typedArray ){
        console.log('recorder buffer: ',typedArray);
        var dataBlob = new Blob( [typedArray], { type: 'audio/wav' } );
          var fileName = new Date().toISOString() + ".wav";
          var url = URL.createObjectURL( dataBlob );

          var audio = document.createElement('audio');
          audio.controls = true;
          parThis.audioRef.src = url;
          parThis.audioRef.load();
          parThis.chunk = typedArray;
      }
      // await recorder.stop();
      
      // await this.getChunk();
      await recorder.stop();
      // console.log("Recorded Buffer: ", buffer);
      await recorder.close();
      // this.chunk=this.int16ToFloat32(this.chunk,44,this.chunk.length);
      console.log("buffer to 32float:",this.chunk);
      const downsampledBuffer = downsampleBuffer(
        this.chunk,
        this.sampleRate,
        this.APIsampleRate,
      );
      const convertedBuffer = this.chunk;
      // console.log('downsampled buffer: ', downsampledBuffer);
      // const convertedBuffer = this.convertFloat32ToInt16(downsampledBuffer);
      if (this.webSocket === null || this.webSocket.readyState === WebSocket.CLOSED) {
        this.initRecording();
        notification.error({
          message: 'WebSocket is closed',
        });
        console.log('websocket null')
        return;
      }
      if (convertedBuffer.byteLength > 0) {
        // console.log('about to send')
        this.webSocket.send(convertedBuffer);
        this.recording_length += convertedBuffer.byteLength;     
      }
      this.sendStopNoHeader();
      var timeout=600000;
      await this.ensureCanDlRecs(timeout);
      clearInterval(this.pingInterval);
      this.props.handleUpload(false);
      this.closeSocket();
      this.initRecording();
      
    }

    if (stream) {
      stream.getTracks().forEach((track) => {
        track.stop();
        stream.removeTrack(track);
      });
    }

    if (!this.get_recs_from_backend) {
      
      this.audioBlob = exportBuffer(this.chunk, this.sampleRate);
      this.audioRef.src = URL.createObjectURL(this.audioBlob);
      this.audioRef.load();
    }
    if (!this.get_recs_from_backend) {

      // Get audio file from backend
      let filename = this.context.user + '_' + this.context.sentence.title + '_' + this.context.session_ts;
      var timeout=100000;
      await this.ensureCanDlRecs(timeout);
      // console.log("We can now download the recording!");
      console.log("Starting to download...");
      let test_audio = await getRecording(filename);
      this.closeSocket();
      test_audio = test_audio[0];
      var audio = new Audio();
      audio.id = `${test_audio.id}_audio`;
      audio.src = `data:audio/wav;base64,${test_audio.src}`;

      // Load audio and create ref
      window[`${test_audio.id}_audio`] = audio;
      // this.setState({test_recording:test_audio});

      const b64toBlob = (b64Data, contentType='', sliceSize=512) => {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];
      
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          const slice = byteCharacters.slice(offset, offset + sliceSize);
      
          const byteNumbers = new Array(slice.length);
          for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }
      
          const byteArray = new Uint8Array(byteNumbers);
          byteArrays.push(byteArray);
        }
      
        const blob = new Blob(byteArrays, {type: contentType});
        return blob;
      }
      // data:audio/wav;base64
      const contentType = 'audio/wav';
      const blob = b64toBlob(test_audio.src, contentType);

      const blobUrl = URL.createObjectURL(blob);
      this.audioRef.src = blobUrl;
      this.audioRef.load()

      console.log('Frontend audio', this.chunk.length);
      console.log('Backend audio', test_audio.src.length);
      // this.chunk.length = test_audio.src.length;

      // console.log(blob.size); // backend 
      // console.log(this.audioBlob.size); // frontend 
    }

  }


    
    render() {

      const listen = this.props.listen;
      const recording = this.props.recording;
      const handleClick = this.props.handleClick;
        // console.log("Recording set to:",recording);
        // console.log("Listen set to:",listen);
      
        // Don't show record button if their browser doesn't support it.
        return (
          <span>
            <span className="mic-icon" onClick={handleClick}>
              <AudioOutlined size="normal"

              style={recording ? (
                {fontSize: '70px', color: '#FF5733', paddingLeft: "10px"}
              ) : 
                ( {fontSize: '70px', color: '#4E4E4E', paddingLeft: "10px"})
              }
              ></AudioOutlined>
            </span>
            {listen ? 
              <span className="audio-controls">
                <audio ref={(audioRef)=>{this.audioRef=audioRef;}} controls /> 
              </span>: null
            }
          </span>
      )
  }
}
RecorderComponent.contextType = SentencesContext;

export default RecorderComponent;

