import React, { Component } from "react";
import 'antd/dist/antd.css';
import {
         Spin, Alert, ButtonToolbar, Button, notification, Layout, Typography, Card, Row, Col, message, Modal, Carousel,Image
        } from 'antd';
import { AudioOutlined, SoundOutlined , RightCircleOutlined, PauseCircleOutlined, LogoutOutlined, NodeIndexOutlined, InfoCircleOutlined, SlidersTwoTone} from '@ant-design/icons';
import {LeftOutlined, RightOutlined} from '@ant-design/icons';
import { SentencesContext, sentences, stories } from "./globals";
const {Header, Footer, Sider, Content} = Layout; 
const {Title} = Typography;
import './recorder.css';
import RecorderComponent from "./RecoderComponent";
import {downloadPDF, getRecording} from '../../utils/utils';

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      // style={{ ...style, display: "block", background: "black" }}
      style={{ ...style, color: "black", fontSize: '100px',lineHeight: '1.5715' }}
      onClick={onClick}
    >
    <RightOutlined/>
    </div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, color: "black", fontSize: '100px',lineHeight: '1.5715' }}
      onClick={onClick}
    >
    <LeftOutlined />
    </div>
  );
}

class ImagesUIComponent extends Component {
    // static contextType = context;
    constructor(props,context) {
        
        super(props,context);
        this.state = {
            stream: null,
            recording: false,
            recorder: null,
            listen: false,
            socketReady: false,
            loading: false,
            countdown_over: false,
            isModalVisible: false,
            uploading: false,
        };
        this.saveFile = this.saveFile.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleUpload = this.handleUpload.bind(this);
        this.sampleRate = 44100;
        this.sampleRateFinal = 22050;
        this.APIsampleRate = 22050;
        this.channels = 1;
        

        this.images = [];
        this.carousel_photos = [];
        // this.quote='';
        this.instructions = ''; 
        this.story_audio = ''; 
        this.questions_list=[];

        this.thumbnail_height = 300;
        this.thumbnail_width = 500;
        this.recorderRef = React.createRef();
        this.get_recs_from_backend=true;
        this.recording_length = 0;
        this.first_time_render = true;

        this.quote = this.context.sentence ? this.context.sentence.quote : ''; 
        // const remaining_words = this.context.sentence ? `${this.context.sentence.id} / ${this.context.number_of_stories}` : ''; 
        // let photos = ''; 
        
        var carousel_settings = {
          autoplay:false,
          dots: false,
          arrows: true,
          accessibility: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />
        };

        if (this.context.sentence.type=="single_image"){
          let photos_sorted = this.context.photos.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
          console.log(photos_sorted.length);
          photos_sorted.forEach( (photo) => {
                this.images.push({
                                    original : `data:image/png;base64,${photo.src}`,
                                    thumbnail: `data:image/png;base64,${photo.src}`,
                                    humbnailWidth: 500,
                                    thumbnailHeight: 400,
                            });
            });          
        }
        if (this.context.photos && this.context.photos.length > 1 && this.context.sentence.type== "multiple_images") {
          let photos_sorted = this.context.photos.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
          console.log(photos_sorted.length);
          photos_sorted.forEach( (photo) => {
                this.images.push(
                                    // original : `data:image/png;base64,${photo.src}`,
                                    // thumbnail: `data:image/png;base64,${photo.src}`,
                                    // thumbnailWidth: 340,
                                    // thumbnailHeight: 200,
                                    // thumbnailCaption: photo.id
                                    <div className="image-wrap">
                                    <Image className="gallery-image" height="100%" src={`data:image/png;base64,${photo.src}`} />
                                    </div>
                            );
            });
        }

        if (this.context.sentence.type == "carousel_images"){
          this.carousel_photos = this.context.photos.sort((a,b) => (parseInt(a.id) > parseInt(b.id)) ? 1 : ((parseInt(b.id) > parseInt(a.id)) ? -1 : 0));
        }

        if (this.context.sentence.type=="questions" || this.context.sentence.type=="questions_two_col" ){
            const mylist = this.context.sentence.questions;
            console.log('questions:',mylist);
            console.log('questions:',this.context.sentence.questions);
            this.questions_list = mylist.map(question=><li>{question}</li>);

        }
                
        

        if (this.context.instructions && this.context.instructions.length > 0) {
          let instructions_sorted = this.context.instructions.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
          this.instructions = []; 
          instructions_sorted.forEach( (i) => {
              var audio = new Audio();
              audio.id = `${i.id}_audio`;
              audio.src = `data:audio/mp3;base64,${i.src}`;
              window[`${i.id}_audio`] = audio;
              this.instructions.push(<SoundOutlined style={{ fontSize: '75px', color: '#4E4E4E', paddingTop:20, paddingLeft:26}}  onClick={ () => this.startStopAudio(window[`${i.id}_audio`]) }/>);
          });
      }

      if (this.context.story_audio && this.context.story_audio.length > 0) {
        let story_audio_sorted = this.context.story_audio.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
        this.story_audio = []; 
        story_audio_sorted.forEach( (i) => {
            var audio = new Audio();
            audio.id = `${i.src}_story_audio`;
            audio.src = `data:audio/wav;base64,${i.src}`;
            window[`${i.src}_story_audio`] = audio;
            this.story_audio.push(<SoundOutlined style={{ fontSize: '75px', color: '#39ac73' }}  onClick={ () => this.startStopAudio(window[`${i.src}_story_audio`]) }/>);
        });
      }           
      
      console.log(this.context.photos);

      // Here we decide what to display in the Content section: either a single image, a gallery or a carousel
      this.picture='';
      if (this.context.sentence.type=="single_image"){
        this.picture=<div  className="picture-wrap"><img className="img div100height" src={`${this.images[0].original}`} style={{ height:"100%"}}/></div>;
      }
      
      else if (this.context.sentence.type == "carousel_images" ){
        this.picture=<div className="div100height"><Carousel className="div100height" arrows {...carousel_settings}>
            {this.carousel_photos.map(item => 
              <div  className="carousel div100height"><img className="carousel-image" src={`data:image/png;base64,${item.src}`} /></div>
            )}
          </Carousel></div>
      }
      else if (this.context.sentence.type=="questions"){
        {if (this.questions_list.length>1){
          this.picture=<div id="question_list" className="gradient-list" id={'parent_question_'+this.state.recorder_id}> 
            {
            
            <ul className='no-styled-bullets' id={'question_'+this.state.recorder_id}>{this.questions_list}</ul> 
            
            }  
            
            </div> }
        
        }
      }
      else if (this.context.sentence.type=="questions_two_col"){
        {if (this.questions_list.length>1){
          this.picture=<div id="question_list" className="gradient-list" id={'parent_question_'+this.state.recorder_id}> 
            {
            
            <div><br/><br/><ul className="two-cols" >{this.questions_list}</ul></div>
            }  
            
            </div> }
        
        }
      }


      else {
          console.log('type else');
        this.picture=<div className="image-container">{this.images}</div>;
      }


    }
// rename save file to something more apt? doesn't save anything
    async saveFile() {
        this.setState({ loading: true });
        if (this.audioRef){
          this.audioRef.removeAttribute('src');
          this.audioRef.load();
        }
        await this.props.goToPage(this.context.currentPage);
        this.setState({ loading: false, listen: false });
    }
    startStopAudio(audio) {
      console.log("aud", audio);
        if (audio.paused) {
            audio.play();
        }else {
            audio.pause();
            audio.currentTime = 0;
        }

    }

    componentDidMount() {
    
    
    
    }



    async triggerRecord(){
      this.state.recording ? this.recorderRef.current.stopRecord() : this.recorderRef.current.startRecord();
    }

    async handleClick() {
      const { recording, listen, uploading } = this.state;
      const error = (uploading) => {
        if (!uploading){
          message.error('Πρέπει πρώτα να ολοκληρώσετε την ηχογράφηση');
        }
        
      };

      if (!uploading) {

        await this.triggerRecord();

        this.setState(state => ({
          recording: !state.recording,
          listen: true,
          countdown_over: false,
        }));}
      else { error(uploading); }
    }

    async handleUpload(uploading){
      console.log('uploading in recorderComponent: ',uploading);



      this.setState({uploading:uploading});
    }

    render() {
      const { recording, listen, uploading } = this.state;

      

      

      const error = (uploading) => {
        if (!uploading){
          message.error('Πρέπει πρώτα να ολοκληρώσετε την ηχογράφηση');
        }
        else {
          message.error('Περιμένετε να σταλθεί η ηχογράφηση');
        }
      };

      const success = () => {
        message.success('Έχετε παύσει τη χορήγηση');
      };

      const showModal = () => {
        this.setState({isModalVisible: true});
      };
    
      const handleOk = () => {
        this.setState({isModalVisible: false});
      };
      
      const handleCancel = () => {
        this.setState({isModalVisible: false});
      };

      
      

        // Don't show record button if their browser doesn't support it.
        return (
          <Spin className="spin" spinning={this.state.loading}>
            <Layout className="layout" style={{background:"white"}}>
              <Image.PreviewGroup>

                
                <Header style={{ background: "white", width:"vw", height:"150px"}}>                   
                  <Row style={{ alignItems: "center" }} >
                    <Col span={4} />
                    <Col span={14}>
                      <Title level={3} style={{color: '#595959', paddingTop:20}}>{this.quote}</Title>
                    </Col>
                    <Col span={6}>
                      {this.instructions}
                    </Col>
                  </Row>
                </Header>
                <Content >
          {/* footer_height=130, header_height=150 */}
                {/* <Col span={2}/> */}
                <Row>
                {/* <Col span={4} /> */}
                <Col span={18} flex={10}>
                <div style={{height: "calc(100vh - 130px - 150px)", width: "90vw", margin:"auto"}}>
                  {/* <div className="div100height"> */}

                    {this.picture}

                   
                  {/* </div> */}
                </div>
                </Col>
                <Col span={6} flex={6}>
                {/* <div style={{height: "calc(100vh - 130px - 150px)", width: "10vw", margin: "auto"}}> */}
                <div className="div100height">
                    {/* <div className="div100height"> */}

                      {this.story_audio}
                      </div>
                {/* </div> */}
                </Col>
                  
                </Row>
                </Content>
                <Footer style={{height: "130px", background: "white"}}>
                  <div>
                    <Row>
                    <Col span={2}/>
                    <Col span={22}>
                      <LogoutOutlined 
                        onClick={()=> {window.location.reload()}}
                        className="icons"
                        style={{fontSize: '64px', color: '#FF5733', paddingBottom:20}} 
                      />
                      <InfoCircleOutlined 
                        className="icons"
                        onClick={showModal}
                        style={{fontSize: '64px', color: '##005077', paddingBottom:20}}                 
                      />
                    
                      <RightCircleOutlined 
                        className="icons"
                        onClick={() => {
                          console.log("can_dl_recs: ",uploading);
                          (listen && !recording && !uploading) ? this.saveFile(): error(uploading); }}
                        style={(listen && !recording && !uploading)?(
                          {fontSize: '64px', color: '#008000', paddingBottom:20}
                        ):(
                          {fontSize: '64px', paddingBottom:20}
                        )}
                      />
                      <span>
                      <RecorderComponent 
                        ref={this.recorderRef}
                        recording={recording}
                        listen={listen}
                        handleClick={this.handleClick}
                        handleUpload={this.handleUpload}
                        >
                      </RecorderComponent>
                      </span>
                    </Col>
                    </Row>
                  </div>
                </Footer>
              </Image.PreviewGroup>
            </Layout>
            <Modal title="Οδηγίες χορήγησης-εκμαίευσης λόγου" 
                  visible={this.state.isModalVisible} 
                  onOk={handleOk} 
                  onCancel={handleCancel}
                  width={1000}>
                  {(this.context.sentence.id==0)? 
                    <div>
                      <p>
                        <button onClick={() => {downloadPDF('instructions.pdf')}}>
                          Download Pdf
                        </button>                        
                      </p>
                    </div>
                  :null}
                  {(this.context.sentence.id==1)? 
                    <div>
                      <p> «Θυμάστε τι έγινε τη μέρα που πάθατε το εγκεφαλικό;» <br/> Αν ναι: <br/> «Πείτε μου λοιπόν, τι συνέβη όταν πάθατε το εγκεφαλικό;» </p>
                      <p> Αν βλέπουμε ότι ο συμμετέχων ανταποκρίνεται και ολοκληρώνει τη δοκιμασία χωρίς να κουράζεται, μπορούμε να ρωτήσουμε και κάτι ακόμα: 
                          <br/>«Θα μπορούσατε να μου πείτε κάποια ιστορία για κάποιο σημαντικό γεγονός που έγινε παλιότερα στη ζωή σας; 
                          Μπορεί να είναι καλό ή κακό, μπορεί να είναι πολύ παλιό, από τα παιδικά σας χρόνια ή πιο πρόσφατο». </p> 
                      <p> 1. Αν όχι (αν δεν θυμάται τι συνέβη με το εγκεφαλικό) ρωτάμε:  <br/>
                          «Ποια είναι τα πρώτα πράγματα που θυμάστε μετά το εγκεφαλικό; Τι θα μπορούσατε να μου πείτε γι’ αυτό;» </p>
                      <p> 2. Αν καμία απάντηση (για περίπου 10 δευτερόλεπτα):  <br/>
                          «Πείτε μου, πώς άλλαξε η ζωή σας μετά το εγκεφαλικό; Πώς άλλαξε η καθημερινότητά σας;»  
                          «Κάνατε κάποιες θεραπείες μετά το εγκεφαλικό; Μπορείτε να μου μιλήσετε γι’ αυτές;»  </p>
                      <p> 3. Αν καμία απάντηση (για περίπου 10 δευτερόλεπτα):  <br/>
                          «Θα μπορούσατε να μου πείτε κάποια ιστορία για κάποιο σημαντικό γεγονός που έγινε παλιότερα στη ζωή σας; 
                          Μπορεί να είναι καλό ή κακό, μπορεί να είναι πολύ παλιό, από τα παιδικά σας χρόνια ή πιο πρόσφατο». </p>
                      <p> 4. Αν καμία απάντηση (για περίπου 10 δευτερόλεπτα): <br/>
                          «Για παράδειγμα, θα μπορούσατε να μου πείτε για κάποιο ταξίδι που πήγατε ή κάτι σχετικό με την οικογένειά σας ή την εργασία σας»  </p>
                      <p> 5. Αν καμία απάντηση, προχωράμε στις επόμενες δοκιμασίες. </p>
                      <p> Σε κάθε ερώτηση, ενθαρρύνουμε ολοκληρωμένες απαντήσεις. </p>
                      <p> Αν η δοκιμασία γίνεται σε υγιείς ομιλητές, τότε ζητάμε να μας αφηγηθούν ένα περιστατικό σχετικό με την υγεία τους: 
                          «Μπορείτε να μας αφηγηθείτε κάποιο ατύχημα που έχετε πάθει ή οποιοδήποτε περιστατικό σχετικό με την υγεία σας 
                          που σας έχει συμβεί στο παρελθόν;» </p> 
                    </div>
                  :null}
                  {(this.context.sentence.id==2)? 
                    <div>
                      <p> Δείχνουμε τις εικόνες στην οθόνη. </p>
                      <p> «Οι εικόνες αυτές δείχνουν μια ιστορία. Δείτε τις με τη σειρά και μετά θα σας ζητήσω να μου πείτε αυτή την ιστορία με αρχή, μέση και τέλος. 
                          Μπορείτε να κοιτάζετε τις εικόνες, καθώς λέτε την ιστορία.»  </p> 
                      <p> Αφήνουμε λίγο χρόνο να δει τις εικόνες.  </p>
                      <p> 1. Αν καμία απάντηση (για περίπου 10 δευτερόλεπτα): <br/>
                          (Δείχνουμε την πρώτη εικόνα) «Κοιτάξτε αυτή την εικόνα και πείτε μου τι πιστεύετε ότι συμβαίνει.»   </p>
                      <p> 2. Αν καμία απάντηση, ρωτάμε: <br/> «Τι μπορείτε να μου πείτε γι’ αυτή την εικόνα;»  </p>
                      <p> 3.Αν καμία απάντηση, ρωτάμε: <br/> «Τι δίνει η μητέρα στο αγόρι;» <br/> Αν δεν απαντήσει, απαντάμε εμείς: <br/>
                          «Του δίνει μια ομπρέλα. Την πήρε το αγόρι την ομπρέλα;» <br/> Αν δεν απαντήσει απαντάμε εμείς: <br/>
                          «Όχι, δεν την πήρε. Τι έγινε μετά;»   </p>
                      <p> Το ίδιο κάνουμε για κάθε μια από τις εικόνες της ιστορίας.  </p>
                      <p> Εάν σε κάποια από τις παραπάνω προσπάθειες εκμαίευσης λόγου (1-3) μας απαντήσει σωστά, 
                          στη συνέχεια σταματάμε να βοηθάμε και επιχειρούμε να εκμαιεύσουμε την ιστορία από αυτό 
                          το σημείο και μετά, ρωτώντας:  </p>
                      <p> «Πολύ ωραία. Πώς συνεχίζεται τώρα η ιστορία;» ή «Τι συνέβη/τι έγινε μετά;» </p> 
                    </div>
                  :null}
                  {(this.context.sentence.id==3)? 
                    <div>
                      <p> Δείχνουμε την εικόνα στην οθόνη.  </p>
                      <p> «Εδώ έχουμε μια άλλη εικόνα. Κοιτάξτε όλα όσα συμβαίνουν σε αυτή την εικόνα και πείτε μου μια ιστορία γι’ αυτά που βλέπετε. 
                          Πείτε μου την ιστορία με αρχή, μέση και τέλος.»  </p> 
                      <p> 1. Αν καμία απάντηση (για περίπου 10 δευτερόλεπτα): <br/> «Κοιτάξτε εδώ (δείχνουμε το τμήμα της εικόνας που 
                          δείχνει τη γάτα ανεβασμένη πάνω στο δέντρο και το κοριτσάκι που προσπαθεί να την πιάσει). 
                          Τι συμβαίνει εδώ; Ξεκινήστε την ιστορία από αυτό το σημείο.»  </p>
                      <p> 2. Αν καμία απάντηση (για περίπου 10 δευτερόλεπτα), ρωτάμε: <br/>
                          «Βλέπετε τη γάτα που έχει κολλήσει πάνω στο δέντρο; Τι συνέβη;»   </p>
                      <p> 3. Αν απαντήσει στο βήμα 1 με λιγότερες από 2 προτάσεις (δηλαδή αν απαντήσει στη μεμονωμένη ερώτηση και μετά σταματήσει), 
                          παρακινούμε να συνεχίσει: <br/> «Πολύ ωραία. Συνεχίστε τώρα την ιστορία από αυτό το σημείο. 
                          Τι άλλο συνέβη;/ Τι άλλο μπορείτε να μου πείτε γι’ αυτή την ιστορία;»  </p>
                      <p> Συνεχίζουμε με τον ίδιο τρόπο, δείχνοντας, διαδοχικά, διάφορα σημεία της εικόνας. 
                          Αν απαντήσει, παρακινούμε να συνεχίσει την ιστορία με τα υπόλοιπα γεγονότα 
                          («Συνεχίστε την ιστορία από αυτό το σημείο»). Αν δεν απαντήσει, εκμαιεύουμε μεμονωμένες απαντήσεις.  </p> 
                    </div>
                  :null}
                  {(this.context.sentence.id==4)? 
                    <div>
                      <p> «Θα σας ζητήσω τώρα να μου πείτε μια ιστορία. 
                          Έχετε ακούσει για το παραμύθι της Σταχτοπούτας; 
                          Το θυμάστε καθόλου; Οι εικόνες που θα δείτε θα σας βοηθήσουν να το θυμηθείτε. 
                          Κοιτάξτε πρώτα όλες τις εικόνες με τη σειρά, για να θυμηθείτε την ιστορία όσο καλύτερα μπορείτε. 
                          Μετά, θα σας ζητήσω να μου πείτε αυτή την ιστορία με δικά σας λόγια, χωρίς να βλέπετε τις εικόνες.»  </p>
                      <p> Αν μας απαντήσει ότι δεν ξέρει το παραμύθι, τότε ζητάμε να μας πει ένα παραμύθι που γνωρίζει. <br/> Ενδεικτικά 
                        <ul>
                          <li>Η Κοκκινοσκουφίτσα </li>
                          <li>Η Χιονάτη και οι 7 νάνοι  </li>
                          <li>Η ωραία κοιμωμένη</li>
                          <li>Τα τρία γουρουνάκια</li>
                          <li>Ο λύκος και τα 7 κατσικάκια</li>
                        </ul>
                      </p> 
                      <p> “Ξεφυλλίζουμε” το βιβλίο στην οθόνη πατώντας τα βελάκια δεξιά και αριστερά, για να δει όλες τις εικόνες. 
                          Αφήνουμε όσο χρόνο χρειάζεται. Δεν φρεσκάρουμε τη μνήμη του σχετικά με την ιστορία.  <br/> Όταν τελειώσει λέμε: </p>
                      <p>
                          «Τώρα πείτε μου όσα μπορείτε περισσότερα από την ιστορία της Σταχτοπούτας. 
                          Μπορείτε να χρησιμοποιήσετε όσες λεπτομέρειες θέλετε από αυτά που θυμάστε ή από τις εικόνες που είδατε.» </p>
                      <p> Αν απαντήσει με λιγότερες από 3 προτάσεις και μετά δείχνει να κολλάει, ρωτάμε: <br/>
                          «Πολύ ωραία. Τι έγινε μετά;» ή «Συνεχίστε την ιστορία.»  </p>
                      <p> Το ίδιο κάνουμε σε κάθε σημείο που δείχνει να “κολλάει”. 
                          Εναλλακτικά, χρησιμοποιούμε ερωτήσεις παρακίνησης, όπως: «Τι άλλο μπορείτε να πείτε για την ιστορία;» 
                          ή «Τι άλλο θυμάστε;» ή «Τι άλλο συνέβη;». </p>
                      <p> Αν στην πορεία δεν θυμάται κάποιο συγκεκριμένο γεγονός της ιστορίας, ενθαρρύνουμε να συνεχίσει με το επόμενο γεγονός που θυμάται:
                          «Δεν πειράζει. Συνεχίστε παρακάτω.» «Ποιο είναι το επόμενο γεγονός που θυμάστε; Συνεχίστε από εκεί.». </p>
                      <p> Αν δεν ανταποκρίνεται καθόλου στις προηγούμενες προσπάθειες παρακίνησης, κάνουμε, διαδοχικά, ερωτήσεις για διάφορα βασικά γεγονότα της ιστορίας, 
                          περιμένοντας απάντηση. Αν μας απαντήσει, παρακινούμε να συνεχίσει την ιστορία από κει και κάτω μόνος/-η του 
                          («Τώρα συνεχίστε την ιστορία από αυτό το σημείο»).  </p>
                      <p> Ενδεικτικές ερωτήσεις για το περιεχόμενο της ιστορίας:  
                        <ul>
                          <li>Πώς ξεκινάει η ιστορία;   </li>
                          <li>Ποια ήταν η Σταχτοπούτα;    </li>
                          <li>Με ποιον ζούσε;</li>
                          <li>Ποιος διοργάνωσε έναν μεγάλο χορό;  </li>
                          <li>Πήγε η Σταχτοπούτα στον χορό;  </li>
                          <li>Ποιος τη βοήθησε να πάει στον χορό;  </li>
                          <li>Πώς τη βοήθησε;  </li>
                          <li>Ποιον συνάντησε η Σταχτοπούτα στον χορό;   </li>
                          <li>Τι συνέβη στον χορό;    </li>
                        </ul>
                      </p>
                      <p> Αν δεν ανταποκριθεί σε κάποια από τις παραπάνω ερωτήσεις, δίνουμε εμείς μια πολύ σύντομη απάντηση και μετά 
                          παρακινούμε να συνεχίσει από εκεί και κάτω. </p> 
                      <p> Συνεχίζουμε μέχρι να ολοκληρώσει την ιστορία ή μέχρι το σημείο που είναι σαφές ότι δεν μπορεί να συνεχίσει. </p>
                    </div>
                  :null}
                  {(this.context.sentence.id==5)? 
                    <div>
                      <p> «Θα σας δείξω τώρα μια σειρά από εικόνες. Αυτές οι εικόνες δείχνουν μια ιστορία.»    </p>
                      <p> Το Πάρτι έχει ως ερεθίσματα 6 εικόνες. Στην οθόνη οι εικόνες φαίνονται αρκετά μικρές. 
                          Πατώντας πάνω τους, μεγαλώνουν και ο συμμετέχων μπορεί να τις δει διαδοχικά σε μεγάλο μέγεθος. 
                          Δείχνουμε τις εικόνες, τη μία μετά την άλλη, πατώντας το δεξιό βελάκι. 
                          Αφήνουμε όσο χρόνο χρειάζεται για να δει τις εικόνες και να καταλάβει τι συμβαίνει σε αυτές.  <br/> 
                          Μετά, κλείνουμε το παράθυρο με το Χ. 
                      </p> 
                      <p> «Τώρα θα ήθελα να μου πείτε την ιστορία που συμβαίνει σε αυτές τις εικόνες με αρχή, μέση και τέλος. 
                          Μπορείτε να κοιτάζετε τις εικόνες, καθώς λέτε την ιστορία.» </p>
                      <p> 1. Αν καμία απάντηση (για περίπου 10 δευτερόλεπτα): <br/> 
                        (Δείχνουμε την πρώτη εικόνα) «Κοιτάξτε αυτή την εικόνα. Τι πιστεύετε ότι συμβαίνει;»  </p>
                      <p> 2. Αν καμία απάντηση, ρωτάμε: <br/> 
                          «Τι μπορείτε να μου πείτε γι’ αυτή την εικόνα;»  </p>
                      <p> Αν καμία απάντηση, ρωτάμε: <br/> «Πού βρίσκεται αυτός ο νεαρός;» <br/> «Τι συμβαίνει στο διπλανό διαμέρισμα;» <br/> Αν δεν απαντήσει, απαντάμε εμείς:<br/>  
                          «Βρίσκεται στο διαμέρισμά του.» <br/> «Στο διπλανό διαμέρισμα γίνεται ένα πάρτι.»  </p>
                      <p> Τα ίδια βήματα ακολουθούμε για κάθε μια από τις εικόνες της ιστορίας. </p>
                      <p> Εάν σε οποιοδήποτε από τα παραπάνω βήματα - προσπάθειες εκμαίευσης λόγου (1-3) μας απαντήσει σωστά, 
                          δεν συνεχίζουμε με τις υπόλοιπες ερωτήσεις, αλλά επιχειρούμε να εκμαιεύσουμε την ιστορία από αυτό το σημείο και μετά, ρωτώντας: 
                          «Πολύ ωραία. Πώς συνεχίζεται τώρα η ιστορία;» ή «Τι συνέβη/τι έγινε μετά;» 
                      </p>
                    </div>
                  :null}
                  {(this.context.sentence.id==6)? 
                    <div>
                      <p> Στην ιστορία του Δαχτυλιδιού το κύριο ερέθισμα είναι η ηχογραφημένη ιστορία που ο συμμετέχων ακούει και πρέπει να επαναφηγηθεί. 
                          Οι εικόνες λειτουργούν βοηθητικά, κυρίως για να μπορέσει, καθώς κοιτάζει τις εικόνες, να θυμηθεί τα κύρια γεγονότα της ιστορίας, 
                          όταν τα επαναφηγείται.      </p>
                      <p> «Θα ακούσετε τώρα μια ιστορία και στη συνέχεια θα σας ζητήσω να την επαναλάβετε. 
                          Οι εικόνες που θα σας δείξω θα σας βοηθήσουν να τη θυμηθείτε.» 
                      </p> 
                      <p> Πατώντας πάνω στις εικόνες που εμφανίζονται στην οθόνη, αυτές μεγαλώνουν και μπορούμε να τις δούμε διαδοχικά, πατώντας τα βελάκια. 
                          Δείχνουμε τις εικόνες, τη μία μετά την άλλη, σε μεγάλο μέγεθος. Κλείνουμε το παράθυρο με το Χ.  </p>
                      <p> «Ακούστε τώρα προσεκτικά, για να μπορέσετε μετά να επαναλάβετε την ιστορία.»  </p>
                      <p> Βάζουμε να ακουστεί η ηχογραφημένη ιστορία (πατώντας το πράσινο μεγάφωνο).   </p>
                      <p> «Τώρα επαναλάβετε την ιστορία όσο το δυνατόν πιο ολοκληρωμένα. Μπορείτε να κοιτάζετε τις εικόνες, όσο θα αφηγείστε την ιστορία».   </p>
                      <p> Αν απαντήσει με λιγότερες από 3 προτάσεις και μετά δείχνει να κολλάει, χρησιμοποιούμε τις παρακάτω ερωτήσεις για παρακίνηση: <br/>
                          «Πολύ ωραία. Τι έγινε μετά;» ή «Συνεχίστε την ιστορία.»   </p>
                      <p> Αν στην πορεία της αφήγησης δυσκολεύεται σε κάποια σημεία της ιστορίας: </p>
                      <p> 1. Χρησιμοποιούμε ερωτήσεις για παρακίνηση: 
                        <ul>
                          <li>«Τι άλλο μπορείτε να πείτε για την ιστορία;» ή  </li>
                          <li>«Τι άλλο θυμάστε;» ή  </li>
                          <li>«Τι άλλο συνέβη;» </li>
                        </ul>
                      </p>
                      <p>2. Αν δεν ανταποκρίνεται, δείχνουμε τη σχετική εικόνα, για να τον/την βοηθήσουμε να θυμηθεί: 
                        <ul>
                          <li>«Μήπως αυτή η εικόνα σας βοηθάει;» ή  </li>
                          <li>«Τι συνέβη σε αυτή την εικόνα;»  </li>
                        </ul>
                      </p>
                      <p>3. Αν δεν ανταποκρίνεται, ενθαρρύνουμε να συνεχίσει με το επόμενο γεγονός που θυμάται:  
                        <ul>
                          <li>
                          «Δεν πειράζει. Συνεχίστε παρακάτω.»  
                          </li>
                          <li> «Ποιο είναι το επόμενο γεγονός της ιστορίας που θυμάστε; Συνεχίστε από κει.» </li>
                        </ul>
                      </p> 
                      <p> 4. Αν δεν ανταποκρίνεται, στα προηγούμενα βήματα κάνουμε, διαδοχικά, ερωτήσεις για βασικά γεγονότα της ιστορίας, 
                          ακολουθώντας τις εικόνες και περιμένουμε απάντηση σε κάθε μία. Αν μας απαντήσει σε κάποια από αυτές, 
                          δεν συνεχίζουμε παρακάτω, αλλά παρακινούμε να συνεχίσει την ιστορία από κει και κάτω μόνος/-η του. 
                          («Ωραία. Συνεχίστε τώρα την ιστορία από εδώ και πέρα.»). Ενδεικτικές ερωτήσεις για το περιεχόμενο της ιστορίας: 
                          <ul>
                            <li>Πώς ξεκινάει η ιστορία; </li>
                            <li>Ποιος είναι αυτός ο νεαρός; </li>
                            <li>Ποιες είναι αυτές οι γυναίκες;  </li>
                            <li>Ποια συνάντησε ο νεαρός;  </li>
                            <li>Τι του έδωσε η κοπέλα;  </li>
                            <li>Τι συνέβη στον νεαρό όταν επέστρεψε στο παλάτι;  </li>
                            <li>Τι συνέβη με τη συνάντησή τους; </li>
                            <li>Πώς βοήθησε το πουλάκι την κοπέλα;  </li>
                            <li>Πώς τελειώνει η ιστορία;  </li>
                          </ul>
                      </p>
                      <p> 5. Αν δεν ανταποκριθεί σε κάποια από τις ερωτήσεις, δίνουμε εμείς την απάντηση και μετά παρακινούμε 
                          να συνεχίσει από εκεί και κάτω.  </p>
                      <p> Συνεχίζουμε μέχρι να ολοκληρώσει την ιστορία ή μέχρι το σημείο που είναι σαφές ότι δεν μπορεί να συνεχίσει.</p>
                    </div>
                  :null}
                  {(this.context.sentence.id==7)? 
                    <div>
                      <p> «Θα ακούσετε τώρα άλλη μια ιστορία και μετά θα σας ζητήσω να την επαναλάβετε.»  </p>
                      <p> Βάζουμε να ακουστεί η ηχογραφημένη ιστορία (πατώντας το πράσινο μεγάφωνο).  </p> 
                      <p> «Τώρα επαναλάβετε την ιστορία όσο το δυνατόν πιο ολοκληρωμένα.»  </p>
                      <p> Αν απαντήσει με λιγότερες από 3 προτάσεις και μετά δείχνει να κολλάει, ρωτάμε: <br/>
                          «Πολύ ωραία. Τι έγινε μετά;» ή «Συνεχίστε την ιστορία.»  </p>
                      <p> Το ίδιο κάνουμε σε κάθε σημείο που δείχνει να “κολλάει”. Εναλλακτικά, χρησιμοποιούμε ερωτήσεις παρακίνησης, 
                          όπως: «Τι άλλο μπορείτε να πείτε για την ιστορία;» ή «Τι άλλο θυμάστε;» ή «Τι άλλο συνέβη;».   </p>
                      <p> Αν στην πορεία δεν θυμάται κάποιο συγκεκριμένο γεγονός της ιστορίας, ενθαρρύνουμε να συνεχίσει με το επόμενο γεγονός που θυμάται: 
                          «Δεν πειράζει. Συνεχίστε παρακάτω.» «Ποιο είναι το επόμενο γεγονός που θυμάστε; Συνεχίστε από εκεί.». </p>
                      <p> Αν δεν ανταποκρίνεται καθόλου στις προηγούμενες προσπάθειες παρακίνησης, κάνουμε, διαδοχικά, ερωτήσεις 
                          για διάφορα βασικά γεγονότα της ιστορίας, περιμένοντας απάντηση. Αν μας απαντήσει, 
                          παρακινούμε να συνεχίσει την ιστορία από κει και κάτω μόνος/-η του. </p>
                      <p> Ενδεικτικές ερωτήσεις για το περιεχόμενο της ιστορίας: 
                        <ul>
                          <li>Πώς ξεκινάει η ιστορία;   </li>
                          <li>Τι πρότεινε η χελώνα στον λαγό;    </li>
                          <li>Πώς ξεκίνησε ο αγώνας;   </li>
                          <li> Τι έκανε ο λαγός στη διάρκεια του αγώνα;  </li>
                          <li> Πώς συνέχισε η χελώνα τον αγώνα;  </li>
                          <li> Τι έγινε στο τέλος της ιστορίας;  </li>
                        </ul>
                      </p>
                      <p> Αν δεν ανταποκριθεί σε κάποια από τις παραπάνω ερωτήσεις, 
                          δίνουμε εμείς μια πολύ σύντομη απάντηση και μετά παρακινούμε να συνεχίσει από εκεί και κάτω. </p>
                      <p> Συνεχίζουμε μέχρι να ολοκληρώσει την ιστορία ή μέχρι το σημείο που είναι σαφές ότι δεν μπορεί να συνεχίσει. </p>
                    </div>
                  :null}
              </Modal>
          </Spin>
      )
  }
}
ImagesUIComponent.contextType = SentencesContext;

export default ImagesUIComponent;

