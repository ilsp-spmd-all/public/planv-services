import React from "react";
import ReactDOM from "react-dom";
import { Router, Link } from "@reach/router";
import "./index.css";
import App from "./App";


ReactDOM.render(
        <div>
            <App />
        </div>,
    document.getElementById("root")

)
/*
class WebRec extends React.Component {
    render() {
        return (
            <div>
                <App/>
            </div>
        )
    }
}

ReactDOM.render(
    <Router basename="/demos">
        <div>
            <Switch>
                <Route exact path="/" component={WebRec}/>
            </Switch>
        </div>
    </Router>,
    document.getElementById("root")
);
*/
