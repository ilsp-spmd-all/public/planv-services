import ReactDOM from 'react-dom'
import React from "react";
import 'antd/dist/antd.css';
import {
         Button,
        } from 'antd';
import {WEBSOCKET_URL} from '../../utils/config.js';



class FinalComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prediction: null,
            features: {},
            ready_to_show_results: false,
            past_results: null
        };
        this.webSocket = null;
        this.nextPage = this.nextPage.bind(this);
        this.getResults = this.getResults.bind(this);
    }

    async nextPage() {
        await this.props.goToPage(this.context.currentPage);
    }

    async getResults(){
        // let results = await getPredictionAndFeatures();
        if (this.state.ready_to_show_results){
            console.log('Do not fetch same features');
            console.log('Features', this.state.features);
            return null;
        }
        try {
            // Get new PID and start sending data
            if (
                this.webSocket === null
                || this.webSocket.readyState === WebSocket.CLOSED
            ) {
                await this.openSocket();
            }
        } catch (error) {
            console.log('Websocket Error', error);
        }
    }

    closeSocket() {
        const parThis = this;
        return new Promise((resolve, reject) => {
          parThis.webSocket.send(JSON.stringify({ control: 'close' }));
          parThis.webSocket.close();
        });
    }

    openSocket() {
        const parThis = this;
        return new Promise((resolve, reject) => {
            // Ensures only one connection is open at a time
            console.log('Opening New websocket connection for resutls ...');
            if (
                parThis.webSocket !== null
                && parThis.webSocket.readyState !== WebSocket.CLOSED
            ) {
                console.log('WebSocket is already opened.');
                resolve();
            }
            // Create a new instance of the websocket
            // console.log("token:", parThis.context.token);
            parThis.webSocket = new WebSocket(
            // `ws://${parThis.state.apihostname}:8080/stream/${parThis.state.clientid}?X-Auth-Token=${parThis.state.token}`,
            `${WEBSOCKET_URL}/?token=${parThis.context.token}`,
            );
    
            /**
             * Binds functions to the listeners for the websocket.
             */
            parThis.webSocket.onopen = function openSock(event) {
                console.log(event);
                // send regular ping messages to keep websocket open
                console.log('Session is ', parThis.props.sessionId);
                parThis.webSocket.send(JSON.stringify({ control: 'results', filename: parThis.props.username, sessionId: parThis.props.sessionId}));
                parThis.pingInterval=setInterval(()=> {
                parThis.webSocket.send(JSON.stringify({ control: 'ping' }));
                // console.log("sent ping!")
                }, 20000);
        
                // parThis.setState({ socketReady: true });
                resolve(true);
                
            };
    
          parThis.webSocket.onmessage = function socketMessage(event) {
            console.log(event.data);
            // console.log(JSON.parse(event.data));
            const status = JSON.parse(event.data).status;
            const data = JSON.parse(event.data);
            //console.log('Data', data);
            //console.log('Prediction', data.severity);
            //console.log('Status', status)
           
            if (status == 'accepting') {
              
              const interval = setInterval(() => {
                if (parThis.recorder!==null) {
                  // parThis.counter = 0;
                  // parThis.webSocket.send(parThis.header);
                  // console.log("OpusRecorderr:", parThis.recorder);
                  
                  // console.log('encoderPath: ',parThis.recorder.config.encoderPath);
                  parThis.recorder.start();
                  
                  
                  
                  // Stream first segment and then start interval
                  if (parThis.use_chunks){
                  // parThis.streamAudio();
                  
                  parThis.streamAudioHandle = setInterval(parThis.streamAudio, 2000);
                  }
                  clearInterval(interval);
                }
              }, 20);
            }
            if (status == 'results') {
                console.log('Changing state');
                parThis.setState({
                    prediction: data.severity,
                    features: data.features,
                    ready_to_show_results: true, 
                    past_results: data.past_results
                });
                console.log(data.past_results);
                parThis.closeSocket();

            }
            if (status == 'received') {
              console.log("status update")
              
              parThis.setState({
                can_dl_recs: true,
              });
              console.log("can receive now!", parThis.state.can_dl_recs)
              
              
            }
          };
    
          parThis.webSocket.onclose = function (event) {
            console.log('Connection closed');
          };
          parThis.webSocket.onerror = function (event) {
            reject(event);
          };
        });
    }


    render() {
        console.log(this.state.ready_to_show_results);
        console.log('Username ', this.props.username)
        if (this.state.ready_to_show_results){
            console.log('Inside show results')
            var arr = [];
            for (var key in this.state.features) {
                arr.push(<li>{key}:{this.state.features[key]}</li>);
            }

            var arr2 = [];
            for (var result in this.state.past_results) {
                var tmp_arr = [];
                for (var feat in result) {
                    tmp_arr.push(<li>{feat}:{result[feat]}</li>);
                }
                arr2.push(<li>{result['date']}:{tmp_arr}</li>);
            }
        }
        return (
          <div>
            <div className="planv-end">
              Σας ευχαριστούμε για τη συμμετοχή σας.
            </div>
                <div className="start-button">
                    <Button type="normal" size="large" onClick={this.nextPage}>Αρχική Σελίδα</Button>
                </div>
                <div className="start-button">
                    <Button type="normal" size="large" onClick={this.getResults}>Αποτελέσματα</Button>
                </div>
                <div>
                    {(this.state.ready_to_show_results)? 
                        <ul>
                            <li>Prediction: {this.state.prediction}</li>
                            {arr}
                        </ul>
                    :null}
                </div>

        </div>
        )
    }
}


export default FinalComponent;

