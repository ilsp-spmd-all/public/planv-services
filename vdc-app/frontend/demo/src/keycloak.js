import Keycloak from "keycloak-js";


const keycloakConfig = {
        realm: 'planv',
        clientId: 'data-collection-app-id',
        url: "https://auth.pikei.io/auth/"
    } 

const keycloak = new Keycloak(keycloakConfig);

export {keycloak};