import React, { Component } from "react";

import ReactPageScroller from "../../src/index";
import {BACKEND_URL} from '../../utils/config.js';
import InputComponent from "./InputComponent";
import ImagesUIComponent from "./ImagesUI";
import WordCardsUIComponent from "./WordCardsUI";
import FinalComponent from "./FinalComponent";
import { SentencesContext, stories, behaviors } from "./globals";
import { getCompletedSentences } from '../../utils/audio';
import { getStories , getPhotos, getInstructionsAudio, getAudio, getOtherAudio , UserLogout} from '../../utils/utils';
import { Router, Link, navigate , Redirect} from "@reach/router";
import RequestPageToRedirectTo from "./RequestPageToRedirectTo";
import { ReactKeycloakProvider } from "@react-keycloak/web";
import {keycloak} from "./keycloak"

// import Logo from'../assets/logo.png';


import "./index.css";
import { FastBackwardOutlined } from "@ant-design/icons";
// import WordCardsUIComponent from "./WordCardsUI";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            currentPath: "",
            sentence: {},
            behavior: {},
            user: '',
            token: '',
            session_ts: Date.now(),
            device: '',
            photos: [],
            instructions: [],
            other_audio: [],
            story_audio: null,
            availableStories: [],
            number_of_stories: 0,
            keycloakInitialized: false
        };
        // this._pageScroller = null;

        this.goToPage = this.goToPage.bind(this);
        this.inputNextPage = this.inputNextPage.bind(this);
        this.setUser = this.setUser.bind(this);
        this.selectBehavior = this.selectBehavior.bind(this);
        this.availableBehaviors = behaviors.slice(0);;
        this.tokenLogger = this.tokenLogger.bind(this)
        this.eventLogger = this.eventLogger.bind(this)

        // this.pages = 0;
        this.page_names = [];
        // this.project flag changes the url for each project
        // this.project="bdae-part-1";
        // this.project="bdae-part-3";
        // this.project="dysarthria";
        // this.project="augmented-narrative";
        this.project="test";
        this.ui_types={
            images:ImagesUIComponent,
            words:WordCardsUIComponent,
        };
        this.ui_type="images";
        this.title="Συλλογή δεδομένων αφηγηματικού λόγου";
        this.subtitle="PLan-V";
        // this.title="Συλλογή δεδομένων δυσαρθρικού λόγου";
        // this.subtitle="PLan-V";
    }

    componentDidMount() {
        keycloak.init({ 
            checkLoginIframe: false, 
            // silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.html'
        }).then((connected) => {
            console.log(connected)
            this.setState({keycloakInitialized:true})
            console.log('state set', this.keycloakInitialized)
            if(connected){
                console.log(connected)
              keycloak.updateToken().then((res) => {
                console.log(res)
                initInfo().then((response)=>{
                  if(response){
                    console.log('suc',response)
                    navigate(`/planv/${this.project}/`)              
                  }else{
                    console.log('nosuc',response)
                    navigate(`/planv/${this.project}/`)
                  }
                  keycloak.login()
                })
              }, (error) => {
                  console.log("An ", error, " error occured on checking token's validity")
              })
            }
          },(error)=>{
            navigate(`/planv/${this.project}/`)
            console.log('There was an error initializing keycloak', error)
          })
    }

    // componentDidMount() {
    //     getStories().then(data => {
    //       console.log(data);
    //       this.setState({ availableStories: data });
    //       this.pages = data.length + 1;
    //     });
    // }

    selectBehavior() {
        let randomElem = Math.floor(Math.random()*this.availableBehaviors.length) 
        let behavior = this.availableBehaviors[randomElem];
        this.availableBehaviors.splice(randomElem, 1);
        this.setState({behavior: behavior});
    }

    async goToPage(eventKey, index) {
        console.log('Event Key=', eventKey, 'Index: ', index);

        if (index==-2){
            // setState({currentPath:`/continue-session`});
            navigate(`/planv/${this.project}/continue-session`);
            
        }
        else if (index >= 0){
            // navigate(`/planv/${this.project}/${this.page_names[index]}`);
            // this.setState({currentPath:`/${this.page_names[index]}`});
            // navigate(`/planv/${this.project}/${index}`);
            this.setState({currentPath:`/${index}`});
            navigate(`/planv/${this.project}`+this.state.currentPath);
            
        }
       
        else if (index==-1){
            // this.setState({currentPath:`/final-page`});
            // navigate(`/planv/${this.project}`+this.state.currentPath);
            navigate(`/planv/${this.project}/final-page`);
        }
        // else if (index==0){
        //     navigate(`/planv/${this.project}/test`);
        // }
        else {
            await getStories().then(data => {
                this.setState({ availableStories: data, user: '', session_ts: Date.now()});
                console.log(Date.now)
                // this.pages = data.length;
                // Collect all page names and store them in page_names list
                for (var i = 0; i < data.length; i++) {
                    this.page_names.push(data[i]['title']);
                }
            });
            await UserLogout();
            // navigate(`/planv/dysarthria/`);
            window.location.reload();
            // navigate(`/`);
            // navigate(-1) to goto previous page (to use it with a button!)
        }
    };

    // Keycloak event logger
    eventLogger = (event, error) => {
        const now = new Date();
        console.log(now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds() + ' onKeycloakEvent:', event, error)
    }

    // Keycloak token logger
    tokenLogger = (tokens) => {
        if(tokens && tokens.token){
            console.log(tokens)
        }
    }
    pageOnChange = (number) => {
        this.setState({currentPage: number});
    };

    async inputNextPage(eventKey) {
        console.log('------------------------------')
            console.log('eventKey', eventKey, 'currentPage', this.state.currentPage)
            console.log('------------------------------')

        const index = 0; 
        let currentSentence = null;
        if (eventKey == this.state.number_of_stories+2){
            this.goToPage(eventKey, -1)
            this.setState({currentPage: this.state.currentPage+1})
        }
        else if (eventKey>1 && eventKey < this.state.number_of_stories+2){
            // const index = Math.floor(Math.random() * this.state.availableStories.length);
            

            if (eventKey>this.state.currentPage){
                // user requests page redirection 

                // get the number of test stories 
                // convention: test stories have id <= 0 
                var n_test_stories = 0;
                console.log('Inside Page Redirection')
                for (let j = 0; j < this.state.availableStories.length; j++) {
                    console.log('Page Redirection', j,n_test_stories);
                    if (this.state.availableStories[j]['id']<=0){
                        n_test_stories++;
                    }
                }
                console.log("test stories: ",n_test_stories);

                var diff = 0;
                diff = eventKey - this.state.currentPage + n_test_stories -1;      
                let availableStories = this.state.availableStories
                for (let i = 0; i < diff; i++) {
                    availableStories.splice(0, 1);
                }
                this.setState({currentPage: eventKey+1, availableStories:availableStories});
                currentSentence = availableStories[index];
                console.log(currentSentence);
            }
            else{
                this.setState({currentPage: this.state.currentPage + 1});
                console.log(index);
                currentSentence = this.state.availableStories[index];
                console.log(currentSentence);
            }
            
            // let currentSentence = this.state.availableStories[index];
            console.log("current", currentSentence, this.state.availableStories);
            if (currentSentence) {
                let photos = await getPhotos(currentSentence.id);
                let instructions = await getInstructionsAudio(currentSentence.id);
                let other_audio = await getOtherAudio(currentSentence.id);
                console.log("other audio", other_audio);
                let story_audio = await getAudio(currentSentence.id);
                let availableStories = this.state.availableStories;
                availableStories.splice(index, 1);
                this.setState({ availableStories: availableStories, sentence: currentSentence, photos: photos, instructions: instructions, story_audio: story_audio, other_audio: other_audio});
                console.log("available_sentences", this.state.availableStories, currentSentence);
                
            }
            this.goToPage(eventKey, currentSentence.id);
        }
        else if (eventKey==1){
            this.setState({currentPage: this.state.currentPage+1})
            this.goToPage(eventKey, -2)
        }
        else{
            this.setState({currentPage: this.state.currentPage+1})
            this.goToPage(eventKey, -4)
        }
    };

    async setUser(username, token) {

        fetch(BACKEND_URL + "/get_stories", {
            method: 'GET',
            credentials: 'include',
          }
        ).then((data) => data.json()
        ).then((res)=>res
        ).then(data => {
            console.log(data)
          this.setState({ availableStories: data, user: '', session_ts: Date.now()});
        //   this.pages = data.length;
          this.setState({number_of_stories : data.length});
          // Collect all page names and store them in page_names list
          for (var i = 0; i < data.length; i++) {
              this.page_names.push(data[i]['title']);
          }
        });
        this.setState({user: username, token: token,currentPath: this.page_names[0]});
    };
    

    render() {
        if (this.state.availableStories === [] || this.state.photos === [] || this.state.instructions === [] || this.state.story_audio === []) {
          return ''
        } else {
            const UI_type=this.ui_types[this.ui_type];
            const currentPath=this.state.currentPath;
            console.log("current_path: ", currentPath);
        return (
            
            <div className="demo-page-contain">
                {/* <keycloak */}
                <ReactKeycloakProvider authClient={keycloak} initOptions={{checkLoginIframe:false, onLoad: 'check-sso'}} onEvent={this.eventLogger} onTokens={this.tokenLogger}>
                    <SentencesContext.Provider value={this.state}>
                        <Router basepath={`planv/${this.project}`} >
                            <InputComponent path='/*' inputNextPage={this.inputNextPage} setUser={this.setUser} title={this.title} subtitle={this.subtitle}/>
                            <PrivateRoute as={RequestPageToRedirectTo} username={this.state.user} path='/continue-session' pageNames={this.page_names} inputNextPage={this.inputNextPage} />
                            {this.page_names.map(page_name => <PrivateRoute as={UI_type} username={this.state.user} path={currentPath} goToPage={this.inputNextPage } />)}
                            <PrivateRoute as={FinalComponent} username={this.state.user} sessionId={this.state.session_ts} path='/final-page' goToPage={this.inputNextPage}/> 
                        </Router>
                    </SentencesContext.Provider>
                </ReactKeycloakProvider>
            </div>
        )
        }
    }
}

class PrivateRoute extends React.Component {
    /* 
        PrivateRoute Component is used to prevent access to private component without logging in first
        In case indirect access is requested, the user is redirected to the first page.
    */
    constructor(props) {
        super(props);
        this.state = {
            username: props.username
        };
    }
    render() {
      let { as: Comp, user: username, ...props } = this.props;
      return  (this.state.username != '')? <Comp {...props} /> : <div>Byeee</div>;
    }
  }
