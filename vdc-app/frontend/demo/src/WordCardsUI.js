import React, { Component } from "react";
import 'antd/dist/antd.css';
import {
         Spin, Alert, ButtonToolbar, Button, notification, Layout, Typography, Card, Row, Col, message, Modal, Carousel,Image
        } from 'antd';
import { AudioOutlined, SoundOutlined , RightCircleOutlined, PauseCircleOutlined, LogoutOutlined, NodeIndexOutlined, InfoCircleOutlined, SlidersTwoTone} from '@ant-design/icons';
import {LeftOutlined, RightOutlined} from '@ant-design/icons';
import { SentencesContext, sentences, stories } from "./globals";
const {Header, Footer, Sider, Content} = Layout; 
const {Title} = Typography;
import './recorder.css';
import RecorderComponent from "./RecoderComponent";
import {CountdownCircleTimer} from 'react-countdown-circle-timer';

import {downloadPDF, getRecording} from '../../utils/utils';


class WordCardsUIComponent extends Component {
    // static contextType = context;
    constructor(props,context) {
        
        super(props,context);
        this.state = {
            stream: null,
            recording: false,
            recorder: null,
            listen: false,
            socketReady: false,
            loading: false,
            countdown_over: false,
            isModalVisible: false,
            uploading: false,
        };
        this.saveFile = this.saveFile.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleUpload = this.handleUpload.bind(this);
        this.sampleRate = 44100;
        this.sampleRateFinal = 22050;
        this.APIsampleRate = 22050;
        this.channels = 1;
        

        this.images = [];
        this.carousel_photos = [];
        this.story_audio = ''; 
        this.questions_list=[];

        this.thumbnail_height = 300;
        this.thumbnail_width = 500;
        this.recorderRef = React.createRef();
        this.get_recs_from_backend=true;
        this.recording_length = 0;
        this.first_time_render = true;

        this.quote = this.context.sentence ? this.context.sentence.quote : ''; 
        
       

                
        

        if (this.context.other_audio) {
          let other_audio_sorted = this.context.other_audio.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
          this.other_audio = []; 
          other_audio_sorted.forEach( (i) => {
              var audio = new Audio();
              audio.id = `${i.id}_audio`;
              audio.src = `data:audio/wav;base64,${i.src}`;
              window[`${i.id}_audio`] = audio;
              this.other_audio.push(<SoundOutlined style={{ fontSize: '75px', color: '#4E4E4E', paddingTop:20, paddingLeft:26}}  onClick={ () => this.startStopAudio(window[`${i.id}_audio`]) }/>);
          });
      }

      if (this.context.story_audio && this.context.story_audio.length > 0) {
        let story_audio_sorted = this.context.story_audio.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
        this.story_audio = []; 
        story_audio_sorted.forEach( (i) => {
            var audio = new Audio();
            audio.id = `${i.src}_story_audio`;
            audio.src = `data:audio/wav;base64,${i.src}`;
            window[`${i.src}_story_audio`] = audio;
            this.story_audio.push(<SoundOutlined style={{ fontSize: '75px', color: '#39ac73' }}  onClick={ () => this.startStopAudio(window[`${i.src}_story_audio`]) }/>);
        });
      }           
      


    }
// rename save file to something more apt? doesn't save anything
    async saveFile() {
        this.setState({ loading: true });
        if (this.audioRef){
          this.audioRef.removeAttribute('src');
          this.audioRef.load();
        }
        await this.props.goToPage(this.context.currentPage);
        this.setState({ loading: false, listen: false });
    }
    startStopAudio(audio) {
      console.log("aud", audio);
        if (audio.paused) {
            audio.play();
        }else {
            audio.pause();
            audio.currentTime = 0;
        }

    }

    componentDidMount() {
    
    
    
    }



    async triggerRecord(){
      this.state.recording ? this.recorderRef.current.stopRecord() : this.recorderRef.current.startRecord();
    }

    async handleClick() {
      const { recording, listen, uploading } = this.state;
      const error = (uploading) => {
        if (!uploading){
          message.error('Πρέπει πρώτα να ολοκληρώσετε την ηχογράφηση');
        }
        
      };

      if (!uploading) {

        await this.triggerRecord();

        this.setState(state => ({
          recording: !state.recording,
          listen: true
        }));}
      else { error(uploading); }
    }

    async handleUpload(uploading){
      console.log('uploading in recorderComponent: ',uploading);



      this.setState({uploading:uploading});
    }

    render() {
      const { recording, listen, uploading } = this.state;
      const layout="wordCards";

      

      

      const error = (uploading) => {
        if (!uploading){
          message.error('Πρέπει πρώτα να ολοκληρώσετε την ηχογράφηση');
        }
        else {
          message.error('Περιμένετε να σταλθεί η ηχογράφηση');
        }
      };

      const success = () => {
        message.success('Έχετε παύσει τη χορήγηση');
      };

      const showModal = () => {
        this.setState({isModalVisible: true});
      };
    
      const handleOk = () => {
        this.setState({isModalVisible: false});
      };
      
      const handleCancel = () => {
        this.setState({isModalVisible: false});
      };

      const remaining_words = this.context.sentence ? `${this.context.sentence.id} / ${this.context.number_of_stories-1}` : ''; 
      const quote = this.context.sentence ? this.context.sentence.quote : ''; 
      const other_audio=this.other_audio;
      
      

        // Don't show record button if their browser doesn't support it.
        return (
          <Spin className="spin" spinning={this.state.loading}>
            <div class="grid-container">
              <div class="title">
                <Title level={2} style={{color: '#595959'}}>Παρακαλώ πείτε τη λέξη όσο πιο φυσικά μπορείτε</Title>
              </div>
              <div class="card">
                <Card title={remaining_words} style={{background: '#BBDCFE'}}>
                  <Title level={1}>{quote}</Title>
                </Card>
              </div>
              <div class="exit">
                <LogoutOutlined 
                  onClick={()=> {window.location.reload()}}
                  style={{fontSize: '64px', color: '#FF5733'}} 
                />
              </div>
                {/*
                <PauseCircleOutlined
                  onClick={()=> {
                    (recording)? this.stopRecord() : success();
                  }}
                  style={{fontSize:'64px'}} 
                />
                */}
              <div class="continue">
              <RightCircleOutlined 
                        className="icons"
                        onClick={() => {
                          console.log("can_dl_recs: ",uploading);
                          (listen && !recording && !uploading) ? this.saveFile(): error(uploading); }}
                        style={(listen && !recording && !uploading)?(
                          {fontSize: '64px', color: '#008000', paddingBottom:20}
                        ):(
                          {fontSize: '64px', paddingBottom:20}
                        )}
                      />
              </div>
              <div class="audio">{other_audio}</div>
              
              <div class="countdown">
                {(this.state.recording && !this.state.countdown_over)?
                  <CountdownCircleTimer
                    className="countdown"
                    onComplete={() => {
                      // do your stuff here
                      this.setState({countdown_over:true})
                      return [false, 1500] // repeat animation in 1.5 seconds if true 
                      }
                    }
                    size={150}
                    isPlaying
                    duration={3}
                    colors={[
                      ['#004777', 0.4],
                      ['#F7B801', 0.4],
                      ['#A30000', 0.2],
                    ]}
                  >
                    {({remainingTime, animatedColor}) => (
                      <h3 style={{color:animatedColor}}> {remainingTime} </h3>
                    )
                    }
                  </CountdownCircleTimer>:null}
              </div>
              <div class="microphone">
              <RecorderComponent 
                        ref={this.recorderRef}
                        recording={recording}
                        listen={listen}
                        layout={layout}
                        handleClick={this.handleClick}
                        handleUpload={this.handleUpload}
                        >
                      </RecorderComponent>
              </div>
              <div class="recordings">
                {/* {listen ? <audio  ref={(audioRef)=>{this.audioRef=audioRef;}} controls /> : null} */}
                {/*
                {(listen && !recording) ? 
                  <Button type="normal" size="large" onClick={this.saveFile}>
                    <Icon type="save" />
                    {this.context.availableStories.length > 0 ? 'Επόμενο': 'Τέλος'}
                  </Button> : null}
                */}
              </div>
              <div class="info">
                <InfoCircleOutlined 
                  onClick={showModal}
                  style={{fontSize: '64px', color: '##005077'}}                 
                />
              </div>
              <Modal title="Οδηγίες χορήγησης δοκιμασίας ηχογράφησης προς χορηγητές" 
                  visible={this.state.isModalVisible} 
                  onOk={handleOk} 
                  onCancel={handleCancel}
                  width={1000}>
                <p> 1.	Η διαδικασία πρέπει να γίνει <b> <u> σε ήσυχο χώρο, χωρίς εξωτερικό θόρυβο και χωρίς την παρουσία άλλων </u></b>. 
                    Ο παραμικρός θόρυβος είτε από εμάς (κινήσεις μας, τηλέφωνα, χαρτιά κ.λπ.) είτε από το εξωτερικό περιβάλλον 
                    (πόρτες, συνομιλίες απ’ έξω, θόρυβος δρόμου) καταστρέφει την ποιότητα των δεδομένων. </p>
                <p> 2.	Πρέπει να φροντίσουμε να μην υπάρχουν διακοπές από τρίτους για όσο διαρκέσει 
                    η διαδικασία και να μη προκαλείται κανένας θόρυβος είτε από κινήσεις του συμμετέχοντα είτε από δικές μας κινήσεις.</p>
                <p> 3. Ο συμμετέχων πρέπει να έχει μπροστά του την οθόνη του υπολογιστή. 
                    Στην αρχή κάθε δοκιμασίας, στεκόμαστε δίπλα του για να βάλουμε τον κωδικό, να του βάλουμε να διαβάσει τις οδηγίες
                    ή να τους τις διαβάσουμε εμείς και να δώσουμε διευκρινίσεις, αν χρειαστεί. </p>
                <p> 4.	Η διαδικασία αρχίζει με την εισαγωγή του κωδικού του συμμετέχοντα. </p>
                <p> 5. Μετά από αυτό, μεταφέρεται στην οθόνη της δοκιμασίας. 
                    Βεβαιωνόμαστε ότι έχει καταλάβει τι πρέπει να κάνει δίνοντας τις παρακάτω οδηγίες με απλά λόγια (ενδεικτική διατύπωση): 
                    </p>
                      <b> 
                        <p>
                        “Σε κάθε σελίδα θα εμφανίζεται η λέξη που θα θέλαμε να μας πείτε. 
                        Έχετε την δυνατότητα να ακούσετε την λέξη πατώντας στο ηχείο που βρίσκεται δίπλα από αυτή.  
                        </p>
                        <p>
                        Στη συνέχεια, πατώντας το κουμπί με το μικρόφωνο θα αρχίσει η αντίστροφη μέτρηση 3,2,1 και 
                        θα ενεργοποιηθεί το μικρόφωνό (θα γίνει κόκκινο) και τότε μπορείτε να πείτε την λέξη που ακούσατε.  
                        </p>
                        <p>
                        Μόλις τελειώσει η ηχογράφηση θα πατήσετε πάλι το μικρόφωνο (θα γίνει και πάλι μαύρο) 
                        και θα μπορέσετε να προχωρήσετε στην επόμενη λέξη πατώντας 
                        το κουμπί «επόμενο» <RightCircleOutlined style={{color: '#008000'}}/>. 
                        </p>
                        <p>
                        Έχετε την δυνατότητα να σταματήσετε για λίγο για να κάνετε διάλειμμα έχοντας 
                        κλειστό το μικρόφωνο (μαύρο χρώμα) και να συνεχίσετε μετά. 
                        Επίσης έχετε την δυνατότητα να διακόψετε την αξιολόγηση πατώντας το κουμπί 
                        για την έξοδο από την εφαρμογή <LogoutOutlined style={{color: '#FF5733'}}/>.” 
                        </p>
                      </b>
                     
                <p> 6. <b>ΔΥΟ ΔΟΚΙΜΑΣΤΙΚΑ ΠΑΡΑΔΕΙΓΜΑΤΑ ΛΕΞΕΩΝ. </b>  Αφού καταλάβει τις οδηγίες ο ασθενής του ζητάτε να κάνει δύο παραδείγματα 
                    για να βεβαιωθείτε ότι κατάλαβε την διαδικασία, με την δική σας βοήθεια. 
                    Σε αυτή την δοκιμαστική διαδικασία έχει ενσωματωθεί και η δοκιμή του μικροφώνου. 
                    Αυτό σημαίνει πρακτικά ότι εξηγώντας όπως ενδεικτικά προτείνουμε παραπάνω, 
                    θα κάνετε τα βήματα μαζί με τον συμμετέχοντα αλλά μετά την ηχογράφηση <b> και ΜΟΝΟ για τις δύο πρώτες λέξεις </b> 
                    θα πρέπει να τις ακούσετε για να βεβαιωθείτε ότι έχουν ηχογραφηθεί σωστά και ότι ο υπολογιστής βρίσκεται στην 
                    σωστή απόσταση από τον συμμετέχοντα. Για τις υπόλοιπες λέξεις δεν χρειάζεται να ακούτε ούτε εσείς ούτε 
                    και ο συμμετέχων τις ηχογραφήσεις του. </p>
                <p> 7. Η δοκιμασία περιλαμβάνει την ηχογράφηση περίπου 182 λέξεων. 
                    Ζητάμε από κάθε συμμετέχοντα να πει το σύνολο των λέξεων.  </p>
                <p> 8. Οι λέξεις είναι οργανωμένες σε ομάδες των 42 λέξεων</p>
                <p> 9. Υπάρχει η δυνατότητα να κάνει παύση και να συνεχίσει μετά από λίγο. </p>
                <p> 10. Υπάρχει η δυνατότητα να διακόψει και να ξεκινήσει άλλη στιγμή από εκεί που άφησε την δοκιμασία. 
                    Αν επιλέξουμε να διακόψουμε την χορήγηση θα πρέπει να έχει ολοκληρωθεί η ηχογράφηση της λέξης πριν την 
                    διακοπή και να σημειώσετε τον αριθμό που αναγράφεται πάνω από την λέξη. Με αυτό τον τρόπο την επόμενη 
                    φορά θα μπορέσετε να συνεχίσετε από την επόμενη λέξη βάζοντας τον επόμενο αριθμό 
                    (πχ αν σταματήσατε στην λέξη νο 75, την επόμενη φορά στην αρχική οθόνη θα βάλετε τον αριθμό 76 
                    και θα συνεχίσετε από εκεί και έπειτα). </p>
                <p> 11. Μπορείτε και πρέπει να τον βοηθήσετε τον συμμετέχοντα αν δυσκολεύεται να χρησιμοποιήσει τον υπολογιστή κατά την ηχογράφηση, 
                    αλλά θα πρέπει να προσέξετε να μην μιλάτε ταυτόχρονα με αυτόν. </p>
              </Modal>

            </div>
          </Spin>
      )
  }
}
WordCardsUIComponent.contextType = SentencesContext;

export default WordCardsUIComponent;

