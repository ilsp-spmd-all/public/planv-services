import React, { Component } from "react";
import 'antd/dist/antd.css';
import {
         Spin, Alert, ButtonToolbar, Button, Icon, notification, Layout, Typography, Card, Row, Col, message, Modal,
        } from 'antd';
import { AudioOutlined, SoundOutlined , RightCircleOutlined, PauseCircleOutlined, LogoutOutlined, NodeIndexOutlined, InfoCircleOutlined} from '@ant-design/icons';
import Recorder from 'recorder-js';
import {
  getAudioStream, exportBuffer, uploadFile, createHeader, downsampleBuffer,
} from '../../utils/audio';
import { SentencesContext, sentences } from "./globals";
import Gallery from 'react-grid-gallery';
import {BACKEND_URL, WEBSOCKET_URL, VERSION} from '../../utils/config.js';
import Grid from "antd/lib/card/Grid";
const {Header, Footer, Sider, Content} = Layout; 
const {Title} = Typography;
import {CountdownCircleTimer} from 'react-countdown-circle-timer';
import {navigate} from "@reach/router";
import {getTestAudio, downloadPDF} from '../../utils/utils';

function concatenate(ResultConstructor, ...arrays) {
  let totalLength = 0;
  arrays.map((arr) => {
    totalLength += arr.length;
    return true;
  });
  const result = new ResultConstructor(totalLength);
  let offset = 0;
  arrays.map((arr) => {
    result.set(arr, offset);
    offset += arr.length;
    return true;
  });
  return result;
}


class RecorderComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stream: null,
            recording: false,
            recorder: null,
            listen: false,
            socketReady: false,
            loading: false,
            countdown_over: false,
            test_recording: null,
            isModalVisible:false,
        };
        this.saveFile = this.saveFile.bind(this);
        this.startRecord = this.startRecord.bind(this);
        this.stopRecord = this.stopRecord.bind(this);
        this.streamAudio = this.streamAudio.bind(this);
        this.sampleRate = 44100;
        this.audioBlob = null;
        this.chunk = null;
        this.webSocket = null;
        this.sampleRateFinal = 22050;
        this.APIsampleRate = 22050;
        this.channels = 1;
        this.images = []
        this.thumbnail_height = 300;
        this.thumbnail_width = 500;
        




        this.s3_bucket = 'webrec';
        this.instructions_audio = React.createRef(null);

    }

  async componentDidMount() {
    this.setupBeforeUnloadListener();
    if (VERSION==1){
      this.forceUpdate(); // this needs to change as we force update when component is mounted 
    } 
    // await this.openSocket();
  }


    setupBeforeUnloadListener() {
    window.addEventListener('beforeunload', (ev) => {
      ev.preventDefault();
      return this.unloadWebsocket();
    });
  }

    async unloadWebsocket() {
    // await this.sendStop();
    await this.closeSocket();
  }

    async saveFile() {
        this.setState({ loading: true });
        //this.audioRef.removeAttribute('src');
        //this.audioRef.load();
        await this.props.goToPage(this.context.currentPage);
        this.setState({ loading: false, listen: false });
        // this.setState({ loading: true });
        // let filename = `${this.context.user}_${this.context.sentence.id}_${this.context.session_ts}`;
        //
        // this.audioRef.removeAttribute('src');
        // this.audioRef.load();
        // uploadFile(this.audioBlob, filename, this.s3_bucket).then(async (status) => {
        //     if (status) {
        //         await this.props.goToPage(this.context.currentPage);
        //     }
        //     this.setState({ loading: false, listen: false });
        //     this.audioBlob = null;
        // });
    }

    async prepareAudioStream() {
        let stream;

        try {
            stream = await getAudioStream();
        } catch (error) {
            // Users browser doesn't support audio.
            // Add your handler here.
            console.log(error);
        }

        this.setState({ stream });
        return stream;
    }

    setPID(newpid) {
    this.setState({
      pid: newpid,
    });
  }



    submitTCP() {
    const parThis = this;
    return new Promise((resolve, reject) => {
      console.log('Requesting PID for live demo...');
      parThis.setPID('');
      var rest_url = `${BACKEND_URL}/get_pid?channels=${parThis.channels}`;
      const options = {
        method: 'POST',
        headers: {
          'X-Auth-Token': parThis.state.token,
        },
        cache: 'no-cache',
      };
      fetch(rest_url, options)
        .then(data => data.json())
        .then((response) => {
          if (response) {
            const pid = response.pid;
            if (pid > 0) {
              parThis.setPID(pid);
              console.log(`Init successful with pid ${pid}`);
              resolve(true);
            } else {
              notification.error({
                message: 'Could not retrieve Process ID from API!',
              });
              reject('error');
            }
          } else {
            notification.error({
              message: 'Could not read API response!',
            });
            reject('error');
          }
        })
        .catch((error) => {
          notification.error({
            message: 'Error on POST request',
            description: `error: ${error}`,
          });
          reject('error');
        });
      return false;
    });
  }


    async startRecord() {
				this.audioBlob = null;
				this.chunk = new Float32Array();
				//if (this.audioRef) {
				//	this.audioRef.removeAttribute('src');
				//	this.audioRef.load();
				//}
				// const stream = await this.prepareAudioStream();
				// const header = createHeader(this.APIsampleRate, 400000000);
				// this.webSocket.send(header);

				// this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
				// this.sampleRate = this.audioContext.sampleRate;
				// const recorder = new Recorder(this.audioContext);
				// recorder.init(stream);

				// this.setState(
				// 	{
				// 		recorder,
				// 		recording: true,
				// 	},
				// 	() => {
				// 		recorder.start();
				// 		this.streamAudio();
				// 	},
				// );
        //


    try {
      // Get new PID and start sending data
      if (
        this.webSocket === null
        || this.webSocket.readyState === WebSocket.CLOSED
      ) {
        await this.openSocket();
      }
      // await this.submitTCP();

      this.audioBlob = null;
      this.chunk = new Float32Array();
      // if (this.audioRef) {
      //  this.audioRef.removeAttribute('src');
      //  this.audioRef.load();
      //}
      const stream = await this.prepareAudioStream();
      console.log("streammmm", stream);
      this.header = createHeader(this.APIsampleRate, 4000000);
      await this.sendStart();


      if (!this.audioContext) {
        this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
      }
      this.sampleRate = this.audioContext.sampleRate;
      const recorder = new Recorder(this.audioContext);
      recorder.init(stream);

      this.setState(
        {
          recorder,
          recording: true,
          countdown_over: false,
        },
      );
    } catch (error) {
      console.log('ooops ', error);
    }


    }

    openSocket() {
    const parThis = this;
    return new Promise((resolve, reject) => {
      // Ensures only one connection is open at a time
      console.log('Opening websocket connection...');
      if (
        parThis.webSocket !== null
        && parThis.webSocket.readyState !== WebSocket.CLOSED
      ) {
        console.log('WebSocket is already opened.');
        resolve();
      }
      // Create a new instance of the websocket
      console.log("token:", parThis.context.token);
      parThis.webSocket = new WebSocket(
        // `ws://${parThis.state.apihostname}:8080/stream/${parThis.state.clientid}?X-Auth-Token=${parThis.state.token}`,
        `${WEBSOCKET_URL}/?token=${parThis.context.token}`,
      );

      /**
       * Binds functions to the listeners for the websocket.
       */
      parThis.webSocket.onopen = function openSock(event) {
        console.log(event);
        parThis.setState({ socketReady: true });
        resolve(true);
      };

      parThis.webSocket.onmessage = function socketMessage(event) {
        console.log(event.data);
        console.log(JSON.parse(event.data));
        const status = JSON.parse(event.data).status;
        if (status == 'accepting') {
          const interval = setInterval(() => {
            if (parThis.state.recorder) {
              parThis.counter = 0;
              // parThis.webSocket.send(parThis.header);
              parThis.state.recorder.start();
              // Stream first segment and then start interval
              parThis.streamAudio();
              parThis.streamAudioHandle = setInterval(parThis.streamAudio, 250);
              clearInterval(interval);
            }
          }, 20);
        }
      };

      parThis.webSocket.onclose = function (event) {
        console.log('Connection closed');
      };
      parThis.webSocket.onerror = function (event) {
        reject(event);
      };
    });
  }

  sendStart() {
    const parThis = this;

    {/*
    let target_team = ''
    if (this.context.sentence.id == 42000){
      target_team = 'test1';
    }
    else if (this.context.sentence.id == 42001){
      target_team = 'test2';
    }
    else {
      target_team = this.context.sentence.path.split('/')[2].split('_')[0];
    }
    */}
    let filename = `${this.context.user}_${this.context.sentence.title}_${this.context.session_ts}.wav`;
    return new Promise((resolve, reject) => {
      try {
        console.log(
          `Sending start message to WS for pid ${parThis.state.pid}`,
        );
        parThis.webSocket.send(
          JSON.stringify({
            control: 'start',
            filename: filename,
            // channels: parThis.channels.toString(),
          }),
        );
        resolve(true);
      } catch (e) {
        reject(e);
      }
    });
  }

  async sendStop() {
    
    const parThis = this;
    let header = createHeader(this.APIsampleRate, this.chunk.length);
    return new Promise((resolve, reject) => {
      console.log('Sending stop message to WS');
      parThis.webSocket.send(JSON.stringify({ control: 'header' }));
      parThis.webSocket.send(header);
      parThis.webSocket.send(JSON.stringify({ control: 'end' }));
    });
    await this.closeSocket();


  }

  closeSocket() {
    const parThis = this;
    return new Promise((resolve, reject) => {
      parThis.webSocket.close();
    });
  }

  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  streamAudio() {
    // const [ stream ] =  this.state.stream;
    // if (stream) {
    if (this.state.recording === true) {
      this.getChunk();
    }
    // } else {
    //   // We need sendStop() in order to send all segments back
    //   this.sendStop();
    //   clearInterval(this.streamAudioHandle);
    // }
  }


  convertFloat32ToInt16(buffer) {
    let l = buffer.length;
    const buf = new Int16Array(l);
    while (l--) {
      buf[l] = Math.min(1, buffer[l]) * 0x7fff;
    }
    return buf.buffer;
  }

  async getChunk() {
    const { recorder } = this.state;
    if (recorder == null) {
      return;
    }
    const { buffer } = await recorder.stop();
    recorder.start();
    const downsampledBuffer = downsampleBuffer(
      buffer[0],
      this.sampleRate,
      this.APIsampleRate,
    );
    const convertedBuffer = this.convertFloat32ToInt16(downsampledBuffer);
    if (this.webSocket === null || this.webSocket.readyState === WebSocket.CLOSED) {
      this.initRecording();
      notification.error({
        message: 'WebSocket is closed',
      });
      return;
    }
    if (convertedBuffer.byteLength > 0) {
      this.webSocket.send(convertedBuffer);
      this.chunk = concatenate(Float32Array, this.chunk, buffer[0]);
    }
  }

  initRecording() {
    clearInterval(this.streamAudioHandle);
    this.setState({
      recorder: null,
      stream: null,
      recording: false,
    });
  }

  async stopRecord() {
    const { recorder, stream } = this.state;

    const { buffer } = await recorder.stop();
    this.chunk = concatenate(Float32Array, this.chunk, buffer[0]);
    this.audioBlob = exportBuffer(this.chunk, this.sampleRate);
    
    this.setState({ listen: true, recording: false});
    // this.audioRef.src = URL.createObjectURL(this.audioBlob);
    // console.log(this.audioBlob.length);
    // console.log(this.audioBlob);
    // console.log(this.chunk.length);
    // this.audioRef.load();
    if (stream) {
      stream.getTracks().forEach((track) => {
        track.stop();
        stream.removeTrack(track);
      });
    }

    await this.timeout(250); // setting time out to receive last chunk
    this.sendStop();
    // setTimeout(this,ini(), 1000)
    // await this.timeout(1000);
    this.initRecording();
    {/* 
    let target_team = ''
    if (this.context.sentence.id == 42000){
      target_team = 'test1';
    }
    else if (this.context.sentence.id == 42001){
      target_team = 'test2';
    }
    else {
      target_team = this.context.sentence.path.split('/')[2].split('_')[0];
    }
    */}
    let filename = this.context.user + '_' + this.context.sentence.title + '_' + this.context.session_ts;
    let test_audio = await getTestAudio(filename);
    test_audio = test_audio[0];
    var audio = new Audio();
    audio.id = `${test_audio.id}_audio`;
    audio.src = `data:audio/wav;base64,${test_audio.src}`;

    // this.testAudioRef.src = audio.src;
    // console.log(this.testAudioRef.src);
    // console.log(this.Aud)
    // this.testAudioRef.load();
    window[`${test_audio.id}_audio`] = audio;
    this.setState({test_recording:test_audio});

    const b64toBlob = (b64Data, contentType='', sliceSize=512) => {
      const byteCharacters = atob(b64Data);
      const byteArrays = [];
    
      for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);
    
        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }
    
        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }
    
      const blob = new Blob(byteArrays, {type: contentType});
      return blob;
    }
    // data:audio/wav;base64
    const contentType = 'audio/wav';
    const blob = b64toBlob(test_audio.src, contentType);
    const blobUrl = URL.createObjectURL(blob);
    this.testingRef2.src = blobUrl;
    this.testingRef2.load()

    console.log('Frontend audio', this.chunk.length);
    console.log('Backend audio', test_audio.src.length);

    console.log(blob.size); // backend 
    console.log(this.audioBlob.size); // frontend 
  }


    startStopAudio(audio) {
      console.log("aud", audio);
        if (audio.paused) {
            audio.play();
        }else {
            audio.pause();
            audio.currentTime = 0;
        }

    }


    render() {
        const { recording, stream, listen } = this.state;
        const quote = this.context.sentence ? this.context.sentence.quote : ''; 
        const remaining_words = this.context.sentence ? `${this.context.sentence.id} / ${this.context.number_of_stories}` : ''; 
        let photos = ''; 
        let instructions = ''; 
        let story_audio = ''; 
        this.images = [];
        if (this.context.photos && this.context.photos.length > 0) {
            let photos_sorted = this.context.photos.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
          console.log(photos_sorted.length);
            photos_sorted.forEach( (photo) => {
                this.images.push({
                                    src: `data:image/png;base64,${photo.src}`,
                                    thumbnail: `data:image/png;base64,${photo.src}`,
                                    thumbnailWidth: 340,
                                    thumbnailHeight: 200,
                                    thumbnailCaption: photo.id
                            });
            });
        }

        if (this.context.instructions && this.context.instructions.length > 0) {
          let instructions_sorted = this.context.instructions.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
          instructions = []; 
          instructions_sorted.forEach( (i) => {
              var audio = new Audio();
              audio.id = `${i.id}_audio`;
              audio.src = `data:audio/wav;base64,${i.src}`;
              window[`${i.id}_audio`] = audio;
              instructions.push(<SoundOutlined style={{ fontSize: '100px', color: '#4E4E4E' }}  onClick={ () => this.startStopAudio(window[`${i.id}_audio`]) }/>);
          });
      }

      if (this.context.story_audio && this.context.story_audio.length > 0) {
        let story_audio_sorted = this.context.story_audio.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
        story_audio = []; 
        story_audio_sorted.forEach( (i) => {
            var audio = new Audio();
            audio.id = `${i.src}_story_audio`;
            audio.src = `data:audio/wav;base64,${i.src}`;
            window[`${i.src}_story_audio`] = audio;
            story_audio.push(<SoundOutlined style={{ fontSize: '750%', color: '#39ac73' }}  onClick={ () => this.startStopAudio(window[`${i.src}_story_audio`]) }/>);
        });
      }                  

      const error = () => {
        message.error('Πρέπει πρώτα να ολοκληρώσετε την ηχογράφηση');
      };

      const success = () => {
        message.success('Έχετε παύσει τη χορήγηση');
      };

      const showModal = () => {
        this.setState({isModalVisible: true});
      };
    
      const handleOk = () => {
        this.setState({isModalVisible: false});
      };
      
      const handleCancel = () => {
        this.setState({isModalVisible: false});
      };
      

        // Don't show record button if their browser doesn't support it.
        return (
          <Spin className="spin" spinning={this.state.loading}>

            <Layout className="layout" style={{background:"white"}}>
                  <Header style={{ background: "white", height: "150px" }}>
                    
                      <Row gutter={[8, 8]}>
                        <Col span={8} />
                        <Col span={8}>
                          <Title level={2} style={{color: '#595959', paddingTop:20}}>Δοκιμαστική Ηχογράφηση</Title>
                        </Col>
                        <Col span={8} />
                      </Row>
                  </Header>
                  <Content>
                    <div style={{height: "calc(100vh - 400px)" }}></div>
                  </Content>
                  <Footer style={{background: "white"}}>

                  <div>
                      <Row>
                      <Col span={2}/>
                      <Col span={2}>
                        <LogoutOutlined 
                          onClick={()=> {window.location.reload()}}
                          style={{fontSize: '64px', color: '#FF5733', paddingTop:20}} 
                        />
                      </Col>
                      <Col span={2}>
                        <InfoCircleOutlined 
                          onClick={showModal}
                          style={{fontSize: '64px', color: '##005077', paddingTop:20}}                 
                        />
                      </Col>
                      
                      <Col span={10}></Col>
                      <Col span={2}>
                        <RightCircleOutlined 
                          onClick={() => {
                            (listen && !recording) ? this.saveFile(): this.saveFile(); }}
                          style={(listen && !recording)?(
                            {fontSize: '64px', color: '#008000', paddingTop:20}
                          ):(
                            {fontSize: '64px', paddingTop:20}
                          )}
                        />
                      </Col>
                      <Col span={6}>
                        
                        <AudioOutlined size="large"
                          onClick={()=> {
                            recording ? this.stopRecord() : this.startRecord();
                          }}
                          style={recording ? (
                            {fontSize: '120px', color: '#FF5733'}
                          ) : 
                          ( {fontSize: '120px', color: '#4E4E4E'})
                          }
                        ></AudioOutlined>
                        {listen ? 
                          <audio ref={(testingRef2)=>{this.testingRef2=testingRef2;}} controls /> : null
                        }
                      </Col>
                      </Row>
                    </div>
                  </Footer>
                
            </Layout>
            
           
            
            


            {/*
            <div class="grid-container">
              <div class="title">
                <Title level={2} style={{color: '#595959', paddingTop:50}}>Δοκιμαστική Ηχογράφηση</Title>
              </div>
              <div class="exit">
                <LogoutOutlined 
                  onClick={()=> {window.location.reload()}}
                  style={{fontSize: '64px', color: '#FF5733', paddingLeft:100}} 
                />
              </div>
                
              <div class="continue">
                <RightCircleOutlined 
                  onClick={() => {
                    (listen && !recording) ? this.saveFile(): this.saveFile(); }}
                  style={(listen && !recording)?(
                    {fontSize: '64px', color: '#008000'}
                  ):(
                    {fontSize: '64px'}
                  )}
                />
              </div>
              <div class="audio">{instructions}</div>

              <div class="microphone">
                <AudioOutlined className="microphone" size="large"
                        onClick={()=> {
                          recording ? this.stopRecord() : this.startRecord();
                        }}
                        style={recording ? (
                          {fontSize: '120px', color: '#FF5733',  paddingBlockStart:80}
                        ) : 
                        ( {fontSize: '120px', color: '#4E4E4E',  paddingBlockStart:80})
                        }
                ></AudioOutlined>
              </div>
              
              <div class="recordings">
                
                {listen ? 
                <audio  ref={(testingRef2)=>{this.testingRef2=testingRef2;}} controls /> : null

                }
              
               
              </div>

              <div class="info">
                <InfoCircleOutlined 
                  onClick={showModal}
                  style={{fontSize: '64px', color: '##005077'}}                 
                />
              </div>
              <Modal title="Οδηγίες χορήγησης δοκιμασίας ηχογράφησης προς χορηγητές" 
                  visible={this.state.isModalVisible} 
                  onOk={handleOk} 
                  onCancel={handleCancel}
                  width={1000}>
                <p> 1.	Η διαδικασία πρέπει να γίνει <b> <u> σε ήσυχο χώρο, χωρίς εξωτερικό θόρυβο και χωρίς την παρουσία άλλων </u></b>. 
                    Ο παραμικρός θόρυβος είτε από εμάς (κινήσεις μας, τηλέφωνα, χαρτιά κ.λπ.) είτε από το εξωτερικό περιβάλλον 
                    (πόρτες, συνομιλίες απ’ έξω, θόρυβος δρόμου) καταστρέφει την ποιότητα των δεδομένων. </p>
                <p> 2.	Πρέπει να φροντίσουμε να μην υπάρχουν διακοπές από τρίτους για όσο διαρκέσει 
                    η διαδικασία και να μη προκαλείται κανένας θόρυβος είτε από κινήσεις του συμμετέχοντα είτε από δικές μας κινήσεις.</p>
                <p> 3. Ο συμμετέχων πρέπει να έχει μπροστά του την οθόνη του υπολογιστή. 
                    Στην αρχή κάθε δοκιμασίας, στεκόμαστε δίπλα του για να βάλουμε τον κωδικό, να του βάλουμε να διαβάσει τις οδηγίες
                    ή να τους τις διαβάσουμε εμείς και να δώσουμε διευκρινίσεις, αν χρειαστεί. </p>
                <p> 4.	Η διαδικασία αρχίζει με την εισαγωγή του κωδικού του συμμετέχοντα. </p>
                <p> 5. Μετά από αυτό, μεταφέρεται στην οθόνη της δοκιμασίας. 
                    Βεβαιωνόμαστε ότι έχει καταλάβει τι πρέπει να κάνει δίνοντας τις παρακάτω οδηγίες με απλά λόγια (ενδεικτική διατύπωση): 
                    </p>
                      <b> 
                        <p>
                        “Σε κάθε σελίδα θα εμφανίζεται η λέξη που θα θέλαμε να μας πείτε. 
                        Έχετε την δυνατότητα να ακούσετε την λέξη πατώντας στο ηχείο που βρίσκεται δίπλα από αυτή.  
                        </p>
                        <p>
                        Στη συνέχεια, πατώντας το κουμπί με το μικρόφωνο θα αρχίσει η αντίστροφη μέτρηση 3,2,1 και 
                        θα ενεργοποιηθεί το μικρόφωνό (θα γίνει κόκκινο) και τότε μπορείτε να πείτε την λέξη που ακούσατε.  
                        </p>
                        <p>
                        Μόλις τελειώσει η ηχογράφηση θα πατήσετε πάλι το μικρόφωνο (θα γίνει και πάλι μαύρο) 
                        και θα μπορέσετε να προχωρήσετε στην επόμενη λέξη πατώντας 
                        το κουμπί «επόμενο» <RightCircleOutlined style={{color: '#008000'}}/>. 
                        </p>
                        <p>
                        Έχετε την δυνατότητα να σταματήσετε για λίγο για να κάνετε διάλειμμα έχοντας 
                        κλειστό το μικρόφωνο (μαύρο χρώμα) και να συνεχίσετε μετά. 
                        Επίσης έχετε την δυνατότητα να διακόψετε την αξιολόγηση πατώντας το κουμπί 
                        για την έξοδο από την εφαρμογή <LogoutOutlined style={{color: '#FF5733'}}/>.” 
                        </p>
                      </b>
                     
                <p> 6. <b>ΔΥΟ ΔΟΚΙΜΑΣΤΙΚΑ ΠΑΡΑΔΕΙΓΜΑΤΑ ΛΕΞΕΩΝ. </b>  Αφού καταλάβει τις οδηγίες ο ασθενής του ζητάτε να κάνει δύο παραδείγματα 
                    για να βεβαιωθείτε ότι κατάλαβε την διαδικασία, με την δική σας βοήθεια. 
                    Σε αυτή την δοκιμαστική διαδικασία έχει ενσωματωθεί και η δοκιμή του μικροφώνου. 
                    Αυτό σημαίνει πρακτικά ότι εξηγώντας όπως ενδεικτικά προτείνουμε παραπάνω, 
                    θα κάνετε τα βήματα μαζί με τον συμμετέχοντα αλλά μετά την ηχογράφηση <b> και ΜΟΝΟ για τις δύο πρώτες λέξεις </b> 
                    θα πρέπει να τις ακούσετε για να βεβαιωθείτε ότι έχουν ηχογραφηθεί σωστά και ότι ο υπολογιστής βρίσκεται στην 
                    σωστή απόσταση από τον συμμετέχοντα. Για τις υπόλοιπες λέξεις δεν χρειάζεται να ακούτε ούτε εσείς ούτε 
                    και ο συμμετέχων τις ηχογραφήσεις του. </p>
                <p> 7. Η δοκιμασία περιλαμβάνει την ηχογράφηση περίπου 182 λέξεων. 
                    Ζητάμε από κάθε συμμετέχοντα να πει το σύνολο των λέξεων.  </p>
                <p> 8. Οι λέξεις είναι οργανωμένες σε ομάδες των 42 λέξεων</p>
                <p> 9. Υπάρχει η δυνατότητα να κάνει παύση και να συνεχίσει μετά από λίγο. </p>
                <p> 10. Υπάρχει η δυνατότητα να διακόψει και να ξεκινήσει άλλη στιγμή από εκεί που άφησε την δοκιμασία. 
                    Αν επιλέξουμε να διακόψουμε την χορήγηση θα πρέπει να έχει ολοκληρωθεί η ηχογράφηση της λέξης πριν την 
                    διακοπή και να σημειώσετε τον αριθμό που αναγράφεται πάνω από την λέξη. Με αυτό τον τρόπο την επόμενη 
                    φορά θα μπορέσετε να συνεχίσετε από την επόμενη λέξη βάζοντας τον επόμενο αριθμό 
                    (πχ αν σταματήσατε στην λέξη νο 75, την επόμενη φορά στην αρχική οθόνη θα βάλετε τον αριθμό 76 
                    και θα συνεχίσετε από εκεί και έπειτα). </p>
                <p> 11. Μπορείτε και πρέπει να τον βοηθήσετε τον συμμετέχοντα αν δυσκολεύεται να χρησιμοποιήσει τον υπολογιστή κατά την ηχογράφηση, 
                    αλλά θα πρέπει να προσέξετε να μην μιλάτε ταυτόχρονα με αυτόν. </p>
              </Modal>
            </div>
                    */}
            <Modal title="Οδηγίες χορήγησης δοκιμασίας ηχογράφησης προς χορηγητές" 
                  visible={this.state.isModalVisible} 
                  onOk={handleOk} 
                  onCancel={handleCancel}
                  width={1000}>
              <p>
                <button onClick={() => {downloadPDF('instructions.pdf')}}>
                  Download Pdf
                </button>
              </p>
              </Modal>
          </Spin>
      )
  }
}
RecorderComponent.contextType = SentencesContext;

export default RecorderComponent;


