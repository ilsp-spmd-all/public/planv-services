import {BACKEND_URL} from './config.js';
import 'antd/dist/antd.css';
import { notification } from 'antd';
import fileDownload from 'js-file-download'
import { useKeycloak } from "@react-keycloak/web";
import { keycloak } from '../demo/src/keycloak.js';


function b64toBlob (b64Data, contentType='', sliceSize=512) {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];
  const {keycloak,initialize} = useKeycloak()

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

async function UserLogin(username, password) {
    const formData = new FormData();
    var status = false;

    formData.append('username', username);
    formData.append('password', password);

    const options = {
      method: 'POST',
      body: formData,
      credentials: 'include',
    };

    let res = await fetch(BACKEND_URL + "/login", options)
        .then((res)=> {if (res.status == 200) {
                          console.log("Login success");
                          return res.json();
                      } else if (res.status != 200) {
                            notification.error({
                            message: 'Failed to login.',
                            description: `error: ${res.status}`,
                          });
                          return false;
                      }})
        .then((data)=>data)
        .catch((e) => { 
                    notification.error({
                        message: 'Failed to login.',
                        description: `error: ${e}`,
                      });
                  return false; });
    console.log(res);
    return res;
}

async function UserLogout() {

    // const options = {
    //   method: 'GET',
    //   credentials: 'include',
    // };

    // let res = await fetch(BACKEND_URL + "/logout", options)
    //     .then((res)=> {if (res.status == 200) {
    //                       console.log("Logout success");
    //                       return true;
    //                   } else if (res.status != 200) {
    //                         notification.error({
    //                         message: 'Failed to logout.',
    //                         description: `error: ${res.status}`,
    //                       });
    //                       return false;
    //                   }}).catch((e) => { 
    //                                 notification.error({
    //                                     message: 'Failed to logout.',
    //                                     description: `error: ${e}`,
    //                                   });
    //                               return false; });
    let res = await keycloak.logout().then(
      response => {return response}
    )
    return res;
}

function getStories() {
    const options = {
      method: 'GET',
      credentials: 'include',
    };
    return fetch(BACKEND_URL + "/get_stories", options)
        .then((data)=>data.json())
        .then((res)=>res)
}

function getTestStories() {
  const options = {
    method: 'GET',
    credentials: 'include',
  };
  return fetch(BACKEND_URL + "/get_test_stories", options)
      .then((data)=>data.json())
      .then((res)=>res)
}

function getPhotos(id) {
    const options = {
      method: 'GET',
      credentials: 'include',
    };
    return fetch(BACKEND_URL + "/get_photos/" + id, options)
        .then((data)=>data.json())
        .then((res)=>res)
}

function getTestAudio(filename){
  const options = {
    method: 'GET',
    credentials: 'include',
  };
  return fetch(BACKEND_URL + "/get_test_audio/" + filename, options)
      .then((data)=>data.json())
      .then((res)=>res)
}

function getInstructionsAudio(id) {
    const options = {
      method: 'GET',
      credentials: 'include',
    };
    return fetch(BACKEND_URL + "/get_instructions/" + id, options)
        .then((data)=>data.json())
        .then((res)=>res)
}

function getAudio(id) {
    const options = {
      method: 'GET',
      credentials: 'include',
    };
    return fetch(BACKEND_URL + "/get_audio/" + id, options)
        .then((data)=>data.json())
        .then((res)=>res)
}

function getOtherAudio(id) {
  const options = {
    method: 'GET',
    credentials: 'include',
  };
  return fetch(BACKEND_URL + "/get_other_audio/" + id, options)
      .then((data)=>data.json())
      .then((res)=>res)
}

function downloadPDF(filename){
  const options = {
    method: 'GET',
    credentials: 'include',
  };
  return fetch(BACKEND_URL + "/download_instructions", options)
    .then((data)=>data.json())
    .then((res)=> {
      var blob = b64toBlob(res.pdf, 'application/pdf');
      fileDownload(blob, filename)})
}

function getRecording(filename){
  const options = {
    method: 'GET',
    credentials: 'include',
  };
  return fetch(BACKEND_URL + "/get_recording/" + filename, options)
      .then((data)=>data.json())
      .then((res)=>res)
}

export { UserLogin, UserLogout, getStories, getPhotos, getInstructionsAudio, getAudio, downloadPDF, getRecording, getOtherAudio};
