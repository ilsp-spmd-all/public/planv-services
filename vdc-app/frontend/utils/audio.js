import 'antd/dist/antd.css';
import { notification } from 'antd';
import { BACKEND_URL } from './config';

/**
 * Get access to the users microphone through the browser.
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia#Using_the_new_API_in_older_browsers
 */
function getAudioStream() {
  // Older browsers might not implement mediaDevices at all, so we set an empty object first
  if (navigator.mediaDevices === undefined) {
    navigator.mediaDevices = {};
  }

  // Some browsers partially implement mediaDevices. We can't just assign an object
  // with getUserMedia as it would overwrite existing properties.
  // Here, we will just add the getUserMedia property if it's missing.
  if (navigator.mediaDevices.getUserMedia === undefined) {
    navigator.mediaDevices.getUserMedia = function (constraints) {
      // First get ahold of the legacy getUserMedia, if present
      const getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

      // Some browsers just don't implement it - return a rejected promise with an error
      // to keep a consistent interface
      if (!getUserMedia) {
        return Promise.reject(
          new Error('getUserMedia is not implemented in this browser'),
        );
      }

      // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
      return new Promise(((resolve, reject) => {
        getUserMedia.call(navigator, constraints, resolve, reject);
      }));
    };
  }

  const params = { audio: true, video: false };

  return navigator.mediaDevices.getUserMedia(params);
}


/**
 * Samples the buffer at 16 kHz.
 */
function downsampleBuffer(buffer, recordSampleRate, exportSampleRate) {
  if (exportSampleRate === recordSampleRate) {
    return buffer;
  }

  const sampleRateRatio = recordSampleRate / exportSampleRate;
  const newLength = Math.round(buffer.length / sampleRateRatio);
  const result = new Float32Array(newLength);

  let offsetResult = 0;
  let offsetBuffer = 0;

  while (offsetResult < result.length) {
    const nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);
    let accum = 0;
    let count = 0;

    for (let i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {
      accum += buffer[i];
      count++;
    }

    result[offsetResult] = accum / count;
    offsetResult++;
    offsetBuffer = nextOffsetBuffer;
  }

  return result;
}

function floatTo16BitPCM(output, offset, input) {
  for (let i = 0; i < input.length; i++, offset += 2) {
    const s = Math.max(-1, Math.min(1, input[i]));
    output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7fff, true);
  }
}

function writeString(view, offset, string) {
  for (let i = 0; i < string.length; i++) {
    view.setUint8(offset + i, string.charCodeAt(i));
  }
}





function createHeader(sampleRate, length) {
  // // create the buffer and view to create the .WAV file
  // const buffer = new ArrayBuffer(44);
  // const view = new DataView(buffer);
  // // write the WAV container, check spec at: https://ccrma.stanford.edu/courses/422/projects/WaveFormat/
  // // RIFF chunk descriptor
  // writeString(view, 0, 'RIFF');
  // view.setUint32(4, 44 + length * 1, true);
  // writeString(view, 8, 'WAVE');
  // // FMT sub-chunk
  // writeString(view, 12, 'fmt ');
  // view.setUint32(16, 16, true);
  // view.setUint16(20, 1, true);
  // // mono (1 channels)
  // view.setUint16(22, 1, true);
  // view.setUint32(24, sampleRate, true);
  // view.setUint32(28, (sampleRate * 16 * 1) / 8, true);
  // view.setUint16(32, (16 * 1) / 8, true);
  // view.setUint16(34, 16, true);
  // // data sub-chunk
  // writeString(view, 36, 'data');
  // view.setUint32(40, length * 1, true);

  // return buffer;

  let numberOfChannels =1;
  let bitDepth=16;
  let bytesPerSample=16;
  var headerLength = 44;
  var wav = new Uint8Array( headerLength);
  var view = new DataView( wav.buffer );

  view.setUint32( 0, 1380533830, false ); // RIFF identifier 'RIFF'
  view.setUint32( 4, 36 + length, true ); // file length minus RIFF identifier length and file description length
  view.setUint32( 8, 1463899717, false ); // RIFF type 'WAVE'
  view.setUint32( 12, 1718449184, false ); // format chunk identifier 'fmt '
  view.setUint32( 16, 16, true ); // format chunk length
  view.setUint16( 20, 1, true ); // sample format (raw)
  view.setUint16( 22, numberOfChannels, true ); // channel count
  view.setUint32( 24, sampleRate, true ); // sample rate
  view.setUint32( 28, sampleRate * bytesPerSample * numberOfChannels, true ); // byte rate (sample rate * block align)
  view.setUint16( 32, bytesPerSample * numberOfChannels, true ); // block align (channel count * bytes per sample)
  view.setUint16( 34, bitDepth, true ); // bits per sample
  view.setUint32( 36, 1684108385, false); // data chunk identifier 'data'
  view.setUint32( 40, length, true ); // data chunk length

  return wav
}
/**
 * Encodes the buffer as a WAV file.
 */
function encodeWAV(samples, sampleRate) {
  const buffer = new ArrayBuffer(44 + samples.length * 2);
  const view = new DataView(buffer);

  writeString(view, 0, 'RIFF');
  view.setUint32(4, 32 + samples.length * 2, true);
  writeString(view, 8, 'WAVE');
  writeString(view, 12, 'fmt ');
  view.setUint32(16, 16, true);
  view.setUint16(20, 1, true);
  view.setUint16(22, 1, true);
  view.setUint32(24, sampleRate, true);
  view.setUint32(28, sampleRate * 2, true);
  view.setUint16(32, 2, true);
  view.setUint16(34, 16, true);
  writeString(view, 36, 'data');
  view.setUint32(40, samples.length * 2, true);
  floatTo16BitPCM(view, 44, samples);

  return view;
}

/**
 * Samples the buffer at 16 kHz.
 * Encodes the buffer as a WAV file.
 * Returns the encoded audio as a Blob.
 */
function exportBuffer(recBuffer, sampleRate) {
  const exportSampleRate = 16000;
  const downsampledBuffer = downsampleBuffer(recBuffer, sampleRate, exportSampleRate);
  const encodedWav = encodeWAV(downsampledBuffer, exportSampleRate);
  const audioBlob = new Blob([encodedWav], {
    type: 'audio/wav',
  });

  return audioBlob;
}

async function uploadFile(audio, filename, bucket) {
  const formData = new FormData();
  const status = false;

  formData.append('audio', audio, filename);

  const options = {
    method: 'POST',
    body: formData,
  };

  const res = await fetch(`${BACKEND_URL}/save_audio`, options)
    .then((res) => {
      if (res.status == 200) {
        saveFileS3(filename, bucket);
        return true;
      } if (res.status != 200) {
        notification.error({
          message: 'Failed to save the recording.',
          description: `error: ${res.status}`,
        });
        return false;
      }
    });
  return res;
}

function getCompletedSentences(username, device, behaviors_len) {
  return fetch(`${BACKEND_URL}/get_completed_sentences/${username}/${device}/${behaviors_len}`)
    .then(data => data.json())
    .then(res => res);
}

function saveFileS3(filename, bucket) {
  return fetch(`${BACKEND_URL}/save_file_s3/${filename}/${bucket}`);
}

export {
  getAudioStream, exportBuffer, uploadFile, getCompletedSentences, createHeader, downsampleBuffer,
};

