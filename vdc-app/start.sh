#!/bin/sh
echo "Running backend and frontend in two different tabs"
gnome-terminal --tab -- /bin/sh -c  'cd backend && gunicorn wsgi:app -b 0.0.0.0:5000 -k flask_sockets.worker '
gnome-terminal --tab -- /bin/sh -c ' cd frontend && npm start'

