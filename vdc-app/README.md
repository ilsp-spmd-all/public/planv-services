# Voice Data Collection

## How To Run
1.  Navigate to `https://apps.ilsp.gr/planv`

## Remote Deployment
Install and setup Nginx
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04

### Backend
1.  Copy backend folder to `/var/www/apps/planv/backend/` on vm.
    `sudo mkdir -p /var/www/apps/planv && sudo cp -r backend/ /var/www/apps/planv/backend`
2.  `cd /var/www/apps/planv/backend/`
3.  Create virtual environment and install requirements:
    1. `virtualenv -p python3 ~/planv_env && source ~/planv_env/bin/activate`
    2. `pip3 install -r requirements.txt` 
4.  Create Gunicorn service:
    https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04
    1.  `sudo vim /etc/systemd/system/planv.service`
    2.  paste the following code:

            [Unit]
            Description=Gunicorn instance to serve planv
            After=network.target

            [Service]
            User=dimastro
            Group=www-data
            WorkingDirectory=/var/www/apps/planv/backend
            Environment="PATH=/home/dimastro/planv_env/bin"
            ExecStart=/home/dimastro/planv_env/bin/gunicorn --workers 1 --bind unix:planv.sock -m 007 wsgi:app

            [Install]
            WantedBy=multi-user.target

    3.  Autostart service on boot:
        `sudo systemctl enable planv.service`

5.  Create Nginx config for the app:
    1. `sudo vim /etc/nginx/sites-available/planv_backend`
    2. paste the following code:

            server {
                    listen 5000 ssl;
                    #listen [::]:80;

                    server_name apps.ilsp.gr;
                    ssl_certificate /home/dimastro/ssl_certificate/apps.ilsp.gr.crt;
                    ssl_certificate_key /home/dimastro/ssl_certificate/apps.ilsp.gr.key;
                    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
                    ssl_prefer_server_ciphers on;
                    ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';

                    location / {
                        include proxy_params;
                        proxy_pass http://unix:/var/www/apps/planv/backend/planv.sock;
                    }
            }

6.  Init database:
    1.  `cd /var/www/apps/planv/backend/`
    2.  `ipython3`
        `from __init__ import db, create_app`
        `create_app()`


### Frontend
1.  Install npm
    1. curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash
    2. export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
    3. [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
    4. nvm install node

2.  `cd ./frontend`
3.  `npm install`
4.  `make reset`
5.  `make deploy`
7.  `make build`
8.  if local build `rsync -azP demo/dist <username>@<IP>:~/`
    if vm build: `cp -r demo/dist ~/`
9.  on vm:
    1. `sudo mkdir -p /var/www/planv/`
    2. `sudo mv ~/dist/* /var/www/planv/`

