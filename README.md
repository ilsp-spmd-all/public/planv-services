# Plan-V services

## General Info
This repo contains code for the PLan-V project services. It is consisted of a Voice Data Collection (VDC) App written in Flask (backend) and React (frontend) and a FastAPI microservice. A more comprehensive guide to the VDC app can be found here. The communication between the VDC's flask backend with the FastAPI microservice is achieved through a RabbitMQ connection. While VDC's recordings are streamed to flask backend through a websocket connection, the same recordings are then streamed to the FastAPI through the RabbitMQ connection. FastAPI microservice is responsible for the data processing and any machine learning applications. When calculations in the microservice are open, they are sent back to the Flask backend which then forwards them to React's frontend through a new websocket connection.

## Generic Folder Structure View
    .
    ├── services                # FastAPI Microservice
    ├── vdc-app                 # Voice Data Collection App
        ├── backend             # Flask Backend
        ├── frontend            # React Frontend

## Setup

The setup for the Voice Data Collection App is described in both `vdc-app/README.md` and in `vdc-app/how_to.md`. However, instead of using the `vdc-app/backend/requirements.txt`, you need to use the `requirements.txt` found in parent directory. These requierements also contain the python requirements for the FastAPI microservice and the asychronous RabbitMQ connection.

`python3 -m venv venv`

`source venv/bin/activate`

`LLVM_CONFIG=/usr/bin/llvm-config`

`pip3 install -r requirements.txt`

In case of a potential issue with `librosa` package, try: 
`LLVM_CONFIG=/usr/bin/llvm-config pip3 install llvmlite==0.31.0 numba==0.48.0 colorama==0.3.9 librosa==0.6.3`

One slight change in the instructions provided in VDC's instruction is the `.env` file which should be changed to

    SECRET_KEY="<secret_key>"
    DEV_DATABASE_URI="sqlite:///db.sqlite"
    PROD_DATABASE_URI="sqlite:///db_prod.sqlite"
    SQLALCHEMY_BINDS={'results': 'sqlite:///results.sqlite'}

After running the commands in backend to create the database, do observe that two `*.sqlite` files will emerge in `vdc-app/backend`; one contains info about the credentials and the other about the microservice's results.

Note: If you face an issue with frontend after installing packages, this issue is forwarded from the main VDC app. A quick fix is to update npm. 

If issue with npm, install recommended version
## Run the sevices
To run the services, follow the provided next steps

### Step 1: Run RabbitMQ Docker image
`docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management`

### Step 2: Run Flask backend 
In directory `vdc-app/backend`, run the following command:

`gunicorn wsgi:app -b 0.0.0.0:5000 -k flask_sockets.worker`

### Step 3: Run React frontend
In directory `vdc-app/frontend`, run the following command:

`npm start`

### Step 4: Run FastAPI microservice
In directory `services`, run the following command:

`uvicorn subservice:app`

## Evaluation
Run the app and make your recordings. Once you reach the Final Page, you must be able to inspect your recordings in both `vdc-app/backend/recordings` and `services/subservice_recordings`. However, once you click `Αποτελέσματα` button the recordings stored `services/subservice_recordings` will be deleted and the results will appear in React's Final Page. The results for now are fixed until data will be collected from Greek aphasic patients given that an AQ score is provided.

## Future work 
- [ ] Django DB in separate port
- [ ] Prolong RabbitMQ connection timeout ( current timeout 60 seconds; closes if idle )
- [ ] Add Models to FastAPI's microservice once proper dataset is provided
