import asyncio
from aio_pika import connect_robust, IncomingMessage, Message
import aio_pika
from fastapi import FastAPI
import numpy as np
# import base64
import os
import threading
from pathlib import Path
import requests
import  pickle
import pandas as pd
from tqdm import tqdm
tqdm.pandas()
from feature_extraction import DatasetFeatureExtractorNew
import librosa
from feature_names import FEATURES_NAMES
# import whisper
from pywhispercpp.model import Model
# import logging
import json
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

DATABASE_URL = os.environ["DB_URL"]
engine = create_engine(DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

whisper_model = 'large'
whisper_path = Path("/vdc/models")
INPUT_DIM = 4  # X is 2-dimensional
HIDDEN_DIM = 10
NUM_CLASSES = 4  # 3 classes



def get_audio_duration(audio_path):
    return librosa.get_duration(filename=audio_path)

#load the classifier model and scaler
with open ("svm_model_aq_cross.pkl", "rb") as f:
    clf = pickle.load(f)
with open ("scaler_aq_cross.pkl", "rb") as f:
    scaler = pickle.load(f)

# Create FastAPI
app = FastAPI()

# Create folder if doesn't exist
FOLDER_RECORDINGS = "./subservice_recordings/test1"
Path(FOLDER_RECORDINGS).mkdir(parents=True, exist_ok=True)

id_to_severity_dict = {0: "mild", 1: "moderate", 2: "severe", 3: "very severe"}

def create_df_from_list(user_recordings_list):
        df = pd.DataFrame()
        df["audio_path"] = user_recordings_list
        df["Participant ID"]=df["audio_path"].map(lambda x: os.path.basename(x).split(".")[0].split("_")[0])
        df["story"]=df["audio_path"].map(lambda x: os.path.basename(x).split(".")[0].split("_")[1])
        df["session"]=df["audio_path"].map(lambda x: os.path.basename(x).split(".")[0].split("_")[2])
        df["duration"]=df["audio_path"].map(get_audio_duration)

        return df

def transcribe_audio_whisper(audio_file, model, lang="el", task="transcribe"):

        options = dict(language=lang, beam_size=5, best_of=5)
        transcribe_options = dict(task=task, **options)
        result = model.transcribe(audio_file, **transcribe_options)

        return result["text"]

def transcribe_audio_whispercpp(audio_file, model, lang="el", task="transcribe"):

    segments = model.transcribe(audio_file, speed_up=True,language=lang)
    text=[]
    for segment in segments:
        text.append(segment.text) 
    text=" ".join(text)
    return text

def calculate(user_recordings_list,whisper_model, whisper_path, user_name="user1",story_id="session1"):

    df = create_df_from_list(user_recordings_list)


    # model = whisper.load_model(whisper_model)
    model=Model(whisper_model, models_dir=whisper_path)
    utterance_column_name = f"whisper_{whisper_model}"
    print("Transcribing audio files...")
    df[utterance_column_name]=df["audio_path"].progress_map(lambda x: transcribe_audio_whispercpp(x,model,lang="el"))
    print("Transcribing audio files...Done")
    print(df)
    
    feature_extractor = DatasetFeatureExtractorNew(df,language="el",utterance_column=utterance_column_name,duration_seconds_column="duration")

    df_feats_stories = feature_extractor.calculate_features_per_story()
    # print(df_feats_stories.iloc[0])
    df_feats_stories.rename(columns={utterance_column_name:"transcription"},inplace=True)
    # print(df_feats_stories.iloc[0])
    # print(df_feats.head())

    def create_stories_dict(df_feats_stories,user_name,story_id):
        #create a stories_dict dictionary like the json file in the demo
        stories=["stroke","umbrella","cat","cinderella","party","ring","rabbit"]
        stories_dict={}

        if len(df_feats_stories["Participant ID"].unique())>1:
            print("More than one participant")
            exit()
        df_feats_stories["user_name"]=user_name
        df_feats_stories["story_id"]=story_id

        for story in stories:
            if story not in df_feats_stories["story"].unique():
                continue
            part_dict=df_feats_stories.drop(columns=["Participant ID"])
            stories_dict=part_dict[part_dict["story"]==story].to_dict(orient="records")[0]
        return stories_dict
    stories_dict=create_stories_dict(df_feats_stories,user_name=user_name,story_id=story_id)

    return stories_dict

def calculate_mean_scores(stories,feature_set=FEATURES_NAMES):
    #calculate the mean feature values of all stories and add to the dictionary
    mean_scores={}
    print(f"calculate_mean_scores {stories}", flush=True)
    for feat in feature_set:
        values = [story[feat] for story in stories if feat in story and story[feat] is not None]
        if values:
            mean_scores[feat] = np.mean(values)
    return mean_scores


def classify_severity(stories_dict,clf, scaler,feature_set=FEATURES_NAMES):
    #classify the severity of the user based on the mean feature values   
    print(f"classify severity {stories_dict.values()}", flush=True)
    X = np.array(list(stories_dict.values()))
    print(f"classify severity X {X}", flush=True)
    X=np.expand_dims(X,axis=0)

    X_test = scaler.transform(X)
    clf_preds = clf.predict(X_test)
    clf_probas = clf.predict_proba(X_test)
    stories_dict["severity"]=id_to_severity_dict[clf_preds[0]]
    return clf_preds, clf_probas




# Global Variables
msg_thread = None
audiofile = None
res = bytearray()
channel = None
exchange = None


@app.on_event("startup")
async def startup():
    # global msg_thread
    # loop = asyncio.set_event_loop()
    # def consumer(loop):
    #     asyncio.set_event_loop(loop)
    #     loop.create_task(consume(loop))
    #     loop.run_forever()
    #
    #
    # consumer_thread = threading.Thread(target=asyncio.run, args=(consumer(loop, "ws"),))
    # print(f"DEBUG:::::: before start", flush=True)
    # consumer_thread.start()
    # print(f"DEBUG:::::: after start", flush=True)
    #
    # # Start the publisher thread with a queue name of "my_other_queue" and a routing key of "my_routing_key"
    # publisher_thread = threading.Thread(target=asyncio.run, args=(publisher(loop, "results", "results"),))
    # publisher_thread.start()
    # print(f"DEBUG:::::: after start2", flush=True)
    #
    # # Wait for both threads to finish
    # consumer_thread.join()
    # publisher_thread.join()
    #
    # # publisher_thread = threading.Thread(target=publish, daemon=True)
    # # publisher_thread.start()
    # # msg_thread = threading.Thread(target=consumer, args=(loop,), daemon=True)
    # # msg_thread.start()
    #

    exchange_name = "default_exchange"

    print(f"DEBUG:::::: before start", flush=True)
    # Define a function to create a new instance of the queue
    def create_async_queue():
        return asyncio.Queue()
    
    # Define a function to start the event loop and run the async functions
    async def start_async(queue_factory, exchange_name):
        queue = queue_factory()
        consumer_task = asyncio.create_task(consumer(queue, exchange_name))
        publisher_task = asyncio.create_task(publisher(queue, exchange_name))
        # await asyncio.gather(consumer_task, publisher_task)
    
    # Define a function to start the event loop in a new thread
    def start_event_loop(queue_factory, exchange_name):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        asyncio.ensure_future(start_async(queue_factory, exchange_name))
        loop.run_forever()
        # loop.run_until_complete(start_async(queue_factory, exchange_name))
    
    # Start the event loop in a new thread
    thread = threading.Thread(target=start_event_loop, args=(create_async_queue, exchange_name))
    thread.start()
    
    # Wait for the thread to finish
    thread.join()
    print(f"DEBUG:::::: after end", flush=True)


@app.get("/")
def read_root():
    return {"Hello": "World"}


header_flag = {}
audio = {}


async def on_message(message: IncomingMessage, queue):
    print(f"DEBUG::::::::::: onmessage", flush=True)
    global channel
    global header_flag, audio

    data = message.body
    if data:
        data = pickle.loads(data)

    # str_message = data.decode("utf-8")
    # str_message = str(data)
    if data["control"] == "start":
        # tmp = data.decode("utf-8")
        # filename = tmp.split('-')[-1]
        audio[data["user"]] = bytearray()
        header_flag[data["user"]] = False
        print(f"1. Start")
    elif data["control"] == "header":
        print("3. Send Header")
        header_flag[data["user"]] = True
    elif data["control"] == "test":
        print(f"DEBUG::::::::::: test", flush=True)
        test_b = {"average_utterance_length": 4.0, "verbs_words_ratio": 0.0, "nouns_words_ratio": 0.25, "adjectives_words_ratio": 0.0, "adverbs_words_ratio": 0.0, "conjuctions_words_ratio": 0.0, "prepositions_words_ratio": 0.01953125, "words_per_minute": 0.0, "verbs_per_utterance": 1.0, "unique_words_per_words": 0, "nouns_verbs_ratio": 4, "open_close_class_ratio": 0.3333333333333333, "mean_clauses_per_utterance": 0.0, "mean_dependent_clauses": 0.0, "mean_independent_clauses": 0.0, "dependent_all_clauses_ratio": 0, "mean_tree_height": 2.0, "max_tree_depth": 2, "n_independent": 0, "n_dependent": 0, "propositional_density": 0.0, "words_in_vocabulary_per_words": 0.75, "unique_words_in_vocabulary_per_words": 0.75, "determiners_words_ratio": 0.0, "pronouns_words_ratio": 0.0, "interjections_words_ratio": 0.0, "story": "stroke", "text": " \\u039b\\u03ad\\u03bd\\u03b1 \\u03b4\\u03cd\\u03bf \\u03c4\\u03c1\\u03af\\u03b1 \\u03c4\\u03b5\\u03c3\\u03c5\\u03b2\\u03ac.", "story_id": 1}
        # await queue.put(json.dumps(test_b))
    elif data["control"] == "results":
        # username = data.decode("utf-8").split("-")[-1]
        username = data["user"]
        story_id = data["story_id"]
        filename = data["message"]
        # search all recordings in FOLDER_RECORDINGS with the corresponding username
        user_recordings_list = [
            os.path.join(FOLDER_RECORDINGS, wavfile) for wavfile in os.listdir(FOLDER_RECORDINGS) if filename in wavfile
        ]
        print(f"user_recording_list {user_recordings_list}", flush=True)
        story_dict=calculate(user_recordings_list, whisper_model, whisper_path, username, story_id)
        story_name = user_recordings_list[0]

        # stories_dict=calculate_mean_scores(stories_dict)
        # clf_preds,clf_probas= classify_severity(story_dict,clf, scaler)
        # stories_dict["mean_scores"]["severity"]=id_to_severity_dict[clf_preds]

        # sending post request and saving response as response object

        story_dict["control"] = "story_results"
        await queue.put(json.dumps(story_dict))
        for wavfile in user_recordings_list:
            os.remove(wavfile)

        # r = requests.post(
        #     url="http://localhost:5000/post_severity_results", json=story_dict
        # )  # send to database (mongodb)
        # if r.status_code == 200:
        #     # delete user's wavfiles
        #     for wavfile in user_recordings_list:
        #         os.remove(os.path.join(FOLDER_RECORDINGS, wavfile))
    elif data["control"] == "mean_scores":
        await asyncio.sleep(10)
        params = {
            'username': data["user"]
        }
        print(f"mean_scores user {data['user']}", flush=True)
        stories_results = requests.get("http://vdc-backend:5000/get_stories_results", params=params)
        stories_results_json = stories_results.json()
        print(f"mean_scores stories {stories_results_json}", flush=True)
        mean_scores=calculate_mean_scores(stories_results_json["stories_list"])
        clf_preds,clf_probas= classify_severity(mean_scores,clf, scaler)
        mean_scores["results_id"] = stories_results_json["id"]
        mean_scores["control"] = "mean_scores"
        await queue.put(json.dumps(mean_scores))
        print(f"mean_scores {mean_scores}", flush=True)
    elif data["control"] == "streaming-data":
        if not header_flag[data["user"]]:
            msg = data["message"]
            audio[data["user"]] += msg
            tmp = audio[data["user"]]
            # audio[filename] += data
            print(f"2. Stream Data. Audio length = {len(tmp)}")
    elif data["control"] == "streaming-header":
        if header_flag[data["user"]]:
            msg = data["message"]
            print(f"4. Header sent: {msg}")
            audio[data["user"]] = msg + audio[data["user"]]
    elif data["control"] == "end":
        filename = data["message"]
        print(f"5. End and write auido in {filename}")
        with open("{}/{}".format(FOLDER_RECORDINGS, filename), "wb") as file:
            file.write(audio[data["user"]])
        
        # remove user's audio and flag from dict
        del audio[data["user"]]
        del header_flag[data["user"]]
    await message.ack()


async def consumer(async_queue, exchange_name):
    print(f"DEBUG:::::: consumer", flush=True)
    connection = await connect_robust("amqp://vdc:vdc@rabbitmq:5672")
    async with connection:
        # Create a channel
        channel = await connection.channel()

        # Create a queue
        queue = await channel.declare_queue("ws", durable=True)

        # Bind the queue to the exchange with the routing key
        await queue.bind(exchange_name, routing_key="ws")

        print(f"DEBUG:::::: consumer before on_message", flush=True)
        # Start consuming messages
        # async for message in queue:
        #     await on_message(message, async_queue)
        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                await on_message(message, async_queue)
        # await queue.consume(lambda message: asyncio.create_task(on_message(message, async_queue)))


async def publisher(async_queue, exchange_name):
    print(f"DEBUG:::::: publisher", flush=True)
    connection = await connect_robust("amqp://vdc:vdc@rabbitmq:5672")
    async with connection:
        # Create a channel
        channel = await connection.channel()

        # Create an exchange
        exchange = await channel.declare_exchange(exchange_name, aio_pika.ExchangeType.DIRECT)
        queue = await channel.declare_queue("results", durable=True)
        #await queue.bind(exchange_name, routing_key="results")

        while True:
            print(f"DEBUG:::::: publisher while True", flush=True)
            message = await async_queue.get()
            print(f"DEBUG:::::: messageeeeee {message}", flush=True)
            await exchange.publish(
                aio_pika.Message(body=message.encode(), delivery_mode=2),
                routing_key="results"
            )





if __name__ == "__main__":
    # story_audios=["/home/plitsis/planv/planv-services/services/subservice_recordings/test1/025_cat_1642148388787.wav","/home/plitsis/planv/planv-services/services/subservice_recordings/025_cinderella_1642148388787.wav"]
    # story_audios=["/home/plitsis/planv/planv-services/services/subservice_recordings/test1/025_cat_1642148388787.wav"]
    story_audios=["/home/plitsis/planv/planv-services/services/pepi_cat_123.wav"]

    stories_dict=calculate(story_audios,"large")
    print(stories_dict)
    # stories_dict=calculate_mean_scores(stories_dict)
    # clf_preds,clf_probas= classify_severity(stories_dict,clf, scaler)
    # print(clf_preds,clf_probas)
    # print(stories_dict["severity"])
    # stories_dict["mean_scores"]["severity"]=id_to_severity_dict[clf_preds[0]]
    # with open("severity_dict.json", "w") as outfile:
    #     json.dump(stories_dict, outfile)
