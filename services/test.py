from pywhispercpp.model import Model
from time import process_time
import time
from pathlib import Path

audio_path='/home/plitsis/planv/planv-services/services/subservice_recordings/test1/025_cat_1642148388787.wav'
models_dir_path=Path("/home/plitsis/.local/share/pywhispercpp/models")

model = Model('medium',models_dir=models_dir_path, n_threads=15)
t1_start = time.perf_counter()
segments = model.transcribe(audio_path, speed_up=True,language='el')
for segment in segments:
    print(segment.text)
t1_stop = time.perf_counter()
print("Elapsed time:", t1_stop, t1_start)
print("Elapsed time during the whole program in seconds:", t1_stop-t1_start)
print("going again...")


t1_start = time.perf_counter()
segments = model.transcribe(audio_path, speed_up=True,language='el')
for segment in segments:
    print(segment.text)
t1_stop = time.perf_counter()
print("Elapsed time:", t1_stop, t1_start)
print("Elapsed time during the whole program in seconds:", t1_stop-t1_start)
