import json
from pprint import pprint
import spacy
from utils import timestamp_to_duration_conversion
from spacy_conll import init_parser
import argparse
from transcript_cleaning import normalize_for_parsing,clean_f_l
# from textacy.export import doc_to_conll
from tqdm import tqdm
class GreekTEDxConlluParser(object):
    def __init__(self):
        self.nlp = init_parser("el_core_news_lg", "spacy", include_headers=True)

    def calculate_conllu(self, story_dict):
        for story in story_dict.keys():
            transcripts_dict = story_dict[story]["raw_transcriptions"]
            for transcript in transcripts_dict.keys():
                conllu_res = self.nlp(transcript)._.conll_str
                story_dict[story]["raw_transcriptions"][transcript][
                    "conllu"
                ] = conllu_res
        return story_dict

    def calculate_conllu(self, transcript):
        return self.nlp(transcript)._.conll_str


class EnglishTEDxConlluParser(object):
    def __init__(self):
        self.nlp = spacy.load("en_core_web_trf")
        config = {"ext_names": {"conll_pd": "pandas"}, "include_headers": True}
        # "conversion_maps": {"deprel": {"nsubj": "subj"}}}
        self.nlp.add_pipe("conll_formatter", config=config, last=True)

    def calculate_conllu(self, story_dict):
        for story in story_dict.keys():
            transcripts_dict = story_dict[story]["raw_transcriptions"]
            for transcript in transcripts_dict.keys():
                print(story, transcript)
                conllu_res = self.nlp(transcript)._.conll_str
                story_dict[story]["raw_transcriptions"][transcript][
                    "conllu"
                ] = conllu_res

        return story_dict

    def calculate_conllu_from_dict(self, data_dict):
        for speaker in data_dict.keys():
            for story in data_dict[speaker].keys():
                if story == "user_info":
                    continue
                transcripts_dict = data_dict[speaker][story]["raw_transcriptions"]
                for transcript in transcripts_dict.keys():
                    if transcript == "":
                        continue
                    print(story, transcript)
                    conllu_res = self.nlp(transcript)._.conll_str
                    data_dict[speaker][story]["raw_transcriptions"][transcript][
                        "conllu"
                    ] = conllu_res
        return data_dict


class SpacyConlluParser(object):
    '''
    Calculates conll string from an utterance, using a Spacy model
    Spacy models to use:
    English: "en_core_web_trf"
    French: "fr_dep_news_trf"
    Mandarin: "zh_core_web_lg"
    '''
    def __init__(self,spacy_model_name="en_core_web_trf"):
        self.nlp = spacy.load(spacy_model_name)
        # config = {"ext_names": {"conll_pd": "pandas"}, "include_headers": True}
        # "conversion_maps": {"deprel": {"nsubj": "subj"}}}
        # self.nlp.add_pipe("conll_formatter", config=config, last=True)
        self.nlp.add_pipe("conll_formatter", last=True)

    def calculate_conllu_from_sentence(self, transcript: str,normalize=True):
        if normalize:
            transcript=clean_f_l(transcript)
            transcript=normalize_for_parsing(transcript)
        return self.nlp(transcript)._.conll_str
    from spacy.language import Language
    from spacy.tokens import Doc

    
    def calculate_conllus_from_list(self, list_of_texts,normalize=True):
        conllus=[]
        if normalize:
            # self.nlp.add_pipe("normalize_before_parser",first=True)
            temp=[]
            for transcript in list_of_texts:
                transcript=clean_f_l(transcript)
                transcript=normalize_for_parsing(transcript)
                temp.append(transcript)
            list_of_texts=temp
        for doc in tqdm(self.nlp.pipe(list_of_texts),total=len(list_of_texts)):
            conllus.append(doc._.conll_str)
        # return self.nlp(transcript)._.conll_str/
        return conllus

class FrenchConlluParser(object):
    def __init__(self):
        self.nlp = spacy.load("fr_dep_news_trf")
        config = {"ext_names": {"conll_pd": "pandas"}, "include_headers": True}
        # "conversion_maps": {"deprel": {"nsubj": "subj"}}}
        self.nlp.add_pipe("conll_formatter", config=config, last=True)

    def calculate_conllu_from_sentence(self, transcript: str):
        return self.nlp(transcript)._.conll_str


class EnglishConlluParser(object):
    def __init__(self):
        self.nlp = spacy.load("en_core_web_trf")
        config = {"ext_names": {"conll_pd": "pandas"}, "include_headers": True}
        # "conversion_maps": {"deprel": {"nsubj": "subj"}}}
        self.nlp.add_pipe("conll_formatter", config=config, last=True)

    def calculate_conllu_from_sentence(self, transcript: str):
        return self.nlp(transcript)._.conll_str

    def calculate_conllu(self, story_dict):
        for story in story_dict.keys():
            transcripts_dict = story_dict[story]["raw_transcriptions"]
            for transcript in transcripts_dict.keys():
                print(story, transcript)
                conllu_res = self.nlp(transcript)._.conll_str
                story_dict[story]["raw_transcriptions"][transcript][
                    "conllu"
                ] = conllu_res

        return story_dict

    def calculate_conllu_from_dict(
        self, data_dict, new_dict_format=True, use_asr_transcripts=False
    ):
        if use_asr_transcripts:
            transcript_type = "asr_prediction"
            conllu_name = "conllu-asr"
        else:
            transcript_type = "processed_transcript"
            conllu_name = "conllu"
        for speaker in data_dict.keys():
            for story in data_dict[speaker].keys():
                if story == "user_info":
                    continue
                if new_dict_format:
                    transcripts_dict = data_dict[speaker][story]["utterances"]
                else:
                    transcripts_dict = data_dict[speaker][story]["raw_transcriptions"]
                for transcript in transcripts_dict.keys():
                    if new_dict_format:
                        transcript_name = transcript
                        if (
                            transcript_type
                            not in transcripts_dict[transcript_name].keys()
                        ):
                            continue
                        transcript = transcripts_dict[transcript_name][transcript_type]
                    if transcript == "":
                        continue
                    print(story, transcript)
                    conllu_res = self.nlp(transcript)._.conll_str
                    if new_dict_format:
                        data_dict[speaker][story]["utterances"][transcript_name][
                            conllu_name
                        ] = conllu_res
                    else:
                        data_dict[speaker][story]["raw_transcriptions"][transcript][
                            conllu_name
                        ] = conllu_res
        return data_dict


class MandarinConlluParser(object):
    """ """

    def __init__(self):
        self.nlp = spacy.load("zh_core_web_lg")
        config = {"ext_names": {"conll_pd": "pandas"}, "include_headers": True}
        # "conversion_maps": {"deprel": {"nsubj": "subj"}}}
        self.nlp.add_pipe("conll_formatter", config=config, last=True)

    def calculate_conllu_from_sentence(self, transcript: str):
        return self.nlp(transcript)._.conll_str


class ChineseConlluParser(object):
    """
    https://spacy.io/models/zh#zh_core_web_trf
    """

    def __init__(self):
        self.nlp = spacy.load("zh_core_web_trf")
        config = {"ext_names": {"conll_pd": "pandas"}, "include_headers": True}
        # "conversion_maps": {"deprel": {"nsubj": "subj"}}}
        self.nlp.add_pipe("conll_formatter", config=config, last=True)

    def calculate_conllu(self, story_dict):
        for story in story_dict.keys():
            transcripts_dict = story_dict[story]["raw_transcriptions"]
            for transcript in transcripts_dict.keys():
                print(story, transcript)
                conllu_res = self.nlp(transcript)._.conll_str
                story_dict[story]["raw_transcriptions"][transcript][
                    "conllu"
                ] = conllu_res

        return story_dict

    def calculate_conllu_from_dict(self, data_dict):
        for speaker in data_dict.keys():
            for story in data_dict[speaker].keys():
                if story == "user_info":
                    continue
                transcripts_dict = data_dict[speaker][story]["raw_transcriptions"]
                for transcript in transcripts_dict.keys():
                    if transcript == "":
                        continue
                    print(story, transcript)
                    conllu_res = self.nlp(transcript)._.conll_str
                    data_dict[speaker][story]["raw_transcriptions"][transcript][
                        "conllu"
                    ] = conllu_res
        return data_dict


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Get dependency tree from transcript data."
    )
    parser.add_argument(
        "--data",
        default="data.json",
        help="Path to the data json file, containing transcripts and utterance timestamps.",
    )
    parser.add_argument(
        "--dest",
        default="processed_transcripts.json",
        help="Path to the json file where the processed data (transcripts + dependency tree) will be saved.",
    )
    args = parser.parse_args()
    with open(args.data, "rb") as json_file:
        data_dict = json.load(json_file)

    # Load spacy model and add spacy_conll configuration
    nlp = spacy.load("en_core_web_sm")
    config = {"ext_names": {"conll_pd": "pandas"}, "include_headers": True}
    # "conversion_maps": {"deprel": {"nsubj": "subj"}}}
    nlp.add_pipe("conll_formatter", config=config, last=True)

    # Iterate over users
    for user in data_dict.keys():
        # Iterate over stories
        story_dict = data_dict[user]
        for story in story_dict.keys():
            if story == "user_info":
                continue
            try:
                transcripts_dict = story_dict[story]["raw_transcriptions"]
            except:
                print(user, story)
            # iterate over transcripts
            for transcript in transcripts_dict.keys():
                timestamp = transcripts_dict[transcript]["timestamp"]
                duration = timestamp_to_duration_conversion(timestamp)
                conllu_res = nlp(transcript)._.conll_str
                data_dict[user][story]["raw_transcriptions"][transcript][
                    "duration"
                ] = duration
                data_dict[user][story]["raw_transcriptions"][transcript][
                    "conllu"
                ] = conllu_res
    with open(args.dest, "w", encoding="utf8") as f_json:
        f_json.write(json.dumps(data_dict, indent=4, ensure_ascii=False))
