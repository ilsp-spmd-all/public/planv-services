import argparse
import soundfile
import librosa 
import torchaudio
from transformers import Wav2Vec2Model, Wav2Vec2Processor, Wav2Vec2ForCTC
from pprint import pprint
import numpy as np
import torch 
import numpy
from pydub import AudioSegment
from pydub.silence import split_on_silence, detect_silence

torch.backends.quantized.engine = 'qnnpack'
torch.set_num_threads(1)


feats_names = [
    'speech-non-speech-ratio',
    'average-silence-segments-duration',
    'average-speech-segments-duration',
    'basic-frequency-per-respiratory-team',
    'words-per-minute'
]


def parse_arguments():
    parser = argparse.ArgumentParser(description='Acoustic Feature Extraction')
    parser.add_argument("--data-folder", type=str, default='/data/projects/covid-vri')
    parser.add_argument("--output_dir", type=str, default='/home/jerrychatz/projects')
    parser.add_argument("--frontend",  type=str, choices=['w2v2'], default='w2v2')
    parser.add_argument("--save-dict", type=bool, default=True)
    args = parser.parse_args()
    return args

def asr(fn, model, processor):
    resampler = torchaudio.transforms.Resample(44_100, 16_000)
    speech_array, sampling_rate = torchaudio.load(fn)
    resampled_speech = resampler(speech_array).squeeze().numpy()
    # resampled_speech = resampled_speech[:10000]
    inputs = processor(resampled_speech, sampling_rate=16_000, return_tensors="pt", padding=True)
    with torch.no_grad():
        logits = model(inputs.input_values, attention_mask=inputs.attention_mask).logits
    pred_ids = torch.argmax(logits, dim=-1)
    pred_ids = processor.decode(pred_ids[0])
    return pred_ids

def calculate_words_per_minute(model, processor, fn, total_frames, sr):
    duration_in_minutes = total_frames / sr / 60 
    transcription = asr(fn, model, processor)
    tokens = transcription.split(" ")
    print(tokens)
    print(duration_in_minutes)
    print(transcription)
    return len(tokens) / duration_in_minutes if duration_in_minutes > 0 else 0


def calculate_speech_non_speech_ratio(fn, total_frames):
    # option 1: 
    # https://github.com/wiseman/py-webrtcvad
    # option 2: [SELECTED]
    # https://github.com/snakers4/silero-vad
    # https://towardsdatascience.com/modern-portable-voice-activity-detector-released-417e516aadde
    # check out also this: 
    # https://github.com/pyannote/pyannote-audio/tree/master/pyannote/audio/models

    model, utils = torch.hub.load(repo_or_dir='snakers4/silero-vad',
                                model='silero_vad',
                                force_reload=False)
    (get_speech_ts,
    get_speech_ts_adaptive,
    save_audio,
    read_audio,
    state_generator,
    single_audio_stream,
    collect_chunks) = utils

    files_dir = torch.hub.get_dir() + '/snakers4_silero-vad_master/files'

    wav = read_audio(fn)
    # full audio
    # get speech timestamps from full audio file
    speech_timestamps = get_speech_ts(wav, model,
                                    num_steps=4)   
    # save_audio('only_speech.wav', collect_chunks(speech_timestamps, wav), 16000)  
    speech_frames = 0
    for timestamp in speech_timestamps:
        speech_frames += timestamp['end'] - timestamp['start']
    non_speech_frames = total_frames - speech_frames

    return speech_frames / non_speech_frames



def calculate_avg_silence_segments_duration(fn, sr=44100):

    sound = AudioSegment.from_wav(fn)
    dBFS = sound.dBFS
    silence_segments = detect_silence(sound, 
        min_silence_len = 200,
        # silence_thresh = dBFS-16,
    )
    silence_durations = [(end-start) / sr for start, end in silence_segments]
    return sum(silence_durations)/len(silence_durations) if len(silence_durations) != 0 else 0


def calculate_avg_speech_segments_duration(fn, sr=16000):
    # option 1: 
    # https://github.com/wiseman/py-webrtcvad
    # option 2: [SELECTED]
    # https://github.com/snakers4/silero-vad
    # https://towardsdatascience.com/modern-portable-voice-activity-detector-released-417e516aadde
    # check out also this: 
    # https://github.com/pyannote/pyannote-audio/tree/master/pyannote/audio/models

    model, utils = torch.hub.load(repo_or_dir='snakers4/silero-vad',
                                model='silero_vad',
                                force_reload=False)
    (get_speech_ts,
    get_speech_ts_adaptive,
    save_audio,
    read_audio,
    state_generator,
    single_audio_stream,
    collect_chunks) = utils

    files_dir = torch.hub.get_dir() + '/snakers4_silero-vad_master/files'

    wav = read_audio(fn)
    # full audio
    # get speech timestamps from full audio file
    speech_timestamps = get_speech_ts(wav, model,
                                    num_steps=4)   
    # save_audio('only_speech.wav', collect_chunks(speech_timestamps, wav), 16000)  
    speech_durations = [(timestamp['end'] - timestamp['start'])/sr for timestamp in speech_timestamps]
    return sum(speech_durations)/len(speech_durations) if len(speech_durations) != 0 else 0

def calculate_basic_freq_per_respiratory_team(fn, wav, sr):
    # option 1: 
    # https://github.com/wiseman/py-webrtcvad
    # option 2: [SELECTED]
    # https://github.com/snakers4/silero-vad
    # https://towardsdatascience.com/modern-portable-voice-activity-detector-released-417e516aadde
    # check out also this: 
    # https://github.com/pyannote/pyannote-audio/tree/master/pyannote/audio/models

    model, utils = torch.hub.load(repo_or_dir='snakers4/silero-vad',
                                model='silero_vad',
                                force_reload=False)
    (get_speech_ts,
    get_speech_ts_adaptive,
    save_audio,
    read_audio,
    state_generator,
    single_audio_stream,
    collect_chunks) = utils

    files_dir = torch.hub.get_dir() + '/snakers4_silero-vad_master/files'

    wav = read_audio(fn)
    # full audio
    # get speech timestamps from full audio file
    speech_timestamps = get_speech_ts(wav, model,
                                    num_steps=4)   
    # save_audio('only_speech.wav', collect_chunks(speech_timestamps, wav), 16000)  
    def extract_max(pitches,magnitudes, shape):
        new_pitches = []
        new_magnitudes = []
        for i in range(0, shape[1]):
            new_pitches.append(np.max(pitches[:,i]))
            new_magnitudes.append(np.max(magnitudes[:,i]))
        return (new_pitches,new_magnitudes)
    def smooth(x,window_len=11,window='hanning'):
        if window_len<3:
                return x
        if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
                print('ValueError', "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")
        s=np.r_[2*x[0]-x[window_len-1::-1],x,2*x[-1]-x[-1:-window_len:-1]]
        if window == 'flat': #moving average
                w=np.ones(window_len,'d')
        else:
                w=eval('numpy.'+window+'(window_len)')
        y=np.convolve(w/w.sum(),s,mode='same')
        return y[window_len:-window_len+1]
    # https://github.com/miromasat/pitch-detection-librosa-python/blob/master/script_final.py

    res_pitches = []
    for timestamp in speech_timestamps:
        wav_segment = wav[timestamp['start']: timestamp['end']+1].numpy()
        pitches, magnitudes = librosa.core.piptrack(y=wav_segment, sr=16000, S=None, threshold=0.75)
        shape = np.shape(pitches)
        #nb_samples = total_samples / hop_length
        nb_samples = shape[0]
        #nb_windows = n_fft / 2
        nb_windows = shape[1]
        pitches,magnitudes = extract_max(pitches, magnitudes, shape)
        pitches1 = smooth(pitches,window_len=10)
        res_pitches.append(np.array(pitches1))
    return speech_timestamps, np.asarray(res_pitches)
    
if __name__ == '__main__':
    args = parse_arguments()
    fn = '/home/jerrychatz/projects/athenarc/planv-services/vdc-app/backend/recordings/user_cinderella_1626681356143.wav'


    wav, sr = torchaudio.load(fn)
    if sr != 16000:
        print('Resampling to 16000')
        transform = torchaudio.transforms.Resample(orig_freq=sr,
                                                   new_freq=16000)
        wav = transform(wav)
        sr = 16000
        total_frames = wav.shape[1]
    
    model = Wav2Vec2ForCTC.from_pretrained("lighteternal/wav2vec2-large-xlsr-53-greek")
    processor = Wav2Vec2Processor.from_pretrained("lighteternal/wav2vec2-large-xlsr-53-greek")

    
    # 1. Speech - Non Speech Ratio 
    speech_non_speech_ratio = calculate_speech_non_speech_ratio(fn, total_frames=total_frames)
    print(speech_non_speech_ratio)

    # 2. Words per minute
    words_per_minute = calculate_words_per_minute(model, processor, fn, total_frames, sr)
    print(words_per_minute)
    # RESULTS
    # TRANSCRIPT
    # ήοόρμροόριμα όλαλίγι πρόπουλο γιαθίκίύθιγάθηκε κάνανει κτε ε νιοόχύμεκρνί χώρα είχε εν ατου αχθρόονταείγαίνεόμως 
    # τοση τοτο με το μέ το μετσιτοα εέχει καέσι καπχ  έθι παρτοέχει ιέ κάθε καίμότο εκάθε με ττριά αχιστελέλο   
    # μεεπουήθερε το τοκρέψει το ζνό ινίε που  όντας τοτο βασιλόολοτοδόψιλόπουλο κλουνόντας στο κοτοόρτα τσοτου 
    # τογετητηνεμπροσυνάδα αια ήρεψε α εείνε ιά φοσιάτο κόρη το τενιά όπήα ευβροφτεύτηκεκαι  το ή το το στηλίδη 
    # ε το το πο τοετον δαυλίρη τοαύριο τοαύγιόταν αεπάύσι ο άνοιξει   τοζαξλείρ εηπόζερατάτης πνερικριάτης  
    # βλέψε το τσοσι ι ξι κός ααιι εσοσε άρροος  γιγοσιοχρότ πήο ετο του ευλοζι κλέγε επήρε το σοχτλρη όχιε πήρε το 
    # ζεε επήρεπήρετοτοδετοδεπειδόνι  ε τοπεέκλαιγε και το ήθε ε του είπε τους είπε το ηείναι εα το είναιυτώ και 
    # τη τω και δαιπόβεο πάόπαπάπόπομ
    # WORDS PER MINUTE
    # 28.049546168781276

    # 3. Average silence segments duration
    avg_silence_segments_duration = calculate_avg_silence_segments_duration(fn)
    print(avg_silence_segments_duration)

    # 4. Average speech segments duration 
    avg_speech_segments_duration = calculate_avg_speech_segments_duration(fn, sr=16000)
    print(avg_speech_segments_duration)

    # 5. Pitch Detection per respiratory team
    pitches = calculate_basic_freq_per_respiratory_team(fn, wav, sr)
    
