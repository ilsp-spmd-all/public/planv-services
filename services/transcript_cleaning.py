import re
from transformers import WhisperProcessor
from transformers import Wav2Vec2Processor
import string


def filterWordsPhonetic(stringg):
    while stringg.find("[:") > 0:
        positionbracket = stringg.find("[:")
        if stringg[positionbracket - 1] != " ":  # If not an space before [:, add it
            stringg = stringg[:positionbracket] + " " + stringg[positionbracket:]
            positionbracket+=1
        if stringg[positionbracket + 2] != " ":  # If not an space after [:, add it
            stringg = stringg[:positionbracket+2] + " " + stringg[positionbracket+2:]
        list_words = stringg.split(" ")
        try:
            corchete_init = list_words.index("[:")
        except:
            corchete_init = list_words.index(
                "[::"
            )  # Sometimes they write [:: instead of [:

        list_words[corchete_init - 1] = list_words[corchete_init + 1][
            0:-1
        ]  # change the bad word for good word
        del list_words[corchete_init : corchete_init + 4]
        stringg = " ".join(list_words)
    return stringg


def clean_other_characters(stringg):
    stringg = str(stringg)
    chars_to_ignore_regex = '[\,\?\.\!\-\;\:"\“\%\‘\”\�\‡\„\$\^\/\//\0\↓\≠\↑]'
    stringg = re.sub(chars_to_ignore_regex, "", stringg).lower()

    stringg = re.sub("\+<", " ", stringg)

    stringg = re.sub("&=laughs", "<LAU>", stringg)
    stringg = re.sub("&=chuckles", "<LAU>", stringg)

    stringg = re.sub("&=nods", "", stringg)

    # tmp = re.sub(r"&=\S*\s"," ",tmp)

    stringg = re.sub("\&=\w*:\w*_\w*", "", stringg)
    stringg = re.sub("[\[].*?[\]]", "", stringg)
    stringg = re.sub(
        "\&=[a-z]*:[a-z]*", "", stringg
    )  # quita las trasncripciones tipo &=word:word

    stringg = re.sub(
        "&-.([a-z]*)", "<FLR>", stringg
    )  # quita las trasncripciones tipo &-word
    stringg = re.sub(
        "&.([a-z]*)", "<FLR>", stringg
    )  # quita las trasncripciones tipo &word

    stringg = re.sub("\+", " ", stringg)
    stringg = re.sub("[/&=] *", "<SPN> ", stringg)

    stringg = re.sub("dada@b", "<FLR>", stringg)
    # stringg = re.sub(chars_to_ignore_regex, "", stringg).lower()
    stringg = re.sub("_", " ", stringg)
    stringg = re.sub("\x15", "", stringg)
    stringg = re.sub("\)", "", stringg)
    stringg = re.sub("\(", "", stringg)
    stringg = re.sub("\[  gra", "", stringg)
    stringg = re.sub("   ", " ", stringg)
    stringg = re.sub("  ", " ", stringg)
    stringg = re.sub(r"[0-9]", "", stringg)
    stringg = re.sub("xn", "", stringg)
    stringg = re.sub(r"  ", " ", stringg)
    stringg = re.sub("blublublubluhb", "<SPN>", stringg)
    stringg = re.sub("www", "", stringg)
    return stringg


def remove_bad_words_ab_english(stringg):
    stringg = re.sub("<seeing>", "seeing", stringg)
    stringg = re.sub("<seeing them>", "seeing", stringg)
    stringg = re.sub("<wanna>", "wanna", stringg)
    stringg = re.sub("<that>", "that", stringg)
    stringg = re.sub("<yeah>", "yeah", stringg)
    stringg = re.sub("<okay>", "okay", stringg)
    stringg = re.sub("<just>", "just", stringg)
    stringg = re.sub("<with>", "with", stringg)
    stringg = re.sub("<raindrops>", "raindrops", stringg)
    stringg = re.sub("<and>", "and", stringg)
    return stringg


def remove_even_more(stringg):
    stringg = re.sub("\[<spn", "<spn>", stringg)
    stringg = re.sub("\[<", "", stringg)
    stringg = re.sub("\[", "", stringg)
    stringg = re.sub("\[ gr", "", stringg)
    stringg = re.sub("\[ g", "", stringg)

    stringg = re.sub("\[ jar", "", stringg)
    stringg = re.sub("\[ ja", "", stringg)
    stringg = re.sub("\[ j", "", stringg)

    stringg = re.sub("\[ exc", "", stringg)
    stringg = re.sub("\[ ex", "", stringg)
    stringg = re.sub("\[ e", "", stringg)

    stringg = re.sub("\[ es", "", stringg)

    stringg = re.sub("\[ ci", "", stringg)
    stringg = re.sub("\[ c", "", stringg)

    stringg = re.sub("gram]", "", stringg)
    stringg = re.sub("pn]", "", stringg)
    stringg = re.sub("nk]", "", stringg)
    stringg = re.sub("srgcpro]", "", stringg)
    stringg = re.sub("belt]", "", stringg)
    stringg = re.sub("sr]", "", stringg)
    stringg = re.sub("suk]", "", stringg)
    stringg = re.sub("\]", "", stringg)
    stringg = re.sub("/snan/s", "", stringg)
    stringg = re.sub("<xxx>", "", stringg)
    stringg = re.sub("<xxx", "", stringg)
    stringg = re.sub("xxx>", "", stringg)
    stringg = re.sub("@", "", stringg)
    return stringg


def stage4_cleaning(stringg):
    stringg = " ".join(i for i in stringg.split(" ") if i != "x")
    stringg = " ".join(i for i in stringg.split(" ") if i != "xx")
    stringg = " ".join(i for i in stringg.split(" ") if i != "xxx")
    return stringg


def remove_words_with_ipa(df, transcript_column_name):
    p = "é|æ|ɑ|ɔ|ɕ|ç|ḏ|ḍ|ð|ə|ɚ|ɛ|ɝ|ḡ|ʰ|ḥ|ḫ|ẖ|ɪ|ỉ|ɨ|ʲ|ǰ|ḳ|ḵ|ḷ|ɬ|ɫ|ŋ|ṇ|ɲ|ɴ|ŏ|ɸ|θ|p̅|þ|ɹ|ɾ|ʀ|ʁ|ṛ|š|ś|ṣ|ʃ|ṭ|ṯ|ʨ|tʂ|ʊ|ŭ|ü|ʌ|ɣ|ʍ|χ|ʸ|ʎ|ẓ|ž|ʒ|’|‘|ʔ|ʕ|∬|↫"
    df = df.loc[
        ~df[transcript_column_name].str.contains(p, regex=True)
    ]  # remove words with phonemes
    df = df.reset_index(drop=True)
    return df

def all_cleaning(df,cleaned_column_name='clean_transcription_v1'):
    df[cleaned_column_name]=df['raw_transcriptions'].apply(filterWordsPhonetic)
    df[cleaned_column_name]=df[cleaned_column_name].apply(clean_other_characters)
    df[cleaned_column_name]=df[cleaned_column_name].apply(remove_bad_words_ab_english)
    df[cleaned_column_name]=df[cleaned_column_name].apply(remove_even_more)
    df[cleaned_column_name]=df[cleaned_column_name].apply(stage4_cleaning)
    df=remove_words_with_ipa(df,cleaned_column_name)
    df=df[~df[cleaned_column_name].isnull()] #remove nulls 
    df[cleaned_column_name] = df[cleaned_column_name].str.rstrip() #remove blanks at the beginning 
    df[cleaned_column_name] = df[cleaned_column_name].str.lstrip() #remove blanks at the end 

    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("<FLR>",'F', string))
    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("<LAU>",'L', string))
    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("<",'', string))
    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub(">",'', string))
    df=df.drop(df[df[cleaned_column_name]==''].index) #remove empty transcripts
    return df

def clean_greek(df,cleaned_column_name='clean_transcription_v1'):
    df[cleaned_column_name]=df['raw_transcriptions']
    #clean everything in brackets
    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("(\[.*?\])",'', string))
    df[cleaned_column_name]=df[cleaned_column_name].apply(clean_other_characters)
    # df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("\+",'', string))
    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("&lt;",'', string))
    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("&gt;",'', string))
    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("&amp;",'', string))

    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("<FLR>",'', string))
    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("<LAU>",'', string))
    df[cleaned_column_name] = df[cleaned_column_name].apply(lambda string:re.sub("xxx",'', string))
    # remove all things in brackets
    df=df[~df[cleaned_column_name].isnull()] #remove nulls 
    df=df.drop(df[df[cleaned_column_name]==""].index) #remove nulls 
    df[cleaned_column_name] = df[cleaned_column_name].str.rstrip() #remove blanks at the beginning 
    df[cleaned_column_name] = df[cleaned_column_name].str.lstrip() #remove blanks at the end 
    return df
 

def clean_f_l(stringg):
    while " f " in stringg:
        stringg = re.sub(" f ", " ", stringg)
    while " l " in stringg:
        stringg = re.sub(" l ", " ", stringg)
    if stringg[-2:] == " f":
        stringg = stringg[:-2]
    if stringg[-2:] == " l":
        stringg = stringg[:-2]
    if stringg[:2] == "l ":
        stringg = stringg[2:]
    if stringg[:2] == "f ":
        stringg = stringg[2:]
    if stringg=="f":
        stringg=""
    if stringg=="l":
        stringg=""
    if stringg[-1:] == " ":
        stringg = stringg[:-1]
    if stringg[:1] == " ":
        stringg = stringg[1:]
    # stringg=re.sub(" f f f "," ",stringg)
    # stringg=re.sub(" f f "," ",stringg)
    # stringg=re.sub(" f "," ",stringg)
    # stringg=re.sub(" l l l "," ",stringg)
    # stringg=re.sub(" l l "," ",stringg)
    # stringg=re.sub(" l "," ",stringg)
    return stringg


# def normalize_whisper(list_of_strings):

#     MODEL_ID = "openai/whisper-small"
#     processor = WhisperProcessor.from_pretrained(MODEL_ID)
#     new_list=[]
#     for s in list_of_strings:
#         new_list.append(processor.tokenizer._normalize(s))
#     return new_list


# def normalize_w2v2_eng(stringg):

#     MODEL_ID = "jonatasgrosman/wav2vec2-large-xlsr-53-english"
#     processor = Wav2Vec2Processor.from_pretrained(MODEL_ID)
#     labels=processor(text=stringg).input_ids
#     return processor.decode(labels, group_tokens=False)


def normalize_for_parsing(stringg):
    # Sentences should begin with Uppercase letter and end with . and no gaps
    # Language Agnostic

    # Remove whitespaces
    stringg = " ".join(stringg.split())
    # Capitalize First letter
    stringg = stringg.capitalize()
    # Remove punctutation
    stringg = stringg.translate(str.maketrans("", "", string.punctuation))
    # Add . at the end of the stringg
    stringg += "."
    return stringg


if __name__ == "__main__":
    import pandas as pd

    # dataset_path='/data/projects/planv/datasets/ab_english/'
    # dataset_path = "splits/ab_english/test.csv"
    # df = pd.read_csv(dataset_path)
    # df["transcriptions"] = df["transcriptions"].apply(filterWordsPhonetic)
    # print(df.head())
    s="l"
    print(clean_f_l(s))