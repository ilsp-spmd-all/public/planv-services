import spacy
import numpy as np
from utils import save_dict, timestamp_to_ints, create_name_value_dict
from utils import get_vocabulary, check_words_in_vocabulary
import tqdm
import pandas as pd
import nltk
nltk.download('punkt')
from nltk.tokenize import sent_tokenize

# from transcript_processing import TranscriptsProcessor
from conllu import parse_tree

# from pprint import pprint
# from arguments import parse_arguments
from feature_names import UTTERANCE_LEVEL_FEATURE_NAMES, FEATURES_NAMES, HELPER_NAMES

from dependency_parser import (
    EnglishConlluParser,
    FrenchConlluParser,
    MandarinConlluParser,
    SpacyConlluParser,
)


class TreeFeatureExtraction(object):
    def __init__(self):
        pass

    def calculate_tree_height(self, node):
        depth_list = []
        if node.children == []:
            # leaf node
            return 0
        for child in node.children:
            depth_list.append(self.calculate_tree_height(child))
        return 1 + max(depth_list)

    def tree_calculations(self, data):
        tree_height = 0

        try:
            tree = parse_tree(data)
        except:
            return 0
        root = tree[0]
        children = root.children

        tree_height = self.calculate_tree_height(root)
        return tree_height

class SingleLineFeatureExtractor(object):
    def __init__(self):
        self.OPEN_CLASS_TYPES = ["NOUN", "VERB", "ADJ", "ADV", "PROPN"]
        self.PUNC = """!()-[]{};:'"\,<>./?@#$%^&*_~"""
        self.tree_extractor = TreeFeatureExtraction()

    def calculate_features_per_utterance(self, text, conllu_str, vocabulary):
        (
            number_of_words,
            n_nouns,
            n_verbs,
            n_adj,
            n_cconj,
            n_adv,
            n_prepositions,
            n_close_class_words,
            n_independent,
            n_dependent,
            n_determiners,
            n_sconj,
            n_pronouns,
            n_interjections,
            n_auxiliary,
            n_sentences

        ) = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0,0,0)
        lemmas, tree_heights, words_in_voc = [], [], []
        # Split lines
        lines = conllu_str.splitlines()

        # Number of words
        number_of_words = self.calculate_number_of_words(text)

        n_sentences=self.calculate_number_of_sentences(text)

        # Words found in dictionary
        words_in_voc, _ = check_words_in_vocabulary(vocabulary, text)

        # Convert lines to list of tokens
        # Remove lines starting with #
        lists_of_tokens = self.convert_lines_to_lists_of_tokens(lines)

        for t in lists_of_tokens:
            lemma = t[2]
            lemmas.append(lemma)
            pos_tag = t[3]

            # POS counters
            if pos_tag == "NOUN" or pos_tag == "PROPN":
                n_nouns += 1
            elif pos_tag == "VERB":
                if t[-3] != "aux":
                    # Example: "Η αλήθεια έχει αναζητηθεί"
                    # we need to count 1 verb and not 2
                    # possible way: do not consider aux verbs
                    n_verbs += 1

                if t[-3] == "xcomp" and "VerbForm=Part" in t[5]:
                    # It is not a verb, it is an adjective
                    # xcomp + verb + "VerbForm=Part"
                    # e.g. Η Νίκη ΄ηταν εξασφαλισμένη >> εξασφαλισμένη is not a verb
                    n_verbs -= 1  # do not count it as verb
                    n_adj += 1  # count it as as adjective
                if t[-3] == "amod" and "VerbForm=Part" in t[5]:
                    n_verbs -= 1  # do not count it as verb
                    n_adj += 1  # count it as as adjective
                if t[-3] == "obj" and "VerbForm=Part" in t[5]:
                    n_verbs -= 1  # do not count it as verb
                    n_nouns += 1  # count it as as noun
                if t[-3] == "nsubj" and "VerbForm=Part" in t[5]:
                    n_verbs -= 1  # do not count it as verb
                    n_nouns += 1  # count it as as noun
            elif pos_tag == "ADJ":
                n_adj += 1
            elif pos_tag == "CCONJ":
                n_cconj += 1
            elif pos_tag == "SCONJ":
                n_sconj += 1
            elif pos_tag == "DET":
                n_determiners += 1
            elif pos_tag == "ADV":
                n_adv += 1
            elif pos_tag == "ADP":
                n_prepositions += 1
            elif pos_tag == "PRON":
                n_pronouns += 1
            elif pos_tag == "INTJ":
                n_interjections += 1
            elif pos_tag == "AUX":
                n_auxiliary +=1
            

            if pos_tag not in self.OPEN_CLASS_TYPES:
                # No need to worry about participle as verb because either way (verb/adj)
                # it will be summed in n_close_class_words_utterance
                n_close_class_words += 1

        if lists_of_tokens:
            tree_heights.append(self.tree_extractor.tree_calculations(conllu_str))
            (
                n_independent,
                n_dependent,
            ) = self.calculate_dependent_independent_sentences_per_utterance(conllu_str)
        return (
            number_of_words,
            n_nouns,
            n_verbs,
            n_adj,
            n_cconj,
            n_adv,
            n_prepositions,
            n_close_class_words,
            n_independent,
            n_dependent,
            lemmas,
            tree_heights,
            words_in_voc,
            n_determiners,
            n_sconj,
            n_pronouns,
            n_auxiliary,
            n_interjections,
            n_sentences
        )

    def calculate_number_of_words(self, utterance):
        for ele in self.PUNC:
            utterance = utterance.replace(ele, " ")
        # remove whitespaces between words and get list of words
        words = " ".join(utterance.split()).split(" ")
        return len(words)
    
    def calculate_number_of_sentences(self,utterance):
        number_of_sentences = sent_tokenize(utterance)

        return(len(number_of_sentences))


    def convert_lines_to_lists_of_tokens(self, lines):
        token_list = []
        for line in lines:
            if (
                ("# newpar id" in line)
                or ("# sent_id" in line)
                or ("# text" in line)
                or ("# newdoc id" in line)
            ):
                continue
            if line:
                # to avoid processing empty lines
                if "PUNCT" in line:
                    continue
                tmp_line = line.split("\t")
                token_list.append(tmp_line)
        return token_list

    def calculate_dependent_independent_sentences_per_utterance(self, conllu_utterance):
        n_dependent_utterance, n_independent_utterance = 0, 0
        tokens = conllu_utterance.split("\n")
        tokens = [token for token in tokens if token and token[0] != "#"]
        prev_sentence = ""
        for token in tokens:
            tmp = token.split("\t")
            pos_tag = tmp[3]
            rel = tmp[-3]
            if pos_tag != "VERB":
                continue
            if rel == "xcomp" and "VerbForm=Part" in tmp[5]:
                continue
            if rel == "amod" and "VerbForm=Part" in tmp[5]:
                continue
            if rel == "obj" and "VerbForm=Part" in tmp[5]:
                continue
            if rel == "nsubj" and "VerbForm=Part" in tmp[5]:
                continue
            if rel == "cop":
                goto_id = tmp[-4]
                goto_token = tokens[int(goto_id) - 1].split("\t")
                if int(goto_token[0]) != int(goto_id):
                    ind = int(goto_id) - int(goto_token[0])
                    goto_token = tokens[int(goto_id) - 1 + ind].split("\t")
                if goto_token[3] == "ADJ":
                    goto_rel = goto_token[-3]
                    if goto_rel in [
                        "advcl",
                        "acl:relcl",
                        "ccomp",
                        "xcomp",
                        "acl",
                        "csubj",
                        "csubj:pass",
                    ]:
                        n_dependent_utterance += 1
                        prev_sentence = "dependent"
                    if goto_rel in ["root"]:
                        n_independent_utterance += 1
                        prev_sentence = "independent"
                    continue
            if rel == "parataxis":
                goto_id = tmp[-4]
                goto_token = tokens[int(goto_id) - 1].split("\t")
                if goto_token[3] == "VERB":
                    goto_rel = goto_token[-3]
                    if goto_rel in [
                        "advcl",
                        "acl:relcl",
                        "ccomp",
                        "xcomp",
                        "acl",
                        "csubj",
                        "csubj:pass",
                    ]:
                        n_dependent_utterance += 1
                        prev_sentence = "dependent"
                    if goto_rel in ["root"]:
                        n_independent_utterance += 1
                        prev_sentence = "independent"
                    continue
            if rel == "conj":
                if prev_sentence == "independent":
                    n_independent_utterance += 1
                elif prev_sentence == "dependent":
                    n_dependent_utterance += 1
                else:
                    # print("conj sentence with no previous dependent/independent sentence")
                    pass
            if rel in [
                "advcl",
                "acl:relcl",
                "ccomp",
                "xcomp",
                "acl",
                "csubj",
                "csubj:pass",
            ]:
                n_dependent_utterance += 1
                prev_sentence = "dependent"
            if rel in ["ROOT"]:
                n_independent_utterance += 1
                prev_sentence = "independent"
        return n_independent_utterance, n_dependent_utterance


class SpeakerFeatureExtractor(SingleLineFeatureExtractor):
    """
    feature_extraction_from_df: Expects a DataFrame with rows the utterances of a single user,
    returns features and helpers for the user
    """

    def __init__(
        self,
        language,
        utterance_column="clean_transcription_v1",
        conllu_column="clean_transcription_v1_conllu",
        duration_seconds_column="duration",
    ):
        super().__init__()
        self.number_of_features = 24
        self.number_of_helpers = 15
        self.language = language
        if language == "el":
            self.spacy_model_name = "el_core_news_lg"
            # self.nlp = spacy.load("el_core_news_lg")
        elif language == "en":
            self.spacy_model_name = "en_core_web_trf"
            # self.nlp = spacy.load("en_core_web_trf")
        elif language == "ma":
            self.spacy_model_name = "zh_core_web_trf"
            # self.nlp = spacy.load("zh_core_web_trf")
        elif language == "fr":
            self.spacy_model_name = "fr_dep_news_trf"
        self.nlp = spacy.load(self.spacy_model_name)
        # self.vocabulary = get_vocabulary(self.nlp, self.language)
        self.vocabulary = set(self.nlp.vocab.strings)
        self.utterance_column = utterance_column
        self.conllu_column = conllu_column
        self.duration_seconds_column = duration_seconds_column

    def feature_extraction_from_df(self, df):
        (
            total_duration,
            n_utterances,
            number_of_words,
            n_nouns,
            n_verbs,
            n_adj,
            n_cconj,
            n_adv,
            n_prepositions,
            n_close_class_words,
            n_independent,
            n_dependent,
            lemmas,
            tree_heights,
            words_in_voc,
            n_determiners,
            n_sconj,
            n_pronouns,
            n_interjections,
            n_auxiliary,
            n_sentences
        ) = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, [], [], [], 0, 0, 0,0,0,0)

        n_utterances = len(df)
        for utterance, conllu, duration in zip(
            df[self.utterance_column],
            df[self.conllu_column],
            df[self.duration_seconds_column],
        ):

            total_duration += duration
            utterance_features = self.calculate_features_per_utterance(
                utterance, conllu, self.vocabulary
            )

            number_of_words += utterance_features[0]
            n_nouns += utterance_features[1]
            n_verbs += utterance_features[2]
            n_adj += utterance_features[3]
            n_cconj += utterance_features[4]
            n_adv += utterance_features[5]
            n_prepositions += utterance_features[6]
            n_close_class_words += utterance_features[7]
            n_independent += utterance_features[8]
            n_dependent += utterance_features[9]
            lemmas += utterance_features[10]
            tree_heights += utterance_features[11]
            words_in_voc += utterance_features[12]
            n_determiners += utterance_features[13]
            n_sconj += utterance_features[14]
            n_pronouns += utterance_features[15]
            n_interjections += utterance_features[16]
            n_auxiliary += utterance_features[17]
            n_sentences += utterance_features[18]

        helpers = [
            total_duration,
            n_utterances,
            number_of_words,
            n_nouns,
            n_verbs,
            n_adj,
            n_cconj,
            n_adv,
            n_prepositions,
            n_close_class_words,
            n_independent,
            n_dependent,
            lemmas,
            tree_heights,
            words_in_voc,
            n_determiners,
            n_sconj,
            n_pronouns,
            n_interjections,
            n_auxiliary,
            n_sentences
        ]

        # helpers_dict= {n: v for n, v in zip(HELPER_NAMES, helpers)}

        story_features = self.calculate_features(*helpers)
        return story_features, helpers

    def calculate_features(
        self,
        total_duration,
        n_utterances,
        number_of_words,
        n_nouns,
        n_verbs,
        n_adj,
        n_cconj,
        n_adv,
        n_prepositions,
        n_close_class_words,
        n_independent,
        n_dependent,
        lemmas,
        tree_heights,
        words_in_voc,
        n_determiners,
        n_sconj,
        n_pronouns,
        n_interjections,
        n_auxiliary,
        n_sentences
    ):
        n_lemmas = len(list(set(lemmas)))
        n_open_class_words = n_nouns + n_verbs + n_adj + n_adv

        # Features
        # 1. Linguistic Productivity
        average_utterance_length = number_of_words / n_sentences if n_sentences != 0 else 0

        # 2. Propositional Density / Richness
        verbs_words_ratio = n_verbs / number_of_words if number_of_words != 0 else 0
        nouns_words_ratio = n_nouns / number_of_words if number_of_words != 0 else 0
        adjectives_words_ratio = n_adj / number_of_words if number_of_words != 0 else 0
        adverbs_words_ratio = n_adv / number_of_words if number_of_words != 0 else 0
        conjuctions_words_ratio = (n_cconj + n_sconj) / number_of_words if number_of_words != 0 else 0
        prepositions_words_ratio = n_prepositions / number_of_words if number_of_words != 0 else 0
        coordinating_conjunctions_words_ratio = n_cconj / number_of_words if number_of_words != 0 else 0
        superlative_conjunctions_words_ratio = n_sconj / number_of_words if number_of_words != 0 else 0
        determiners_words_ratio = n_determiners / number_of_words if number_of_words != 0 else 0
        pronouns_words_ratio = n_pronouns / number_of_words if number_of_words != 0 else 0
        # interjections_words_ratio=n_interjections/number_of_words if number_of_words != 0 else 0
        auxiliaries_words_ratio=n_auxiliary/number_of_words if number_of_words != 0 else 0

        # 3. Fluency
        words_per_minute = number_of_words / (total_duration * 60) if total_duration != 0 else 0

        # 4. Syntactic Complexity
        verbs_per_utterance = n_verbs / n_sentences if n_sentences != 0 else 0

        # 5. Lexical Diversity (type - token ratio)
        unique_words_per_words = n_lemmas / number_of_words if number_of_words != 0 else 0

        # 6. Syntactic Complexity
        nouns_verbs_ratio = (n_nouns / n_verbs) if n_verbs != 0 else 0

        # 7. Word Retrieval - Gross Output
        number_of_words = number_of_words

        # 8. Syntactic Complexity
        open_close_class_ratio = (
            n_open_class_words / n_close_class_words if n_close_class_words != 0 else 0
        )

        # 9. Mean Number of Clauses per Utterance
        mean_clauses_per_utterance = (
            (n_independent + n_dependent) / n_utterances if n_utterances != 0 else 0
        )

        # 10. Mean of dependent clauses per utterance
        mean_dependent_clauses = n_dependent / n_utterances if n_utterances != 0 else 0

        # 10.1. Mean of independent clauses per utterance
        mean_independent_clauses = (
            n_independent / n_utterances if n_utterances != 0 else 0
        )

        ## Extra 10.2 Dependent Clauses / Total Number of clauses
        dependent_all_clauses_ratio = (
            n_dependent / (n_dependent + n_independent)
            if n_dependent + n_independent != 0
            else 0
        )

        ## 11. Mean Tree Height
        mean_tree_height = (
            sum(tree_heights) / len(tree_heights) if len(tree_heights) != 0 else 0
        )

        ## 13. Max Tree Depth (equals to Tree Height)
        max_tree_depth = max(tree_heights) if tree_heights else 0

        ## Extra 1: Number of Independent Sentences (Utterance)
        n_independent = n_independent
        ## Extra 2: Number of Dependent Sentences (Utterance)
        n_dependent = n_dependent
        ## Extra 3: Number of verbs + adjectives + adverbs + prepositions + conjunctions
        ## divided by the total number of words
        propositional_density = (
            (n_verbs + n_adj + n_adv + n_prepositions + n_cconj) / number_of_words
            if number_of_words != 0
            else 0
        )
        ## Number of Words in Vocabulary per Number of Words
        words_in_vocabulary_per_words = (
            len(words_in_voc) / number_of_words if number_of_words != 0 else 0
        )
        ## Number of Unique Words in Vocabulary per Number of Words
        unique_words_in_vocabulary_per_words = (
            len(list(set(words_in_voc))) / number_of_words if number_of_words != 0 else 0
        )



        features = [
            average_utterance_length,
            verbs_words_ratio,
            nouns_words_ratio,
            adjectives_words_ratio,
            adverbs_words_ratio,
            conjuctions_words_ratio,
            prepositions_words_ratio,
            words_per_minute,
            verbs_per_utterance,
            unique_words_per_words,
            nouns_verbs_ratio,
            number_of_words,
            open_close_class_ratio,
            mean_clauses_per_utterance,
            mean_dependent_clauses,
            mean_dependent_clauses,
            dependent_all_clauses_ratio,
            mean_tree_height,
            max_tree_depth,
            n_independent,
            n_dependent,
            propositional_density,
            words_in_vocabulary_per_words,
            unique_words_in_vocabulary_per_words,
            coordinating_conjunctions_words_ratio,
            superlative_conjunctions_words_ratio,
            determiners_words_ratio,
            pronouns_words_ratio#,
            # interjections_words_ratio
        ]

        features_dict = {n: v for n, v in zip(FEATURES_NAMES, features)}

        return features_dict


class DatasetFeatureExtractor(SpeakerFeatureExtractor):
    """
    feature_extraction_from_df: Expects a DataFrame with rows the utterances of a single user,
    returns features and helpers for the user
    """

    def __init__(
        self,
        language="en",
        utterance_column="clean_transcription_v1",
        conllu_column="clean_transcription_v1_conllu"
    ):
        super().__init__(language=language)
        # self.number_of_features = 24
        # self.number_of_helpers = 15
        self.language = language
        
        if language == "el":
            self.spacy_model_name = "el_core_news_lg"
            # self.nlp = spacy.load("el_core_news_lg")
        elif language == "en":
            self.spacy_model_name = "en_core_web_trf"
            # self.nlp = spacy.load("en_core_web_trf")
        elif language == "ma":
            self.spacy_model_name = "zh_core_web_trf"
            # self.nlp = spacy.load("zh_core_web_trf")
        elif language == "fr":
            self.spacy_model_name = "fr_dep_news_trf"
        self.spacy_parser = SpacyConlluParser(spacy_model_name=self.spacy_model_name)
        self.nlp = self.spacy_parser.nlp

        # self.vocabulary = get_vocabulary(self.nlp, self.language)
        self.vocabulary = set(self.nlp.vocab.strings)
        self.utterance_column = utterance_column
        self.conllu_column = conllu_column

    def extract_conllu(self,df):
        # extract conllu string from dataset and save it
        df[self.conllu_column] = self.spacy_parser.calculate_conllus_from_list(
            list(df[self.utterance_column])
        )
        # df.to_csv(csv_path, index=False)

    def calculate_features_per_story(self,df,utterance_column_name):
        df_feats=pd.DataFrame()
        for par in df["Participant ID"].unique():
            df_par=df[df["Participant ID"] == par]
            for story in df_par["story"].unique():
                dff=df_par[df_par["story"]==story]
                feats, helpers = self.feature_extraction_from_df(dff)
                if "control" in dff.columns:

                    dfff = (
                        dff[
                            [
                                # "sex",
                                # "age",
                                "WAB_AQ",
                                "aphasia_type",
                                "Participant ID",
                                "WAB_AQ_category",
                                "control",
                                "story",
                            ]
                        ]
                        .iloc[0]
                        .to_dict()
                    )
                else:
                    dfff=(
                        dff[
                            [
                                # "sex",
                                # "age",
                                # "aphasia_type",
                                "Participant ID",
                                # "control",
                                # "BDAE_score",
                                "story",
                                utterance_column_name
                            ]
                        ]
                        .iloc[0]
                        .to_dict()
                    )
                feats = feats | dfff
                feats = {k: [v] for k, v in zip(feats.keys(), feats.values())}
                feats = pd.DataFrame.from_dict(feats)

                df_feats = pd.concat([df_feats, feats])

        df_feats=df_feats.reset_index(drop=True)
        return df_feats
    
    def calculate_features_all_speakers(self,df):

        df_feats = pd.DataFrame()
        for par in df["Participant ID"].unique():
            dff = df[df["Participant ID"] == par]

            feats, helpers = self.feature_extraction_from_df(dff)
            if "control" in dff.columns:

                dfff = (
                    dff[
                        [
                            # "sex",
                            # "age",
                            "WAB_AQ",
                            "aphasia_type",
                            "Participant ID",
                            "WAB_AQ_category",
                            "control",
                            "WAB_AQ_category"
                        ]
                    ]
                    .iloc[0]
                    .to_dict()
                )
            else:
                dfff=(
                    dff[
                        [
                            # "sex",
                            # "age",
                            # "aphasia_type",
                            "Participant ID",
                            # "control",
                            # "BDAE_score",
                        ]
                    ]
                    .iloc[0]
                    .to_dict()
                )
            feats = feats | dfff
            feats = {k: [v] for k, v in zip(feats.keys(), feats.values())}
            feats = pd.DataFrame.from_dict(feats)

            df_feats = pd.concat([df_feats, feats])

        df_feats=df_feats.reset_index(drop=True)
        return df_feats


class SingleLineFeatureExtractorNew(object):
    # Uses a spacy model to extract features from some text (preferably an utterance)
    def __init__(self, spacy_model):
        self.OPEN_CLASS_TYPES = ["NOUN", "VERB", "ADJ", "ADV", "PROPN"]
        self.PUNC = """!()-[]{};:'"\,<>./?@#$%^&*_~"""
        self.tree_extractor = TreeFeatureExtraction()
        self.spacy_model = spacy_model
        

    def calculate_features_per_utterance(self, text):
        (
            number_of_words,
            n_nouns,
            n_verbs,
            n_adj,
            n_cconj,
            n_adv,
            n_prepositions,
            n_close_class_words,
            n_independent,
            n_dependent,
            n_determiners,
            n_sconj,
            n_pronouns,
            n_inerjections
            
        ) = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        lemmas=[]
        tree_heights=[]
        words_in_voc=[]
        doc = self.spacy_model(text)

        def calculate_token_depth(token):
            if token.dep_ == 'ROOT':
                return 0

            depths = [0]
            for child in token.children:
                depths.append(calculate_token_depth(child))

            return max(depths) + 1

        #POS counters
        for token in doc:
            pos = token.pos_
            word=token.text
            # exclude punctuation
            if pos=="PUNCT":
                continue
            number_of_words+=1
            lemmas.append(token.lemma_)
            if word in self.spacy_model.vocab:
                words_in_voc.append(word)
            if pos == "NOUN" or pos == "PROPN":
                n_nouns += 1
            if pos == "VERB":
                n_verbs += 1
            if pos == "ADJ":
                n_adj += 1
            if pos == "CCONJ":
                n_cconj += 1
            if pos == "ADV":
                n_adv += 1
            if pos == "ADP":
                n_prepositions += 1
            if pos == "DET":
                n_determiners += 1
            if pos == "SCONJ":
                n_sconj += 1
            if pos == "PRON":
                n_pronouns += 1
            if pos == "INTJ":
                n_interjections += 1

        n_close_class_words = n_nouns + n_verbs + n_adj + n_adv

        # dependent/independent sentence counters
        for sentence in doc.sents:
            max_depth = 0
            has_dependent = False
            
            # Iterate over the tokens in the sentence
            for token in sentence:

                # tree height = the maximum depth of all tokens in the text
                depth = calculate_token_depth(token)
                if depth > max_depth:
                    max_depth = depth

                # Check if the token has a dependent
                # if token.dep_ != 'ROOT':
                if depth > 0:
                    has_dependent = True
                    # break
            
            # Increment the count based on dependency presence
            if has_dependent:
                n_dependent += 1
            else:
                n_independent += 1
        tree_heights.append(max_depth)
        
            
        return (
                    number_of_words,
                    n_nouns,
                    n_verbs,
                    n_adj,
                    n_cconj,
                    n_adv,
                    n_prepositions,
                    n_close_class_words,
                    n_independent,
                    n_dependent,
                    lemmas,
                    tree_heights,
                    words_in_voc,
                    n_determiners,
                    n_sconj,
                    n_pronouns,
                    n_inerjections
                )


class SpeakerFeatureExtractorNew(SingleLineFeatureExtractorNew):

    """
    feature_extraction_from_df: Expects a DataFrame with rows the utterances of a single user,
    returns features and helpers for the user
    """

    def __init__(
        self,
        language="en",
        utterance_column="clean_transcription_v1",
        # conllu_column="clean_transcription_v1_conllu",
        duration_seconds_column="duration_seconds",
    ):
        self.language = language
        if self.language == "el":
            self.spacy_model = spacy.load("el_core_news_lg")
        elif self.language == "en":
            self.spacy_model = spacy.load("en_core_web_trf")
        elif self.language == "fr":
            self.spacy_model = spacy.load("fr_core_news_lg")
        super().__init__(spacy_model=self.spacy_model)
        self.number_of_features = 24
        self.number_of_helpers = 15
        
        # self.language = language
        # if language == "el":
        #     self.nlp = spacy.load("el_core_news_lg")
        # elif language == "en":
        #     self.nlp = spacy.load("en_core_web_trf")
        # elif language == "ma":
        #     self.nlp = spacy.load("zh_core_web_trf")
        # elif language == "fr":
        #     self.nlp = spacy.load("fr_dep_news_trf")
        # self.vocabulary = get_vocabulary(self.nlp, self.language)
        # self.vocabulary = set(self.nlp.vocab.strings)
        self.utterance_column = utterance_column
        # self.conllu_column = conllu_column
        self.duration_seconds_column = duration_seconds_column

    def feature_extraction_from_df(self, df):
        (
            total_duration,
            n_utterances,
            number_of_words,
            n_nouns,
            n_verbs,
            n_adj,
            n_cconj,
            n_adv,
            n_prepositions,
            n_close_class_words,
            n_independent,
            n_dependent,
            lemmas,
            tree_heights,
            words_in_voc,
            n_determiners,
            n_sconj,
            n_pronouns,
            n_inerjections
        ) = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, [], [], [], 0, 0, 0, 0)

        n_utterances = len(df)
        for utterance, duration in zip(
            df[self.utterance_column],
            df[self.duration_seconds_column],
        ):

            total_duration += duration
            utterance_features = self.calculate_features_per_utterance(
                utterance
            )

            number_of_words += utterance_features[0]
            n_nouns += utterance_features[1]
            n_verbs += utterance_features[2]
            n_adj += utterance_features[3]
            n_cconj += utterance_features[4]
            n_adv += utterance_features[5]
            n_prepositions += utterance_features[6]
            n_close_class_words += utterance_features[7]
            n_independent += utterance_features[8]
            n_dependent += utterance_features[9]
            lemmas += utterance_features[10]
            tree_heights += utterance_features[11]
            words_in_voc += utterance_features[12]
            n_determiners += utterance_features[13]
            n_sconj += utterance_features[14]
            n_pronouns += utterance_features[15]
            n_inerjections += utterance_features[16]

        helpers = [
            total_duration,
            n_utterances,
            number_of_words,
            n_nouns,
            n_verbs,
            n_adj,
            n_cconj,
            n_adv,
            n_prepositions,
            n_close_class_words,
            n_independent,
            n_dependent,
            lemmas,
            tree_heights,
            words_in_voc,
            n_determiners,
            n_sconj,
            n_pronouns,
            n_inerjections
        ]

        # helpers_dict= {n: v for n, v in zip(HELPER_NAMES, helpers)}

        story_features = self.calculate_features(*helpers)
        return story_features, helpers
    
    

    def calculate_features(
        self,
        total_duration,
        n_utterances,
        number_of_words,
        n_nouns,
        n_verbs,
        n_adj,
        n_cconj,
        n_adv,
        n_prepositions,
        n_close_class_words,
        n_independent,
        n_dependent,
        lemmas,
        tree_heights,
        words_in_voc,
        n_determiners,
        n_sconj,
        n_pronouns,
        n_inerjections
    ):
        n_lemmas = len(list(set(lemmas)))
        n_open_class_words = n_nouns + n_verbs + n_adj + n_adv

        # Features
        # 1. Linguistic Productivity
        average_utterance_length = number_of_words / n_utterances if n_utterances != 0 else 0

        # 2. Propositional Density / Richness
        verbs_words_ratio = n_verbs / number_of_words if number_of_words != 0 else 0
        nouns_words_ratio = n_nouns / number_of_words if number_of_words != 0 else 0
        adjectives_words_ratio = n_adj / number_of_words if number_of_words != 0 else 0
        adverbs_words_ratio = n_adv / number_of_words if number_of_words != 0 else 0
        conjuctions_words_ratio = (n_cconj + n_sconj) / number_of_words if number_of_words != 0 else 0
        prepositions_words_ratio = n_prepositions / number_of_words if number_of_words != 0 else 0
        coordinating_conjunctions_words_ratio = n_cconj / number_of_words if number_of_words != 0 else 0
        superlative_conjunctions_words_ratio = n_sconj / number_of_words if number_of_words != 0 else 0
        determiners_words_ratio = n_determiners / number_of_words if number_of_words != 0 else 0
        pronouns_words_ratio = n_pronouns / number_of_words if number_of_words != 0 else 0
        # interjections_words_ratio=n_inerjections/number_of_words if number_of_words != 0 else 0
        # 3. Fluency
        words_per_minute = number_of_words * 60 / total_duration if total_duration != 0 else 0

        # 4. Syntactic Complexity
        verbs_per_utterance = n_verbs / n_utterances if n_utterances != 0 else 0

        # 5. Lexical Diversity (type - token ratio)
        unique_words_per_words = n_lemmas / number_of_words if number_of_words != 0 else 0

        # 6. Syntactic Complexity
        nouns_verbs_ratio = (n_nouns / n_verbs) if n_verbs != 0 else 0

        # 7. Word Retrieval - Gross Output
        number_of_words = number_of_words

        # 8. Syntactic Complexity
        open_close_class_ratio = (
            n_open_class_words / n_close_class_words if n_close_class_words != 0 else 0
        )

        # 9. Mean Number of Clauses per Utterance
        mean_clauses_per_utterance = (
            (n_independent + n_dependent) / n_utterances if n_utterances != 0 else 0
        )

        # 10. Mean of dependent clauses per utterance
        mean_dependent_clauses = n_dependent / n_utterances if n_utterances != 0 else 0

        # 10.1. Mean of independent clauses per utterance
        mean_independent_clauses = (
            n_independent / n_utterances if n_utterances != 0 else 0
        )

        ## Extra 10.2 Dependent Clauses / Total Number of clauses
        dependent_all_clauses_ratio = (
            n_dependent / (n_dependent + n_independent)
            if n_dependent + n_independent != 0
            else 0
        )

        ## 11. Mean Tree Height
        mean_tree_height = (
            sum(tree_heights) / len(tree_heights) if len(tree_heights) != 0 else 0
        )

        ## 13. Max Tree Depth (equals to Tree Height)
        max_tree_depth = max(tree_heights) if tree_heights else 0

        ## Extra 1: Number of Independent Sentences (Utterance)
        n_independent = n_independent
        ## Extra 2: Number of Dependent Sentences (Utterance)
        n_dependent = n_dependent
        ## Extra 3: Number of verbs + adjectives + adverbs + prepositions + conjunctions
        ## divided by the total number of words
        propositional_density = (
            (n_verbs + n_adj + n_adv + n_prepositions + n_cconj) / number_of_words
            if number_of_words != 0
            else 0
        )
        ## Number of Words in Vocabulary per Number of Words
        words_in_vocabulary_per_words = (
            len(words_in_voc) / number_of_words if number_of_words != 0 else 0
        )
        ## Number of Unique Words in Vocabulary per Number of Words
        unique_words_in_vocabulary_per_words = (
            len(list(set(words_in_voc))) / number_of_words if number_of_words != 0 else 0
        )

        features = [
            average_utterance_length,
            verbs_words_ratio,
            nouns_words_ratio,
            adjectives_words_ratio,
            adverbs_words_ratio,
            conjuctions_words_ratio,
            prepositions_words_ratio,
            words_per_minute,
            verbs_per_utterance,
            unique_words_per_words,
            nouns_verbs_ratio,
            number_of_words,
            open_close_class_ratio,
            mean_clauses_per_utterance,
            mean_dependent_clauses,
            mean_independent_clauses,
            dependent_all_clauses_ratio,
            mean_tree_height,
            max_tree_depth,
            n_independent,
            n_dependent,
            propositional_density,
            words_in_vocabulary_per_words,
            unique_words_in_vocabulary_per_words,
            coordinating_conjunctions_words_ratio,
            superlative_conjunctions_words_ratio,
            determiners_words_ratio,
            pronouns_words_ratio#,
            # interjections_words_ratio
        ]

        features_dict={
            "average_utterance_length": average_utterance_length,
            "verbs_words_ratio": verbs_words_ratio,
            "nouns_words_ratio": nouns_words_ratio,
            "adjectives_words_ratio": adjectives_words_ratio,
            "adverbs_words_ratio": adverbs_words_ratio,
            "conjuctions_words_ratio": conjuctions_words_ratio,
            "prepositions_words_ratio": prepositions_words_ratio,
            "words_per_minute": words_per_minute,
            "verbs_per_utterance": verbs_per_utterance,
            "unique_words_per_words": unique_words_per_words,
            "nouns_verbs_ratio": nouns_verbs_ratio,
            "number_of_words": number_of_words,
            "open_close_class_ratio": open_close_class_ratio,
            "mean_clauses_per_utterance": mean_clauses_per_utterance,
            "mean_dependent_clauses": mean_dependent_clauses,
            "mean_independent_clauses": mean_independent_clauses,
            "dependent_all_clauses_ratio": dependent_all_clauses_ratio,
            "mean_tree_height": mean_tree_height,
            "max_tree_depth": max_tree_depth,
            "n_independent": n_independent,
            "n_dependent": n_dependent,
            "propositional_density": propositional_density,
            "words_in_vocabulary_per_words": words_in_vocabulary_per_words,
            "unique_words_in_vocabulary_per_words": unique_words_in_vocabulary_per_words,
            # "coordinating_conjunctions_words_ratio": coordinating_conjunctions_words_ratio,
            # "superlative_conjunctions_words_ratio": superlative_conjunctions_words_ratio,
            # "interjections_words_ratio": interjections_words_ratio,
            "determiners_words_ratio": determiners_words_ratio,
            "pronouns_words_ratio": pronouns_words_ratio,
        }

        # features_dict = {n: v for n, v in zip(FEATURES_NAMES, features)}

        return features_dict

class DatasetFeatureExtractorNew(SpeakerFeatureExtractorNew):
    """
    feature_extraction_from_df: Expects a DataFrame with rows the utterances of a single user,
    returns features and helpers for the user
    """

    def __init__(
        self,
        df,
        language="en",
        utterance_column="clean_transcription_v1",
        duration_seconds_column="duration_seconds",
        # conllu_column="clean_transcription_v1_conllu",
        # extract_conllu=False,
    ):
        super().__init__(language=language, utterance_column=utterance_column, duration_seconds_column=duration_seconds_column)
        self.number_of_features = 24
        self.number_of_helpers = 15
        self.df = df
        # self.spacy_parser = SpacyConlluParser(spacy_model_name=self.spacy_model_name)
        # self.nlp = self.spacy_parser.nlp
        # self.utterance_column = utterance_column


    def calculate_features_all_speakers(self):

        df_feats = pd.DataFrame()
        for par in self.df["Participant ID"].unique():
            dff = self.df[self.df["Participant ID"] == par]

            feats, helpers = self.feature_extraction_from_df(dff)
            if "WAB_AQ" in dff.columns:

                dfff = (
                    dff[
                        [
                            "sex",
                            "age",
                            "WAB_AQ",
                            "aphasia_type",
                            "Participant ID",
                            "WAB_AQ_category",
                            "control",
                        ]
                    ]
                    .iloc[0]
                    .to_dict()
                )
            elif "BDAE_score" in dff.columns:
                dfff=(
                    dff[
                        [
                            "sex",
                            "age",
                            "aphasia_type",
                            "Participant ID",
                            "control",
                            "BDAE_score",
                        ]
                    ]
                    .iloc[0]
                    .to_dict()
                )
            else:
                dfff = (
                    dff[
                        [
                            # "sex",
                            # "age",
                            # "aphasia_type",
                            "Participant ID",
                            "control",
                        ]
                    ]
                    .iloc[0]
                    .to_dict()
                )
            feats = feats | dfff
            feats = {k: [v] for k, v in zip(feats.keys(), feats.values())}
            feats = pd.DataFrame.from_dict(feats)

            df_feats = pd.concat([df_feats, feats])

        return df_feats

    def calculate_features_per_story(self):
        utterance_column_name=self.utterance_column
        df=self.df
        df_feats=pd.DataFrame()
        for par in df["Participant ID"].unique():
            df_par=df[df["Participant ID"] == par]
            for story in df_par["story"].unique():
                dff=df_par[df_par["story"]==story]
                feats, helpers = self.feature_extraction_from_df(dff)
                if "WAB_AQ" in dff.columns:

                    dfff = (
                        dff[
                            [
                                # "sex",
                                # "age",
                                "WAB_AQ",
                                "aphasia_type",
                                "Participant ID",
                                "WAB_AQ_category",
                                "control",
                                "story",
                            ]
                        ]
                        .iloc[0]
                        .to_dict()
                    )
                else:
                    dfff=(
                        dff[
                            [
                                # "sex",
                                # "age",
                                # "aphasia_type",
                                "Participant ID",
                                # "control",
                                # "BDAE_score",
                                "story",
                                utterance_column_name
                            ]
                        ]
                        .iloc[0]
                        .to_dict()
                    )
                feats = feats | dfff
                feats = {k: [v] for k, v in zip(feats.keys(), feats.values())}
                feats = pd.DataFrame.from_dict(feats)

                df_feats = pd.concat([df_feats, feats])

        df_feats=df_feats.reset_index(drop=True)
        return df_feats


def calculate_conllu_spacy_df(
    df,
    key_target="clean_transcription_v1",
    conllu_name="conllu",
    spacy_model_name="en_core_web_trf",
):

    conllu_name = key_target + "_" + conllu_name
    english_parser = SpacyConlluParser(spacy_model_name)

    def call_parser_on_str(st):
        if st != "":
            conllu = english_parser.calculate_conllu_from_sentence(st)
        return conllu

    print("Calculating connlu's...")
    conllus = []
    for utt in tqdm.tqdm(df[key_target]):
        conllus.append(call_parser_on_str(utt))
    df[conllu_name] = conllus
    print("Conllu calculation finished!")
    print("-------------------")
    return df


if __name__ == "__main__":
    # Running this script will perform the whole feature extraction process
    # better calculate conllus separately and save them to avoid overhead
    # feature extraction given conllus is much faster

    # csv_path = "/home/plitsis/planv/datasets/ab_english/all_data.csv"
    # csv_path="/home/plitsis/planv/datasets/planv-aphasia/all_data.csv"
    # csv_path = "/home/plitsis/planv/datasets/planv_all.csv"
    # csv_path="/home/plitsis/planv/crosslingual-aphasia-assessment/data_csv/all_data_fr.csv"
    csv_path="gr-data2.csv"
    df = pd.read_csv(csv_path)
    utterance_column_name = "whisper_large"

    feature_extractor = DatasetFeatureExtractor(
        language="el",
        utterance_column=utterance_column_name,
        conllu_column=utterance_column_name + "_conllu"
    )

    # feature_extractor.extract_conllu(df)
    # df.to_csv(csv_path, index=False)
    df_feats = feature_extractor.calculate_features_all_speakers(df)
    print(df_feats.head())

    df_feats.to_csv("greek-feats.csv",index=False)

    # slf=SingleLineFeatureExtractor()
    # text="i told him to stay or go"
    # conllu=SpacyConlluParser().calculate_conllu_from_sentence(text)
    # feats=slf.calculate_features_per_utterance(text,conllu,[])
    # print(text)
    # print(feats)
